package com.iphonezoo.cpapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;


public class AllVictimsListAdapter extends BaseAdapter {

    Context context;
    List<String> victims;

    public AllVictimsListAdapter(Context context, List<String> victims) {
        this.context = context;
        this.victims = victims;
    }

    @Override
    public int getCount() {
        return victims.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.all_victims_layout,null);

        TextView tvName = (TextView)view.findViewById(R.id.textViewVName);

        tvName.setText(victims.get(position));

        return view;
    }
}
