package com.iphonezoo.cpapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class AddNewInvestigatorActivity extends AppCompatActivity {

    EditText etXmanID;
    EditText etXmanName;
    EditText etXmanContact;
    //   EditText etArea;
    EditText etAddress;
    EditText etVehicleNo;
    Button register;
    Button btGetLatLong;
    Button btPickImage;

    ImageView ivUploadPic;

    GoogleMap googleMap;
    MarkerOptions markerOptions;
    LatLng latLng;
    Double lat;
    Double lng;

    Bitmap image;

    List<String> typeList = new ArrayList<>();
    List<String> policeServiceList = new ArrayList<>();
    List<String> hospitalServiceList = new ArrayList<>();
    List<String> ngoServiceList = new ArrayList<>();
    List<String> firebrigadeServiceList = new ArrayList<>();
    List<String> mainServiceList = new ArrayList<>();


    Spinner spType;
    String typeSelected;
    int zoneID;
    String zoneName;
    String stateName;
    String districtName;

    // Image loading result to pass to startActivityForResult method.
    private static int LOAD_IMAGE_RESULTS = 1;


    // ListView lvServicesProvided;
    //  Button btGetSelectedServices;
    //  ServiceListAdapter serviceListAdapter;

    ProgressDialog progressDialog;

    SupportMapFragment supportMapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_investigator);

        zoneID = getIntent().getIntExtra("zoneID", 0);
        zoneName = getIntent().getStringExtra("zoneSelected");
        stateName = getIntent().getStringExtra("stateSelected");
        districtName = getIntent().getStringExtra("districtSelected");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");

        policeServiceList.add("A");
        policeServiceList.add("b");
        policeServiceList.add("c");
        policeServiceList.add("d");

        hospitalServiceList.add("h");
        hospitalServiceList.add("i");
        hospitalServiceList.add("j");
        hospitalServiceList.add("k");

        ngoServiceList.add("L");
        ngoServiceList.add("m");
        ngoServiceList.add("n");
        ngoServiceList.add("o");

        firebrigadeServiceList.add("p");
        firebrigadeServiceList.add("q");
        firebrigadeServiceList.add("r");
        firebrigadeServiceList.add("s");

        etXmanID = (EditText) findViewById(R.id.editTextXmanID);
        etXmanName = (EditText) findViewById(R.id.editTextXmanName);
        etXmanContact = (EditText) findViewById(R.id.editTextXmanContact);
        //   etArea = (EditText) findViewById(R.id.editTextArea);
        etAddress = (EditText) findViewById(R.id.editTextAddress);
        etVehicleNo = (EditText) findViewById(R.id.editTextVehicleNo);

        register = (Button) findViewById(R.id.registerButton);
        btGetLatLong = (Button) findViewById(R.id.buttonGetLatLong);
        btPickImage = (Button) findViewById(R.id.buttonPickPhoto);

        ivUploadPic = (ImageView) findViewById(R.id.imageViewUploadPic);
        spType = (Spinner) findViewById(R.id.spinnerType);

        //  lvServicesProvided = (ListView)findViewById(R.id.listViewServicesProvided);

        //   btGetSelectedServices = (Button)findViewById(R.id.buttonGetSelectedServices);

        typeList.add("Police");
        typeList.add("NGO");
        typeList.add("Hospital");
        typeList.add("Fire Brigade");
        typeList.add("Family");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddNewInvestigatorActivity.this, android.R.layout.simple_list_item_1, typeList);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spType.setAdapter(adapter);

        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                typeSelected = typeList.get(position);

                Log.d("Registration", "typeSelected = " + typeSelected);

          /*      mainServiceList.clear();

                if(typeSelected.equalsIgnoreCase("Police")) {

                    mainServiceList.addAll(policeServiceList);
                    serviceListAdapter.notifyDataSetChanged();

                } else if (typeSelected.equalsIgnoreCase("Hospital")){

                    mainServiceList.addAll(hospitalServiceList);
                    serviceListAdapter.notifyDataSetInvalidated();

                }
                else if (typeSelected.equalsIgnoreCase("NGO")){

                    mainServiceList.addAll(ngoServiceList);
                    serviceListAdapter.notifyDataSetChanged();

                }else if (typeSelected.equalsIgnoreCase("Fire Brigade")){

                    mainServiceList.addAll(firebrigadeServiceList);
                    serviceListAdapter.notifyDataSetChanged();
                }
                */
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

                Toast.makeText(AddNewInvestigatorActivity.this, "Plz Select Type", Toast.LENGTH_SHORT).show();

            }
        });


      /*  serviceListAdapter = new ServiceListAdapter(AddNewInvestigatorActivity.this,mainServiceList);
        lvServicesProvided.setAdapter(serviceListAdapter);

        btGetSelectedServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               List<String> sList = serviceListAdapter.services;

                Log.d("ServicesProvided","sList = "+sList);

                Toast.makeText(AddNewInvestigatorActivity.this, "Items Selected = ", Toast.LENGTH_LONG).show();

            }
        });

        */

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable()) {

                    progressDialog.setMessage("\t Registering...");
                    progressDialog.show();

                    ParseObject newInvestigator = new ParseObject("GeoXman");

                    String XManID = etXmanID.getText().toString();
                    String xmanName = etXmanName.getText().toString();
                    String XManContactNum = etXmanContact.getText().toString();
                    String XManVehicleNo = etVehicleNo.getText().toString();

                    if (!XManID.equals("")) {

                        if (!xmanName.equals("")) {

                            if (!XManContactNum.equals("")) {


                                if (!XManVehicleNo.equals("")) {


                                    newInvestigator.put("XManID", etXmanID.getText().toString());

                                    //  Log.d("GeoXMan", "RegistratioActivity : name =" + name);
                                    newInvestigator.put("XManName", etXmanName.getText().toString());
                                    newInvestigator.put("XManType", typeSelected);
                                    newInvestigator.put("XManContactNum", "+91" + etXmanContact.getText().toString());
                                    newInvestigator.put("XManAddress", etAddress.getText().toString());
                                    newInvestigator.put("XManVehicleNo", etVehicleNo.getText().toString());
                                    newInvestigator.put("City", zoneName);
                                    newInvestigator.put("ZoneName", zoneName);
                                    newInvestigator.put("ZoneID", zoneID);
                                    newInvestigator.put("State", stateName);
                                    newInvestigator.put("District", districtName);

                                    newInvestigator.put("isVerified", false);

                                    if (lat != null && lat != 0) {

                                        ParseGeoPoint point = new ParseGeoPoint(lat, lng);

                                        newInvestigator.put("location", point);

                                        //     newInvestigator.put("AreaCovered", etArea.getText().toString());

                                        if (image != null) {

                                            ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                                            image.compress(Bitmap.CompressFormat.JPEG, 75, stream1);
                                            byte[] image1 = stream1.toByteArray();
                                            System.out.println("size after compression jpeg = " + stream1.toByteArray().length);

                                            // Create the ParseFile
                                            //  ParseFile file = new ParseFile("emergencyImage.png", image);
                                            ParseFile file = new ParseFile("ProfilePic.jpeg", image1);

                                            // Upload the image into Parse Cloud
                                            try {

                                                file.save();

                                            } catch (ParseException e) {
                                                Log.d("Image", "Image upload : failed to upload image file");
                                                e.printStackTrace();
                                            }

                                            newInvestigator.put("Pic", file);
                                        } else {
                                            Toast.makeText(AddNewInvestigatorActivity.this, "unable to upload image", Toast.LENGTH_SHORT).show();
                                        }


                                        newInvestigator.saveInBackground(new SaveCallback() {
                                            @Override
                                            public void done(ParseException e) {

                                                progressDialog.dismiss();

                                                if (e == null) {

                                                    Log.i("XMAn", "investigator registered");

                                                    Toast.makeText(getApplicationContext(), "Registered Successfully", Toast.LENGTH_LONG).show();

                                                    etXmanContact.setText("");
                                                    etXmanName.setText("");
                                                    etXmanID.setText("");
                                                    etVehicleNo.setText("");


                                                } else {

                                                    Toast.makeText(getApplicationContext(), "Registration Failed try again", Toast.LENGTH_LONG).show();
                                                    e.printStackTrace();

                                                }
                                            }
                                        });

                                    } else {

                                        progressDialog.dismiss();

                                        Toast.makeText(AddNewInvestigatorActivity.this, "Please select your location", Toast.LENGTH_SHORT).show();

                                    }
                                } else {

                                    progressDialog.dismiss();

                                    Toast.makeText(AddNewInvestigatorActivity.this, "Please enter vehicle number", Toast.LENGTH_SHORT).show();

                                }
                            } else {

                                progressDialog.dismiss();

                                Toast.makeText(AddNewInvestigatorActivity.this, "Please enter contact number", Toast.LENGTH_SHORT).show();

                            }
                        } else {

                            progressDialog.dismiss();

                            Toast.makeText(AddNewInvestigatorActivity.this, "Please enter name", Toast.LENGTH_SHORT).show();

                        }
                    } else {

                        progressDialog.dismiss();

                        Toast.makeText(AddNewInvestigatorActivity.this, "Please enter id", Toast.LENGTH_SHORT).show();

                    }
                }else {

                    Toast.makeText(AddNewInvestigatorActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                }
            }
        });

        btGetLatLong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(AddNewInvestigatorActivity.this);
                dialog.setContentView(R.layout.map_dialog_layout);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setTitle("Select Your Location");

                supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapDialogBox);

                supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap map) {

                        googleMap = map;

                        if (ActivityCompat.checkSelfPermission(AddNewInvestigatorActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(AddNewInvestigatorActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        googleMap.setMyLocationEnabled(true);


                        // Setting a click event handler for the map
                        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                            @Override
                            public void onMapClick(LatLng arg0) {
                                if (arg0 != null) {
                                    latLng = arg0;
                                } else {
                                    latLng = new LatLng(0, 0);
                                }
                                googleMap.clear();
                                googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                                markerOptions = new MarkerOptions();
                                markerOptions.position(latLng);
                                googleMap.addMarker(markerOptions);
                            }
                        });



                    }
                });


                Button ib = (Button) dialog.findViewById(R.id.buttonSubmitMapDialog);
                ib.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {

                            lat = latLng.latitude;
                            lng = latLng.longitude;

                            Log.d("MAPDialog", "Lat Long = " + lat + " , " + lng);

                        } catch (Exception e) {

                            Log.d("MAPDialog", "Failed to get Lat long from map");
                            lat = 0.0;
                            lng = 0.0;
                        }

                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ft.remove(supportMapFragment);
                        ft.commit();
                        dialog.dismiss();
                    }
                });

                dialog.show();

            }

        });

        btPickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Create the Intent for Image Gallery.
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                // Start new activity with the LOAD_IMAGE_RESULTS to handle back the results when image is picked from the Image Gallery.
                startActivityForResult(i, LOAD_IMAGE_RESULTS);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_new_investigator, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Here we need to check if the activity that was triggers was the Image Gallery.
        // If it is the requestCode will match the LOAD_IMAGE_RESULTS value.
        // If the resultCode is RESULT_OK and there is some data we know that an image was picked.
        if (requestCode == LOAD_IMAGE_RESULTS && resultCode == RESULT_OK && data != null) {
            // Let's read picked image data - its URI
            Uri pickedImage = data.getData();
            // Let's read picked image path using content resolver
            String[] filePath = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

            Log.d("Registration","imagePath = "+imagePath);

            //  String[] imgPath = imagePath.split("/");

            //   String imageName = imgPath[imgPath.length-1];

            //   Log.d("Registration","imageName = "+imageName);


            // Now we need to set the GUI ImageView data with data read from the picked file.

            image = BitmapFactory.decodeFile(imagePath);

            ivUploadPic.setImageBitmap(image);

//            tvImagePath.setText(imageName);

            // At the end remember to close the cursor or you will end with the RuntimeException!
            cursor.close();
        }
    }



    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }


}
