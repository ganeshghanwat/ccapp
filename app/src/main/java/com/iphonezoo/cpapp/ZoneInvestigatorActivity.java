package com.iphonezoo.cpapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class ZoneInvestigatorActivity extends AppCompatActivity {

    ListView lvInvestigators;
    EditText etSearch;

    int zoneID;

    AllInvestigatorListAdapter adapter;

    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone_investigator);

        zoneID =   getIntent().getIntExtra("ZoneID",0);

        mProgressDialog = new ProgressDialog(this);

        lvInvestigators = (ListView)findViewById(R.id.listViewZoneInvestigators);
        etSearch = (EditText)findViewById(R.id.editTextZoneInvestSearch);

        if (isNetworkAvailable()) {

            new GetInvestigatorDataTask().execute();
        }else {
            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
        }

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                adapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        lvInvestigators.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                GeoXman geoXman = (GeoXman)parent.getItemAtPosition(position);

                Intent intent = new Intent(ZoneInvestigatorActivity.this,InvestigatorDetailsActivity.class);
                intent.putExtra("name",geoXman.getName());
                intent.putExtra("contact",geoXman.getContact());
                intent.putExtra("zone",geoXman.getZoneName());
                intent.putExtra("zoneid",geoXman.getZoneID());
                intent.putExtra("state",geoXman.getState());
                intent.putExtra("address",geoXman.getAddress());
                intent.putExtra("type",geoXman.getType());
                startActivity(intent);

            }
        });


    }


    //Async task..
    private class GetInvestigatorDataTask extends AsyncTask<String, Void, Void> {


        List<GeoXman> geoXmanList = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {

            ParseQuery query = ParseQuery.getQuery("GeoXman");
            query.whereEqualTo("ZoneID",zoneID);
           // query.whereEqualTo("ZoneName",selectedZoneName);
            query.setLimit(1000);

            try {

                List<ParseObject> parseObjectList = query.find();

                for (ParseObject parseObject : parseObjectList){

                    GeoXman geoXman = new GeoXman();

                    String id = parseObject.getString("XManID");
                    String name = parseObject.getString("XManName");
                    String type = parseObject.getString("XManType");
                    String contactNum = parseObject.getString("XManContactNum");
                    ParseGeoPoint location = parseObject.getParseGeoPoint("location");
                    String area = parseObject.getString("AreaCovered");
                    String address = parseObject.getString("XManAddress");
                    String zoneName = parseObject.getString("ZoneName");
                    int zoneId = parseObject.getInt("ZoneID");
                    String state = parseObject.getString("State");

                    String imageURL = "";

                    try {

                        imageURL = parseObject.getParseFile("Pic").getUrl();

                    } catch (NullPointerException e) {

                        imageURL = "";

                    }


                    geoXman.setId(id);
                    geoXman.setName(name);
                    geoXman.setType(type);
                    geoXman.setContact(contactNum);
                    geoXman.setLocation(location);
                    geoXman.setArea(area);
                    geoXman.setAddress(address);
                    geoXman.setZoneName(zoneName);
                    geoXman.setZoneID(zoneId);
                    geoXman.setState(state);
                    geoXman.setImgURL(imageURL);

                    geoXmanList.add(geoXman);

                }

            } catch (ParseException e) {

                Log.d("GeoXman", "Failed to find investigators");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            adapter = new AllInvestigatorListAdapter(ZoneInvestigatorActivity.this,geoXmanList);
            lvInvestigators.setAdapter(adapter);

            if (geoXmanList.size()<1){
                Toast.makeText(ZoneInvestigatorActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
            }

            mProgressDialog.dismiss();

        }

    }

    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }


}
