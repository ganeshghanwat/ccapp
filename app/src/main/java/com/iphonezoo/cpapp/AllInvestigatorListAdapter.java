package com.iphonezoo.cpapp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;


public class AllInvestigatorListAdapter extends BaseAdapter implements Filterable {

    Context context;
    List<GeoXman> geoXman;
    List<GeoXman> geoXmanOrigin;



    public AllInvestigatorListAdapter(Context context, List<GeoXman> geoXman) {
        this.context = context;
        this.geoXman = geoXman;
        this.geoXmanOrigin = new ArrayList<>();
        this.geoXmanOrigin.addAll(geoXman);

        Log.d("ZoneAdapter","geoXman size" +geoXman.size());
        Log.d("ZoneAdapter","geoXmanOrigin size" +geoXmanOrigin.size());



    }

    @Override
    public int getCount() {
        return geoXman.size();
    }

    @Override
    public Object getItem(int position) {
        return geoXman.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        GeoXman g = geoXman.get(position);

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView = inflater.inflate(R.layout.investgator_list_layout,null);

        TextView tvName = (TextView)convertView.findViewById(R.id.textViewName);
        TextView tvType = (TextView)convertView.findViewById(R.id.textViewType);
        TextView tvAddress = (TextView)convertView.findViewById(R.id.textViewZoneID);
        TextView tvZone = (TextView)convertView.findViewById(R.id.textViewZone);

        ImageView imPic = (ImageView) convertView.findViewById(R.id.imageViewPic);


        tvName.setText(g.getName());
        tvType.setText(g.getType());
        tvAddress.setText(g.getAddress());
        tvZone.setText(g.getZoneName());

        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .build();

        if(!g.getImgURL().equals("")) {

            imageLoader.displayImage(g.getImgURL(), imPic, options);
        }


        return convertView;
    }


    public void filter(String charText) {
         charText = charText.toLowerCase();
         geoXman.clear();
         if (charText.length() == 0) {
             geoXman.addAll(geoXmanOrigin);
             } else {
             for (GeoXman xman : geoXmanOrigin) {
                 if (xman.getName().toLowerCase().contains(charText)) {
                     geoXman.add(xman);
                     }
                 }
             }
         notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,FilterResults results) {

                geoXman = (List<GeoXman>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                List<GeoXman> FilteredArrList = new ArrayList<GeoXman>();

                if (geoXmanOrigin == null) {
                    geoXmanOrigin = new ArrayList<GeoXman>(geoXman); // saves the original data in mOriginalValues
                }

                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = geoXmanOrigin.size();
                    results.values = geoXmanOrigin;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < geoXmanOrigin.size(); i++) {
                        String data = geoXmanOrigin.get(i).getName();
                        if (data.toLowerCase().startsWith(constraint.toString())) {

                            GeoXman g = new GeoXman();
                            g.setName(geoXmanOrigin.get(i).getName());
                            g.setZoneID(geoXmanOrigin.get(i).getZoneID());
                            g.setZoneName(geoXmanOrigin.get(i).getZoneName());
                            g.setAddress(geoXmanOrigin.get(i).getAddress());
                            g.setArea(geoXmanOrigin.get(i).getArea());
                            g.setContact(geoXmanOrigin.get(i).getContact());
                            g.setId(geoXmanOrigin.get(i).getId());
                            g.setLocation(geoXmanOrigin.get(i).getLocation());
                            g.setType(geoXmanOrigin.get(i).getType());
                            g.setImgURL(geoXmanOrigin.get(i).getImgURL());


                            FilteredArrList.add(g);
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };

        return filter;
    }
}
