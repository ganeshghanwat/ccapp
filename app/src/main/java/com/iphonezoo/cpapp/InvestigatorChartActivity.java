package com.iphonezoo.cpapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class InvestigatorChartActivity extends AppCompatActivity {

    List<Case> caseList = new ArrayList<>();

    ProgressDialog mProgressDialog;

    String investContact;

    Spinner spinner;

    List<String> spinnerList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_investigator_chart);

        spinner = (Spinner)findViewById(R.id.spinnerInvestigatorChart);

        investContact = getIntent().getStringExtra("contact");

        mProgressDialog = new ProgressDialog(this);

        spinnerList.add("Daily");
        spinnerList.add("Weekly");

        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerList);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // categoryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(stateAdapter);

        if (isNetworkAvailable()) {

            new GetVictimsData().execute();
        }else {

            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
        }


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                String item = spinnerList.get(position);

                if (item.equalsIgnoreCase("Daily")) {

                    displayChartDaily(caseList);

                } else if (item.equalsIgnoreCase("Weekly")) {

                    displayChartWeekly(caseList);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });





    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_investigator_chart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    ///Async Task

    private class GetVictimsData extends AsyncTask<Void,Void,Void> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

        }


        @Override
        protected Void doInBackground(Void... params) {

            ParseQuery investigatorQuery = ParseQuery.getQuery("GeoXman");
            investigatorQuery.whereEqualTo("XManContactNum",investContact);
            investigatorQuery.setLimit(1000);

            ParseQuery query = ParseQuery.getQuery("Case");
            query.whereMatchesQuery("GeoXman", investigatorQuery);
            query.setLimit(1000);
            query.addAscendingOrder("createdAt");

            try {


                List<ParseObject> parseObjectList = query.find();

                Log.d("CaseData", "parseObjectList size  = " + parseObjectList.size());

                for (ParseObject parseObject : parseObjectList){

                    Case c = new Case();

                    JSONArray location = parseObject.getJSONArray("Locations");

                    Log.d("CaseData","location jsonArray = "+location);

                    JSONObject jsonObject =  location.getJSONObject(0);

                    double latitude = jsonObject.getDouble("latitude");
                    double longitude = jsonObject.getDouble("longitude");
                    String vName = parseObject.getString("VName");
                    String sImage = parseObject.getString("SImage");
                    String vAddress = parseObject.getString("VAddress");
                    String vContact = parseObject.getString("VContact");
                    String vEmail = parseObject.getString("VEmail");
                    String vImage = parseObject.getString("VImage");
                    String vParents = parseObject.getString("VParents");
                    String vReport = parseObject.getString("VReport");
                    String crimeCategory = parseObject.getString("CrimeCategory");
                    String financialLoss = parseObject.getString("FinancialLoss");
                    String humansAffected = parseObject.getString("HumansAffected");
                    String humansKilled = parseObject.getString("HumansKilled");
                    String noOfSuspects = parseObject.getString("NoOfSuspects");
                    String evidenceAudio = parseObject.getString("evidenceAudio");
                    String evidenceImage = parseObject.getString("evidenceImage");
                    String evidenceVideo = parseObject.getString("evidenceVideo");
                    String invest_contact = parseObject.getString("invest_contact");
                    String invest_name = parseObject.getString("invest_name");
                    String invest_insta_id = parseObject.getString("invest_insta_id");
                    String status = parseObject.getString("status");
                    Date date = parseObject.getCreatedAt();

                    SimpleDateFormat sdf = new SimpleDateFormat("dd:MM:yyyy");

                    String d = sdf.format(date);

                    Log.d("Case"," String date d = "+d);

                    Date d1 = sdf.parse(d);

                    Log.d("Case"," final date d1 = "+d1);

                    Log.d("Case","Latitude = "+latitude+" longitude = "+longitude);


                    c.setLocationPoint(new ParseGeoPoint(latitude, longitude));
                    c.setLocations(location);
                    c.setvName(vName);
                    c.setvImage(vImage);
                    c.setsImage(sImage);
                    c.setAudios(evidenceAudio);
                    c.setFinancialLoss(financialLoss);
                    c.setHumansAffected(humansAffected);
                    c.setHumansKilled(humansKilled);
                    c.setInvestContact(invest_contact);
                    c.setInvestInstaID(invest_insta_id);
                    c.setInvestName(invest_name);
                    c.setNoOfSuspect(noOfSuspects);
                    c.setStatus(status);
                    c.setImages(evidenceImage);
                    c.setvAddress(vAddress);
                    c.setvContact(vContact);
                    c.setvParents(vParents);
                    c.setvEmail(vEmail);
                    c.setVideos(evidenceVideo);
                    c.setvReport(vReport);
                    c.setCrimeCategory(crimeCategory);
                    c.setCreatedAt(d1);

                    caseList.add(c);

                }

            } catch (ParseException e) {

                e.printStackTrace();

            } catch (JSONException e) {

                e.printStackTrace();
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mProgressDialog.dismiss();

            displayChartDaily(caseList);

            //  displayChartWeekly(caseList);
        }
    }



    public void displayChartDaily(List<Case> caseList){

        Map<Date,ChartData> hashMap = new TreeMap<>();

        if (caseList.size() >= 1) {

            for (Case c : caseList) {

                Date d = c.getCreatedAt();

                Log.d("Case","d = "+d);

                if (hashMap.containsKey(d)){

                    Log.d("Case","duplicate record...."+d);

                    ChartData chartData = hashMap.get(d);

                    if (c.getStatus().equalsIgnoreCase("Successful")){

                        int success = chartData.getSuccess()+1;
                        int failed = chartData.getFail()+0;
                        chartData.setSuccess(success);
                        chartData.setFail(failed);

                    }else{

                        int failed = chartData.getFail()+1;
                        int success = chartData.getSuccess()+0;
                        chartData.setFail(failed);
                        chartData.setSuccess(success);
                    }
                    hashMap.put(d,chartData);

                }else {

                    Log.d("Case","new record...."+d);

                    ChartData chartData = new ChartData();

                    if (c.getStatus().equalsIgnoreCase("Successful")){

                        Log.d("Case","status....= success  "+c.getStatus());

                        chartData.setSuccess(1);
                        chartData.setFail(0);

                    }else{

                        Log.d("Case","status....= Failed "+c.getStatus());

                        chartData.setFail(1);
                        chartData.setSuccess(0);
                    }

                    hashMap.put(d,chartData);
                }
            }

            Log.d("Case","hashMap"+hashMap.toString());
            Log.d("Case","hashMap"+hashMap);


            String s = "['Date', 'Success', 'Failed']";

            Iterator<Date> iterator = hashMap.keySet().iterator();

            while(iterator.hasNext()){

                Date key = iterator.next();

                Log.d("Case"," Iterator date = "+key);

                String a = ",[";

                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
                String date1 = sdf.format(key);

                a = a +"'"+ date1+"'"+","+ hashMap.get(key).getSuccess()+ ","+ hashMap.get(key).getFail()+"]";

                //  a = a + sales.get(i).getYear()+","+sales.get(i).getSales()+","+sales.get(i).getExpences()+"]";

                Log.d("Case "," Chart data a = "+a);

                s=s+a;

            }

            Log.d("Case "," Chart data s = "+s);

            WebView webview = (WebView) findViewById(R.id.webViewInvestigatorChart);
            String content = "<html>"
                    + "  <head>"
                    + "    <script type=\"text/javascript\" src=\"jsapi.js\"></script>"
                    + "    <script type=\"text/javascript\">"
                    + "      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});"
                    + "      google.setOnLoadCallback(drawChart);"
                    + "      function drawChart() {"
                    + "        var data = google.visualization.arrayToDataTable(["

                    +s

               /* + "          ['Year', 'Sales', 'Expenses'],"
                + "          ['2010',  1000,      400],"
                + "          ['2011',  1170,      460],"
                + "          ['2012',  660,       1120],"
                + "          ['2013',  1030,      540]"

                */

                    + "        ]);"
                    + "        var options = {"
                    + "          title: 'Emergency Events',"
                    + "          hAxis: {title: 'Date', titleTextStyle: {color: 'red'}}"
                    + "        };"
                    + "        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));"
                    + "        chart.draw(data, options);"
                    + "      }"
                    + "    </script>"
                    + "  </head>"
                    + "  <body>"
                    + "    <div id=\"chart_div\" style=\"width: 1000px; height: 500px;\"></div>"
                    + "  </body>" + "</html>";

            WebSettings webSettings = webview.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webview.requestFocusFromTouch();
            webview.loadDataWithBaseURL("file:///android_asset/", content, "text/html", "utf-8", null);
            //webview.loadUrl("file:///android_asset/Code.html"); // Can be used in this way too.
            webview.setInitialScale(1);
            webview.getSettings().setLoadWithOverviewMode(true);
            webview.getSettings().setUseWideViewPort(true);
            webview.getSettings().setBuiltInZoomControls(true);


        }else {

           // Toast.makeText(InvestigatorChartActivity.this, "Nothing to Display", Toast.LENGTH_SHORT).show();

        }

    }

    public void displayChartWeekly(List<Case> caseList){

        Map<Integer,ChartData> hashMap = new TreeMap<>();

        if (caseList.size() >= 1) {

            for (Case c : caseList) {

                Date d = c.getCreatedAt();

                Log.d("Case", "d = " + d);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(d);

                int week = calendar.get(Calendar.WEEK_OF_YEAR);

                Log.d("Case","week  = "+week);


                if (hashMap.containsKey(week)){

                    Log.d("Case","duplicate record...."+week);

                    ChartData chartData = hashMap.get(week);

                    if (c.getStatus().equalsIgnoreCase("Successful")){

                        Log.d("Case","status....= success  "+c.getStatus());

                        int success = chartData.getSuccess()+1;
                        int failed = chartData.getFail()+0;
                        chartData.setSuccess(success);
                        chartData.setFail(failed);

                    }else{

                        Log.d("Case","status....= Failed "+c.getStatus());

                        int failed = chartData.getFail()+1;
                        int success = chartData.getSuccess()+0;
                        chartData.setFail(failed);
                        chartData.setSuccess(success);
                    }
                    hashMap.put(week,chartData);

                }else {

                    Log.d("Case","new record...."+week);

                    ChartData chartData = new ChartData();

                    if (c.getStatus().equalsIgnoreCase("Successful")){

                        Log.d("Case","status....= success  "+c.getStatus());

                        chartData.setSuccess(1);
                        chartData.setFail(0);

                    }else{

                        Log.d("Case","status....= Failed "+c.getStatus());


                        chartData.setFail(1);
                        chartData.setSuccess(0);
                    }
                    hashMap.put(week,chartData);
                }
            }

            Log.d("Case","hashMap"+hashMap.toString());
            Log.d("Case","hashMap"+hashMap);



            String s = "['Date', 'Success', 'Failed']";

            Iterator<Integer> iterator = hashMap.keySet().iterator();

            while(iterator.hasNext()){

                int key = iterator.next();

                Log.d("Case"," Iterator week = "+key);

                String a = ",[";

                int w = key;
                int year = 2016;

                // Get calendar, clear it and set week number and year.
                Calendar calendarr = Calendar.getInstance();

                calendarr.set(Calendar.WEEK_OF_YEAR, w);
                calendarr.set(Calendar.YEAR, year);
                // Now get the first day of week.
                Date dateFirst = calendarr.getTime();

                calendarr.add(Calendar.DATE,6);
                Date dateLast = calendarr.getTime();

                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
                String date1 = sdf.format(dateFirst);
                String date2 = sdf.format(dateLast);

                a = a +"'"+ date1+" - "+ date2 +"'"+","+ hashMap.get(key).getSuccess()+ ","+ hashMap.get(key).getFail()+"]";

                //  a = a + sales.get(i).getYear()+","+sales.get(i).getSales()+","+sales.get(i).getExpences()+"]";

                Log.d("Case "," Chart data a = "+a);

                s=s+a;

            }



            Log.d("Case "," Chart data s = "+s);

            WebView webview = (WebView) findViewById(R.id.webViewInvestigatorChart);
            String content = "<html>"
                    + "  <head>"
                    + "    <script type=\"text/javascript\" src=\"jsapi.js\"></script>"
                    + "    <script type=\"text/javascript\">"
                    + "      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});"
                    + "      google.setOnLoadCallback(drawChart);"
                    + "      function drawChart() {"
                    + "        var data = google.visualization.arrayToDataTable(["

                    +s

               /* + "          ['Year', 'Sales', 'Expenses'],"
                + "          ['2010',  1000,      400],"
                + "          ['2011',  1170,      460],"
                + "          ['2012',  660,       1120],"
                + "          ['2013',  1030,      540]"

                */

                    + "        ]);"
                    + "        var options = {"
                    + "          title: 'Emergency Events',"
                    + "          hAxis: {title: 'Week of Year', titleTextStyle: {color: 'red'}}"
                    + "        };"
                    + "        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));"
                    + "        chart.draw(data, options);"
                    + "      }"
                    + "    </script>"
                    + "  </head>"
                    + "  <body>"
                    + "    <div id=\"chart_div\" style=\"width: 1000px; height: 500px;\"></div>"
                    + "  </body>" + "</html>";

            WebSettings webSettings = webview.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webview.requestFocusFromTouch();
            webview.loadDataWithBaseURL("file:///android_asset/", content, "text/html", "utf-8", null);
            //webview.loadUrl("file:///android_asset/Code.html"); // Can be used in this way too.
            webview.setInitialScale(1);
            webview.getSettings().setLoadWithOverviewMode(true);
            webview.getSettings().setUseWideViewPort(true);
            webview.getSettings().setBuiltInZoomControls(true);


        }else {

          //  Toast.makeText(InvestigatorChartActivity.this, "Nothing to Display", Toast.LENGTH_SHORT).show();

        }

    }

    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }



}
