package com.iphonezoo.cpapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;
import org.json.JSONException;
import org.json.JSONObject;


public class CustomPushBroadcastReceiver extends ParsePushBroadcastReceiver {

    SharedPreferences sharedPreferences;

    @Override
    public void onReceive(Context context,Intent intent){
            super.onReceive(context, intent);

        sharedPreferences = context.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
    }


    @Override
    protected void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);


    }


    @Override
    public void onPushOpen(Context context,Intent intent){
        super.onPushOpen(context, intent);

        Log.d("Notification", "CustomPushBroadcastReceiver : onPushOpen() fired.....");

        ParseAnalytics.trackAppOpenedInBackground(intent);

        System.out.println(intent.getStringExtra("com.parse.Data"));

        try {
            JSONObject jsonObject = new JSONObject(intent.getStringExtra("com.parse.Data"));

            String notificationID = jsonObject.getString("notificationID");

            if(notificationID.equals("needHelp")){

                Intent  i = new Intent(context,NeedHelpNotificationActivity.class);
                i.putExtras(intent.getExtras());
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
