package com.iphonezoo.cpapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;


public class StateInvestigatorsFragment extends Fragment {

    EditText etSearch;
    ListView lvInvestigator;

    AllInvestigatorListAdapter adapter;

    String stateSelected;

    ProgressDialog mProgressDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        stateSelected = ZonesTabbedActivity.getStateSelected();
        mProgressDialog = new ProgressDialog(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_state_investigators, container, false);


        etSearch = (EditText)view.findViewById(R.id.editTextStateInvestSearch);
        lvInvestigator = (ListView)view.findViewById(R.id.listViewStateInvestTab);

        new GetInvestigatorDataTask().execute();

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                adapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        lvInvestigator.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                GeoXman geoXman = (GeoXman) parent.getItemAtPosition(position);
                Intent intent = new Intent(getActivity(),InvestigatorDetailsActivity.class);
                intent.putExtra("name",geoXman.getName());
                intent.putExtra("contact",geoXman.getContact());
                intent.putExtra("zone",geoXman.getZoneName());
                intent.putExtra("zoneid",geoXman.getZoneID());
                intent.putExtra("state",geoXman.getState());
                intent.putExtra("address",geoXman.getAddress());
                intent.putExtra("type",geoXman.getType());
                startActivity(intent);

            }
        });



        return view;
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }




    //Async task..
    private class GetInvestigatorDataTask extends AsyncTask<String, Void, Void> {


        List<GeoXman> geoXmanList = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {

            Log.d("GeoXman","state Selected = "+stateSelected);

            ParseQuery query = ParseQuery.getQuery("GeoXman");
            query.whereEqualTo("State", stateSelected);
          //  query.whereEqualTo("ZoneID",selectedZoneID);
         //   query.whereEqualTo("ZoneName",selectedZoneName);

            query.setLimit(1000);

            try {

                List<ParseObject> parseObjectList = query.find();

                Log.d("GeoXman","parseObjectList size = "+parseObjectList.size());


                for (ParseObject parseObject : parseObjectList){

                    GeoXman geoXman = new GeoXman();

                    String id = parseObject.getString("XManID");
                    String name = parseObject.getString("XManName");
                    String type = parseObject.getString("XManType");
                    String contactNum = parseObject.getString("XManContactNum");
                    ParseGeoPoint location = parseObject.getParseGeoPoint("location");
                    String area = parseObject.getString("AreaCovered");
                    String address = parseObject.getString("XManAddress");
                    String zoneName = parseObject.getString("ZoneName");
                    int zoneId = parseObject.getInt("ZoneID");
                    String state = parseObject.getString("State");

                    String imageURL = "";

                    try {

                        imageURL = parseObject.getParseFile("Pic").getUrl();

                    } catch (NullPointerException e) {

                        imageURL = "";

                    }

                    geoXman.setId(id);
                    geoXman.setName(name);
                    geoXman.setType(type);
                    geoXman.setContact(contactNum);
                    geoXman.setLocation(location);
                    geoXman.setArea(area);
                    geoXman.setAddress(address);
                    geoXman.setZoneName(zoneName);
                    geoXman.setZoneID(zoneId);
                    geoXman.setState(state);
                    geoXman.setImgURL(imageURL);


                    geoXmanList.add(geoXman);

                }

            } catch (ParseException e) {

                Log.d("GeoXman", "Failed to find investigators");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            adapter = new AllInvestigatorListAdapter(getActivity(),geoXmanList);
            lvInvestigator.setAdapter(adapter);

            mProgressDialog.dismiss();

        }

    }


}
