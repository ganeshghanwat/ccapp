package com.iphonezoo.cpapp.Activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.iphonezoo.cpapp.Case;
import com.iphonezoo.cpapp.ChartData;
import com.iphonezoo.cpapp.R;
import com.iphonezoo.cpapp.StateSelectionActivity;
import com.iphonezoo.cpapp.Zone;
import com.iphonezoo.cpapp.ZoneListAdapter;
import com.iphonezoo.cpapp.ZonesTabFragment;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class AllChartsActivity extends AppCompatActivity {

    List<String> districtList = new ArrayList<>();
  //  List<Zone> zoneList = new ArrayList<>();
    List<String> zoneListString = new ArrayList<>();

    ProgressDialog mProgressDialog;

    LinearLayout layoutAdvanceOpt;
    Spinner spState;
    Spinner spDistrict;
    Spinner spZone;

    TextView tvAdvanceOpt;


    String stateSelected;

    List<String> stateList = new ArrayList<>();

    List<String> staticStateList = new ArrayList<>(Arrays.asList("West Bengal","Uttarakhand", "Uttar Pradesh", "Tripura", "Telangana", "Tamil Nadu", "Sikkim", "Rajasthan", "Punjab", "Pondicherry", "Odisha","Nagaland", "Mizoram", "Maharashtra", "Meghalaya", "Madhya Pradesh", "Karnataka", "Jharkhand", "Jammu & Kashmir", "Himachal Pradesh", "Haryana", "Gujarat","Goa",
            "Daman & Diu", "Chattisgarh", "Bihar", "Assam", "Arunachal Pradesh", "Andhra Pradesh", "Andaman & Nicobar Islands"));

    boolean isAdvanceOpt = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_all_charts);

        mProgressDialog = new ProgressDialog(this);

        layoutAdvanceOpt = (LinearLayout)findViewById(R.id.linearLayoutAdvanceOpt);
        spState = (Spinner)findViewById(R.id.spinnerStateCharts);
        spDistrict = (Spinner)findViewById(R.id.spinnerDistrictsCharts);
        spZone = (Spinner)findViewById(R.id.spinnerZonesCharts);

        tvAdvanceOpt = (TextView)findViewById(R.id.textViewAdvanceOpt);


        layoutAdvanceOpt.setVisibility(View.GONE);

        Collections.sort(staticStateList);

        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(AllChartsActivity.this, android.R.layout.simple_spinner_item, staticStateList);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // categoryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spState.setAdapter(stateAdapter);


        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //    Log.d("EZones", "State selected = " + stateList.get(position));

                //   stateSelected = stateList.get(position);

                stateSelected = staticStateList.get(position);

                if (isNetworkAvailable()) {

                    if (isAdvanceOpt) {

                        PrepareDistrictSpinner(stateSelected);

                    } else {

                        // load web view with chart

                        new GetVictimsDataState(stateSelected).execute();

                    }

                }else {

                    Toast.makeText(AllChartsActivity.this, "You are not connected to internet", Toast.LENGTH_SHORT).show();
                    
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        tvAdvanceOpt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isAdvanceOpt){

                    layoutAdvanceOpt.setVisibility(View.GONE);
                    isAdvanceOpt = false;
                    tvAdvanceOpt.setText("Show advance options ");

                }else {

                    layoutAdvanceOpt.setVisibility(View.VISIBLE);
                    isAdvanceOpt = true;
                    tvAdvanceOpt.setText("Hide advance options ");

                    if (isNetworkAvailable()) {

                        PrepareDistrictSpinner(stateSelected);

                    }else {

                        Toast.makeText(AllChartsActivity.this, "You are not connected to internet", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

    }



    private void PrepareDistrictSpinner(String state){

        new GetDistrictDataTask(state).execute();

    }

    private void PrepareZoneSpinner(String district){

        new GetZoneDataTask().execute(district);

    }

    //Async task..
    private class GetDistrictDataTask extends AsyncTask<Void, Void, Void> {


        Set<String> districts = new HashSet<>();

        String state;

        public GetDistrictDataTask(String state) {
            this.state = state;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

            districtList.clear();

        }

        @Override
        protected Void doInBackground(Void... params) {


            ParseQuery query = ParseQuery.getQuery("EZones");
            query.whereEqualTo("STATE",state);
            query.setLimit(1000);
            try {

                List<ParseObject> parseObjectList =  query.find();

                for (ParseObject parseObject : parseObjectList){

                    String d = parseObject.getString("DISTRICT");
                    districts.add(d);

                }
                //  districts.add("All");

                districtList.addAll(districts);

            } catch (ParseException e) {

                Log.d("EZones", "failed to find Ezone objects");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mProgressDialog.dismiss();

            ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(AllChartsActivity.this, android.R.layout.simple_spinner_item, districtList);
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // categoryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            spDistrict.setAdapter(stateAdapter);

          //  new GetZoneDataTask().execute(districtList.get(0));


            spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    String districtSelected = districtList.get(position);
                    Log.d("EZOnes", "Dustrict selected = " + districtSelected);

                    PrepareZoneSpinner(districtSelected);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }

    }

    //Async task..
    private class GetZoneDataTask extends AsyncTask<String, Void, Void> {

        Set<Zone> zonesSet = new HashSet<>();

        Set<String> zoneSetStrings = new HashSet<>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {


            String district = params[0];
            Log.d("EZones","doInBackground... district = "+district);

            ParseQuery query = ParseQuery.getQuery("EZones");
            query.whereEqualTo("STATE",stateSelected);
            query.whereEqualTo("DISTRICT", district);
            query.setLimit(1000);
            try {

                List<ParseObject> parseObjectList =  query.find();

                for (ParseObject parseObject : parseObjectList){

                    Zone zone = new Zone();

                    String state = parseObject.getString("STATE");
                    String dist = parseObject.getString("DISTRICT");
                    String tal = parseObject.getString("TAHSIL");
                    String name = parseObject.getString("TAHSIL");
                    int id = (int)parseObject.getDouble("SECUREZONE");
                    double lat = parseObject.getDouble("LATITUDE");
                    double lng = parseObject.getDouble("LONGITUDE");
                    String email = parseObject.getString("EMAIL");
                    String fb = parseObject.getString("FACEBOOK");
                    String tw = parseObject.getString("TWITTER");


                    zone.setZoneName(name);
                    zone.setZoneID(id);
                    zone.setZoneState(state);
                    zone.setZoneDist(dist);
                    zone.setZoneTal(tal);
                    zone.setZoneLat(lat);
                    zone.setZoneLng(lng);
                    zone.setZoneEmail(email);
                    zone.setZoneFb(fb);
                    zone.setZoneTw(tw);

                    zonesSet.add(zone);

                    zoneSetStrings.add(name);

                }

              //  zoneList.clear();
              //  zoneList.addAll(zonesSet);

                zoneListString.clear();
                zoneListString.addAll(zoneSetStrings);


            } catch (ParseException e) {

                Log.d("EZones", "failed to find Ezone objects");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(AllChartsActivity.this, android.R.layout.simple_spinner_item, zoneListString);
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // categoryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            spZone.setAdapter(stateAdapter);

            mProgressDialog.dismiss();

            spZone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    String zone = zoneListString.get(position);

                    new GetVictimsDataZone(zone).execute();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


        }

    }


    private class GetVictimsDataState extends AsyncTask<Void,Void,Void> {

        List<Case> caseList = new ArrayList<>();

        String state;

        public GetVictimsDataState(String state) {
            this.state = state;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

        }


        @Override
        protected Void doInBackground(Void... params) {

            ParseQuery investigatorQuery = ParseQuery.getQuery("GeoXman");
            investigatorQuery.whereEqualTo("State",state);
            investigatorQuery.setLimit(1000);


            ParseQuery query = ParseQuery.getQuery("Case");
            query.whereMatchesQuery("GeoXman", investigatorQuery);
            query.setLimit(1000);
            query.addAscendingOrder("createdAt");

            try {


                List<ParseObject> parseObjectList = query.find();

                Log.d("CaseData", "parseObjectList size  = " + parseObjectList.size());

                for (ParseObject parseObject : parseObjectList){

                    Case c = new Case();

                    JSONArray location = parseObject.getJSONArray("Locations");

                    Log.d("CaseData","location jsonArray = "+location);

                    JSONObject jsonObject =  location.getJSONObject(0);

                    double latitude = jsonObject.getDouble("latitude");
                    double longitude = jsonObject.getDouble("longitude");
                    String vName = parseObject.getString("VName");
                    String sImage = parseObject.getString("SImage");
                    String vAddress = parseObject.getString("VAddress");
                    String vContact = parseObject.getString("VContact");
                    String vEmail = parseObject.getString("VEmail");
                    String vImage = parseObject.getString("VImage");
                    String vParents = parseObject.getString("VParents");
                    String vReport = parseObject.getString("VReport");
                    String crimeCategory = parseObject.getString("CrimeCategory");
                    String financialLoss = parseObject.getString("FinancialLoss");
                    String humansAffected = parseObject.getString("HumansAffected");
                    String humansKilled = parseObject.getString("HumansKilled");
                    String noOfSuspects = parseObject.getString("NoOfSuspects");
                    String evidenceAudio = parseObject.getString("evidenceAudio");
                    String evidenceImage = parseObject.getString("evidenceImage");
                    String evidenceVideo = parseObject.getString("evidenceVideo");
                    String invest_contact = parseObject.getString("invest_contact");
                    String invest_name = parseObject.getString("invest_name");
                    String invest_insta_id = parseObject.getString("invest_insta_id");
                    String status = parseObject.getString("status");
                    Date date = parseObject.getCreatedAt();

                    SimpleDateFormat sdf = new SimpleDateFormat("dd:MM:yyyy");

                    String d = sdf.format(date);

                    Log.d("Case"," String date d = "+d);

                    Date d1 = sdf.parse(d);

                    Log.d("Case"," final date d1 = "+d1);

                    Log.d("Case","Latitude = "+latitude+" longitude = "+longitude);


                    c.setLocationPoint(new ParseGeoPoint(latitude, longitude));
                    c.setLocations(location);
                    c.setvName(vName);
                    c.setvImage(vImage);
                    c.setsImage(sImage);
                    c.setAudios(evidenceAudio);
                    c.setFinancialLoss(financialLoss);
                    c.setHumansAffected(humansAffected);
                    c.setHumansKilled(humansKilled);
                    c.setInvestContact(invest_contact);
                    c.setInvestInstaID(invest_insta_id);
                    c.setInvestName(invest_name);
                    c.setNoOfSuspect(noOfSuspects);
                    c.setStatus(status);
                    c.setImages(evidenceImage);
                    c.setvAddress(vAddress);
                    c.setvContact(vContact);
                    c.setvParents(vParents);
                    c.setvEmail(vEmail);
                    c.setVideos(evidenceVideo);
                    c.setvReport(vReport);
                    c.setCrimeCategory(crimeCategory);
                    c.setCreatedAt(d1);

                    caseList.add(c);

                }

            } catch (ParseException e) {

                e.printStackTrace();

            } catch (JSONException e) {

                e.printStackTrace();
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mProgressDialog.dismiss();

            if (caseList.size()<1){

                Toast.makeText(AllChartsActivity.this, "Data Not Available", Toast.LENGTH_LONG).show();

                findViewById(R.id.webViewAllCharts).setVisibility(View.GONE);

            }else {

                findViewById(R.id.webViewAllCharts).setVisibility(View.VISIBLE);

                displayChartDaily(caseList);
            }

            //  displayChartWeekly(caseList);
        }
    }

    public void displayChartDaily(List<Case> caseList){

        Map<Date,ChartData> hashMap = new TreeMap<>();

        if (caseList.size() >= 1) {

            for (Case c : caseList) {

                Date d = c.getCreatedAt();

                Log.d("Case","d = "+d);

                if (hashMap.containsKey(d)){

                    Log.d("Case","duplicate record...."+d);

                    ChartData chartData = hashMap.get(d);

                    if (c.getStatus().equalsIgnoreCase("Successful")){

                        int success = chartData.getSuccess()+1;
                        int failed = chartData.getFail()+0;
                        chartData.setSuccess(success);
                        chartData.setFail(failed);

                    }else{

                        int failed = chartData.getFail()+1;
                        int success = chartData.getSuccess()+0;
                        chartData.setFail(failed);
                        chartData.setSuccess(success);
                    }
                    hashMap.put(d,chartData);

                }else {

                    Log.d("Case","new record...."+d);

                    ChartData chartData = new ChartData();

                    if (c.getStatus().equalsIgnoreCase("Successful")){

                        Log.d("Case","status....= success  "+c.getStatus());

                        chartData.setSuccess(1);
                        chartData.setFail(0);

                    }else{

                        Log.d("Case","status....= Failed "+c.getStatus());

                        chartData.setFail(1);
                        chartData.setSuccess(0);
                    }

                    hashMap.put(d,chartData);
                }
            }

            Log.d("Case","hashMap"+hashMap.toString());
            Log.d("Case","hashMap"+hashMap);


            String s = "['Date', 'Success', 'Failed']";

            Iterator<Date> iterator = hashMap.keySet().iterator();

            while(iterator.hasNext()){

                Date key = iterator.next();

                Log.d("Case"," Iterator date = "+key);

                String a = ",[";

                SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
                String date1 = sdf.format(key);

                a = a +"'"+ date1+"'"+","+ hashMap.get(key).getSuccess()+ ","+ hashMap.get(key).getFail()+"]";

                //  a = a + sales.get(i).getYear()+","+sales.get(i).getSales()+","+sales.get(i).getExpences()+"]";

                Log.d("Case "," Chart data a = "+a);

                s=s+a;

            }

            Log.d("Case "," Chart data s = "+s);

            WebView webview = (WebView) findViewById(R.id.webViewAllCharts);
            String content = "<html>"
                    + "  <head>"
                    + "    <script type=\"text/javascript\" src=\"jsapi.js\"></script>"
                    + "    <script type=\"text/javascript\">"
                    + "      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});"
                    + "      google.setOnLoadCallback(drawChart);"
                    + "      function drawChart() {"
                    + "        var data = google.visualization.arrayToDataTable(["

                    +s

               /* + "          ['Year', 'Sales', 'Expenses'],"
                + "          ['2010',  1000,      400],"
                + "          ['2011',  1170,      460],"
                + "          ['2012',  660,       1120],"
                + "          ['2013',  1030,      540]"

                */

                    + "        ]);"
                    + "        var options = {"
                    + "          title: 'Emergency Events',"
                    + "          hAxis: {title: 'Date', titleTextStyle: {color: 'red'}}"
                    + "        };"
                    + "        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));"
                    + "        chart.draw(data, options);"
                    + "      }"
                    + "    </script>"
                    + "  </head>"
                    + "  <body>"
                    + "    <div id=\"chart_div\" style=\"width: 1000px; height: 500px;\"></div>"
                    + "  </body>" + "</html>";

            WebSettings webSettings = webview.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webview.requestFocusFromTouch();
            webview.loadDataWithBaseURL("file:///android_asset/", content, "text/html", "utf-8", null);
            //webview.loadUrl("file:///android_asset/Code.html"); // Can be used in this way too.
            webview.setInitialScale(1);
            webview.getSettings().setLoadWithOverviewMode(true);
            webview.getSettings().setUseWideViewPort(true);
            webview.getSettings().setBuiltInZoomControls(true);


        }else {

            //  Toast.makeText(StateChartActivity.this, "Nothing to Display", Toast.LENGTH_SHORT).show();

        }

    }

    private class GetVictimsDataZone extends AsyncTask<Void,Void,Void> {

        List<Case> caseList = new ArrayList<>();

        String zone;

        public GetVictimsDataZone(String zone) {
            this.zone = zone;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

        }


        @Override
        protected Void doInBackground(Void... params) {


            ParseQuery investigatorQuery = ParseQuery.getQuery("GeoXman");
            investigatorQuery.whereEqualTo("ZoneName",zone);
            investigatorQuery.setLimit(1000);


            ParseQuery query = ParseQuery.getQuery("Case");
            query.whereMatchesQuery("GeoXman", investigatorQuery);
            query.setLimit(1000);
            query.addAscendingOrder("createdAt");

            try {


                List<ParseObject> parseObjectList = query.find();

                Log.d("CaseData", "parseObjectList size  = " + parseObjectList.size());

                for (ParseObject parseObject : parseObjectList){

                    Case c = new Case();

                    JSONArray location = parseObject.getJSONArray("Locations");

                    Log.d("CaseData","location jsonArray = "+location);

                    JSONObject jsonObject =  location.getJSONObject(0);

                    double latitude = jsonObject.getDouble("latitude");
                    double longitude = jsonObject.getDouble("longitude");
                    String vName = parseObject.getString("VName");
                    String sImage = parseObject.getString("SImage");
                    String vAddress = parseObject.getString("VAddress");
                    String vContact = parseObject.getString("VContact");
                    String vEmail = parseObject.getString("VEmail");
                    String vImage = parseObject.getString("VImage");
                    String vParents = parseObject.getString("VParents");
                    String vReport = parseObject.getString("VReport");
                    String crimeCategory = parseObject.getString("CrimeCategory");
                    String financialLoss = parseObject.getString("FinancialLoss");
                    String humansAffected = parseObject.getString("HumansAffected");
                    String humansKilled = parseObject.getString("HumansKilled");
                    String noOfSuspects = parseObject.getString("NoOfSuspects");
                    String evidenceAudio = parseObject.getString("evidenceAudio");
                    String evidenceImage = parseObject.getString("evidenceImage");
                    String evidenceVideo = parseObject.getString("evidenceVideo");
                    String invest_contact = parseObject.getString("invest_contact");
                    String invest_name = parseObject.getString("invest_name");
                    String invest_insta_id = parseObject.getString("invest_insta_id");
                    String status = parseObject.getString("status");
                    Date date = parseObject.getCreatedAt();

                    SimpleDateFormat sdf = new SimpleDateFormat("dd:MM:yyyy");

                    String d = sdf.format(date);

                    Log.d("Case"," String date d = "+d);

                    Date d1 = sdf.parse(d);

                    Log.d("Case"," final date d1 = "+d1);

                    Log.d("Case","Latitude = "+latitude+" longitude = "+longitude);


                    c.setLocationPoint(new ParseGeoPoint(latitude, longitude));
                    c.setLocations(location);
                    c.setvName(vName);
                    c.setvImage(vImage);
                    c.setsImage(sImage);
                    c.setAudios(evidenceAudio);
                    c.setFinancialLoss(financialLoss);
                    c.setHumansAffected(humansAffected);
                    c.setHumansKilled(humansKilled);
                    c.setInvestContact(invest_contact);
                    c.setInvestInstaID(invest_insta_id);
                    c.setInvestName(invest_name);
                    c.setNoOfSuspect(noOfSuspects);
                    c.setStatus(status);
                    c.setImages(evidenceImage);
                    c.setvAddress(vAddress);
                    c.setvContact(vContact);
                    c.setvParents(vParents);
                    c.setvEmail(vEmail);
                    c.setVideos(evidenceVideo);
                    c.setvReport(vReport);
                    c.setCrimeCategory(crimeCategory);
                    c.setCreatedAt(d1);

                    caseList.add(c);

                }

            } catch (ParseException e) {

                e.printStackTrace();

            } catch (JSONException e) {

                e.printStackTrace();
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mProgressDialog.dismiss();

            if (caseList.size()<1){

                Toast.makeText(AllChartsActivity.this, "Data Not Available", Toast.LENGTH_LONG).show();

                findViewById(R.id.webViewAllCharts).setVisibility(View.GONE);

            }else {

                findViewById(R.id.webViewAllCharts).setVisibility(View.VISIBLE);

                displayChartDaily(caseList);
            }

            //  displayChartWeekly(caseList);
        }
    }

    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }


}
