package com.iphonezoo.cpapp.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.iphonezoo.cpapp.Adapters.InvestigatorVictimsAdapter;
import com.iphonezoo.cpapp.AllInvestigatorListAdapter;
import com.iphonezoo.cpapp.Case;
import com.iphonezoo.cpapp.GeoXman;
import com.iphonezoo.cpapp.R;
import com.iphonezoo.cpapp.Victim;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InvestigatorsVictimsActivity extends AppCompatActivity {

    ProgressDialog mProgressDialog;

    ListView listView;

    String invest_contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_investigators_victims);

        setTitle("Victims");

        mProgressDialog = new ProgressDialog(this);

        listView = (ListView)findViewById(R.id.listViewInvestigatorVictims);

        invest_contact = getIntent().getStringExtra("invest_contact");

        if (isNetworkAvailable()) {

            new GetInvestigatorVictimsTask().execute();

        }else {

            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
        }



    }

    private class GetInvestigatorVictimsTask extends AsyncTask<String, Void, Void> {


        List<Case> victimList = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {

            ParseQuery query = ParseQuery.getQuery("Case");
            query.whereEqualTo("invest_contact",invest_contact);
            query.setLimit(1000);

            try {

                List<ParseObject> parseObjectList = query.find();

                for (ParseObject parseObject : parseObjectList){

                    Case cs = new Case();

                    String name = parseObject.getString("VName");
                    String humansKilled = parseObject.getString("HumansKilled");
                    String vImageUrl=null;
                    String sImageUrl=null;
                    try {
                         vImageUrl = parseObject.getParseFile("VImage").getUrl();
                         sImageUrl = parseObject.getParseFile("SImage").getUrl();

                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                    String vEmail = parseObject.getString("VEmail");
                    String invest_insta_id = parseObject.getString("invest_insta_id");
                    String vReport = parseObject.getString("VReport");
                    String vContact = parseObject.getString("VContact");
                    String fLoss = parseObject.getString("FinancialLoss");
                    String crimeCategory = parseObject.getString("CrimeCategory");
                    String status = parseObject.getString("status");
                    String vParents = parseObject.getString("VParents");
                    String suspects = parseObject.getString("NoOfSuspects");
                    String invest_name = parseObject.getString("invest_name");
                    String humansAffected = parseObject.getString("HumansAffected");
                    String vAddress = parseObject.getString("VAddress");
                    long vFBID  = 11;
                    try {
                         vFBID = parseObject.getNumber("VictimFbID").longValue();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    String videosJSON = parseObject.getString("videosJSON");
                    String imagesJSON = parseObject.getString("imagesJSON");
                    String audiosJSON = parseObject.getString("audiosJSON");

                    JSONArray locationsArray = parseObject.getJSONArray("locationsJSON");

                    Date createdAt = parseObject.getCreatedAt();

                    cs.setCreatedAt(createdAt);
                    cs.setvName(name);
                    cs.setHumansKilled(humansKilled);
                    cs.setHumansAffected(humansAffected);
                    cs.setvImage(vImageUrl);
                    cs.setsImage(sImageUrl);
                    cs.setvEmail(vEmail);
                    cs.setInvestInstaID(invest_insta_id);
                    cs.setvReport(vReport);
                    cs.setvContact(vContact);
                    cs.setFinancialLoss(fLoss);
                    cs.setCrimeCategory(crimeCategory);
                    cs.setStatus(status);
                    cs.setvParents(vParents);
                    cs.setNoOfSuspect(suspects);
                    cs.setInvestName(invest_name);
                    cs.setInvestContact(invest_contact);
                    cs.setvAddress(vAddress);
                    cs.setFbID(vFBID);
                    cs.setVideosJSON(videosJSON);
                    cs.setAudiosJSON(audiosJSON);
                    cs.setImagesJSON(imagesJSON);

                    if (locationsArray!=null) {
                        cs.setLocationsJSON(locationsArray.toString());
                    }


                    victimList.add(cs);

                }

            } catch (ParseException e) {

                Log.d("GeoXman", "Failed to find investigators");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

           InvestigatorVictimsAdapter adapter = new InvestigatorVictimsAdapter(victimList,InvestigatorsVictimsActivity.this);
            listView.setAdapter(adapter);

            mProgressDialog.dismiss();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                  //  Toast.makeText(InvestigatorsVictimsActivity.this, ""+victimList.get(position).getvName(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(InvestigatorsVictimsActivity.this,VictimEvidenceTabbedActivity.class);
                    intent.putExtra("imagesJSON",victimList.get(position).getImagesJSON());
                    intent.putExtra("audiosJSON",victimList.get(position).getAudiosJSON());
                    intent.putExtra("videosJSON",victimList.get(position).getVideosJSON());
                    intent.putExtra("locationsJSON",victimList.get(position).getLocationsJSON());
                    intent.putExtra("victimName",victimList.get(position).getvName());
                    startActivity(intent);

                }
            });

        }

    }


    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }

}
