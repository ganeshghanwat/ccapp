package com.iphonezoo.cpapp.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.iphonezoo.cpapp.R;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AddUnsafeZoneActivity extends AppCompatActivity {


    int zoneID;
    String zoneName;
    String stateName;
    String districtName;

    Spinner spinner;
    EditText editTextName;
    TextView tvAddress;
    Button btSubmit;
    Button btSelectLocation;

    List<Integer> radiousList = new ArrayList<>();

    int radiousSelected;

    LatLng latLng;
    String address;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_unsafe_zone);

        zoneID = getIntent().getIntExtra("zoneID", 0);
        zoneName = getIntent().getStringExtra("zoneSelected");
        stateName = getIntent().getStringExtra("stateSelected");
        districtName = getIntent().getStringExtra("districtSelected");

        final ProgressDialog progressDialog = new ProgressDialog(AddUnsafeZoneActivity.this);


        spinner = (Spinner)findViewById(R.id.spinnerSelectRadious);
        editTextName = (EditText)findViewById(R.id.editTextUnsafeZoneName);
        tvAddress = (TextView)findViewById(R.id.textViewUnsafeZoneAddress);
        btSubmit = (Button)findViewById(R.id.buttonSubmitUnSafeZone);
        btSelectLocation = (Button)findViewById(R.id.buttonSelectLocationUnsafeZone);

        radiousList.add(250);
        radiousList.add(500);
        radiousList.add(1000);
        radiousList.add(1500);

        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,radiousList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                radiousSelected = radiousList.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btSelectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable()) {

                    Intent intent = new Intent(AddUnsafeZoneActivity.this, SelectLocationFromMapActivity.class);
                    intent.putExtra("zoneName", zoneName);

                    startActivityForResult(intent, 1991);
                }else {

                    Toast.makeText(AddUnsafeZoneActivity.this, "You are not connected to internet", Toast.LENGTH_SHORT).show();

                }


            }
        });

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable()) {

                    String name = editTextName.getText().toString();

                    if (!name.trim().equalsIgnoreCase("")) {

                        ParseObject parseObject = new ParseObject("UnsafeZones");
                        parseObject.put("name", name);
                        parseObject.put("address", address);
                        parseObject.put("location", new ParseGeoPoint(latLng.latitude, latLng.longitude));
                        parseObject.put("radious", radiousSelected);
                        parseObject.put("zoneName", zoneName);
                        parseObject.put("zoneID", zoneID);
                        parseObject.put("state", stateName);
                        parseObject.put("district", districtName);

                        progressDialog.setMessage("Creating Unsafe Zone .....please wait");
                        progressDialog.show();

                        parseObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {

                                progressDialog.dismiss();

                                if (e == null) {

                                    Toast.makeText(AddUnsafeZoneActivity.this, "Zone Created Successfully", Toast.LENGTH_SHORT).show();
                                    onBackPressed();
                                    finish();

                                } else {

                                    e.printStackTrace();

                                    Throwable th = e.getCause();

                                    if (th instanceof SocketTimeoutException) {

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                Toast.makeText(AddUnsafeZoneActivity.this, "Internal Server Error", Toast.LENGTH_SHORT).show();

                                            }
                                        });

                                    }

                                    if (th instanceof ConnectException) {

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                Toast.makeText(AddUnsafeZoneActivity.this, "Network Error", Toast.LENGTH_SHORT).show();

                                            }
                                        });
                                    }

                                    Toast.makeText(AddUnsafeZoneActivity.this, "Please Try Again", Toast.LENGTH_SHORT).show();


                                }

                            }
                        });


                    } else {

                        Toast.makeText(AddUnsafeZoneActivity.this, "Please Enter Name", Toast.LENGTH_SHORT).show();
                    }

                }else {

                    Toast.makeText(AddUnsafeZoneActivity.this, "You are not connected to internet", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==1991)
        {

            Log.d("Location", "Select location request code 1991");
            // String message=data.getStringExtra("MESSAGE");
            // textView1.setText(message);

            if (data!=null) {

                double lat = data.getDoubleExtra("lat", 0);
                double lng = data.getDoubleExtra("lng", 0);

                address = getMyLocationAddress(new LatLng(lat,lng));

                latLng = new LatLng(lat, lng);

                Log.d("Location", "Select location lat  " + data.getDoubleExtra("lat", 0));
                Log.d("Location", "Select location lng  " + data.getDoubleExtra("lng", 0));
                Log.d("Location", "Address  " + address);

                address = getMyLocationAddress(new LatLng(lat, lng));

                tvAddress.setText(address);

                Log.d("Location", "Select location address  " + address);

            }

        }

    }


    //reverse geocodeing ...getting address from lat long
    public String getMyLocationAddress(LatLng latLng) {

        Double lat = latLng.latitude;
        Double log = latLng.longitude;

        Geocoder geocoder= new Geocoder(this, Locale.ENGLISH);
        String myAddress = null;

        try {

            //Place your latitude and longitude
            List<Address> addresses = geocoder.getFromLocation(lat,log, 1);

            System.out.println("addresses : "+addresses);

            if(addresses != null) {

                Address fetchedAddress = addresses.get(0);
                StringBuilder strAddress = new StringBuilder();

                for(int i=0; i<fetchedAddress.getMaxAddressLineIndex(); i++) {
                    strAddress.append(fetchedAddress.getAddressLine(i)).append("\n");
                }
                //   myAddress.setText("I am at: " +strAddress.toString());
                myAddress= strAddress.toString();

                String address = addresses.get(0).getAddressLine(0);
                System.out.println("addressLine : " + address);

                if (myAddress.equals("")){
                    myAddress = address;
                }


            }
        }
        catch (Exception ex){

            myAddress = "null";
            ex.printStackTrace();
        }
        return myAddress.toString();

    }

    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }


}
