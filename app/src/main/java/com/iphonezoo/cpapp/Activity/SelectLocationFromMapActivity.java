package com.iphonezoo.cpapp.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.iphonezoo.cpapp.R;

import java.util.List;

public class SelectLocationFromMapActivity extends AppCompatActivity {

    MapFragment mapFragment;

    Button btSubmit, btSearch;
    EditText etSearch;

    GoogleMap map;

    Marker marker;

    LatLng location;

    String locAddress;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_location_from_map);

        btSubmit = (Button) findViewById(R.id.buttonSelectLoc);
        btSearch = (Button) findViewById(R.id.buttonSelectLocSearch);
        etSearch = (EditText) findViewById(R.id.editTextSearchLoc);
        mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapDialogBoxSelectLoc));

        final String zoneName = getIntent().getStringExtra("zoneName");

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                if (googleMap != null) {

                    map = googleMap;

                    googleMap.getUiSettings().setAllGesturesEnabled(true);

                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);

                    if (ActivityCompat.checkSelfPermission(SelectLocationFromMapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SelectLocationFromMapActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    googleMap.setMyLocationEnabled(true);

                    map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {

                            location = latLng;

                            if (marker!=null) {

                                marker.setPosition(latLng);

                            }else {

                                marker =  map.addMarker(new MarkerOptions().position(latLng));

                            }

                        }
                    });

                    List<Address> addressList = null;

                    if (zoneName != null ) {

                        Geocoder geocoder = new Geocoder(getApplicationContext());

                        try {

                            addressList = geocoder.getFromLocationName(zoneName, 1);

                            Address address = addressList.get(0);

                            locAddress = address.getAddressLine(0);

                            Log.d("Location", "address =  " + address.getAddressLine(0));

                           LatLng location1 = new LatLng(address.getLatitude(), address.getLongitude());

                            Log.d("Location", "geocoder address LatLng  " + location1.toString());


                                map.animateCamera(CameraUpdateFactory.newLatLng(location1));

                                // Move the camera instantly to target with a zoom of 15.
                                map.moveCamera(CameraUpdateFactory.newLatLngZoom(location1, 14));

                                // Zoom in, animating the camera.
                                map.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);


                        } catch (Exception e) {

                            e.printStackTrace();

                            Log.d("Location", "geocoder address not found..." + e);

                            //Toast.makeText(SelectLocationFromMapActivity.this, "Location Not Found...try again", Toast.LENGTH_SHORT).show();
                        }
                    }


                } else {
                    Log.d("Location", "googleMap is null ");

                }
            }
        });



        btSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String s = etSearch.getText().toString();
                List<Address> addressList = null;

                if (s != null & !s.equals("")) {

                    Geocoder geocoder = new Geocoder(getApplicationContext());

                    try {

                        addressList = geocoder.getFromLocationName(s, 1);

                        Address address = addressList.get(0);

                         locAddress = address.getAddressLine(0);

                        Log.d("Location", "address =  " + address.getAddressLine(0));

                        location = new LatLng(address.getLatitude(), address.getLongitude());

                        Log.d("Location", "geocoder address LatLng  " + location.toString());

                        if (marker != null) {

                            marker.setPosition(location);
                            map.animateCamera(CameraUpdateFactory.newLatLng(location));

                            // Move the camera instantly to target with a zoom of 15.
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 14));

                            // Zoom in, animating the camera.
                            map.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);

                        } else {

                            marker = map.addMarker(new MarkerOptions().position(location));

                            // Move the camera instantly to target with a zoom of 15.
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 14));

                            // Zoom in, animating the camera.
                            map.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);

                        }


                    } catch (Exception e) {

                        e.printStackTrace();

                        Log.d("Location", "geocoder address not found..." + e);

                        Toast.makeText(SelectLocationFromMapActivity.this, "Location Not Found...try again", Toast.LENGTH_SHORT).show();
                    }

                } else {

                    Toast.makeText(SelectLocationFromMapActivity.this, "Please Enter Location", Toast.LENGTH_SHORT).show();
                }

            }
        });


        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (location != null) {

                    //  String message=editText1.getText().toString();
                    Intent intent = new Intent();
                    // intent.putExtra("MESSAGE",message);
                    if (location != null) {
                        intent.putExtra("lat", location.latitude);
                        intent.putExtra("lng", location.longitude);

                    }
                    setResult(1991, intent);
                    finish();//finishing activity

                } else {

                    Toast.makeText(SelectLocationFromMapActivity.this, "Please select location on map", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }
}
