package com.iphonezoo.cpapp.Activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.iphonezoo.cpapp.R;
import com.iphonezoo.cpapp.SlidingTabLayout;
import com.iphonezoo.cpapp.ViewPager3Adapter;

public class VictimEvidenceTabbedActivity extends AppCompatActivity {

    Toolbar toolbar;
    ViewPager pager;
    ViewPager3Adapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[]={"Evidence","Map"};
    int Numboftabs =2;


   static String imagesJSON;
   static String audiosJSON;
   static  String videosJSON;
   static String locationsJSON;
   static String victimName;


    public static String getVictimName() {
        return victimName;
    }

    public static String getLocationsJSON() {
        return locationsJSON;
    }

    public static String getVideosJSON() {
        return videosJSON;
    }

    public static String getAudiosJSON() {
        return audiosJSON;
    }

    public static String getImagesJSON() {
        return imagesJSON;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_victim_evidence_tabbed);

        imagesJSON = getIntent().getStringExtra("imagesJSON");
        audiosJSON = getIntent().getStringExtra("audiosJSON");
        videosJSON = getIntent().getStringExtra("videosJSON");
        locationsJSON = getIntent().getStringExtra("locationsJSON");
        victimName = getIntent().getStringExtra("victimName");

        Log.d("Evidence","VictimEvidenceTabbedActivity victim name = "+victimName);
        Log.d("Evidence","VictimEvidenceTabbedActivity victim imagesJSON = "+imagesJSON);
        Log.d("Evidence","VictimEvidenceTabbedActivity victim videosJSON = "+videosJSON);
        Log.d("Evidence","VictimEvidenceTabbedActivity victim audiosJSON = "+audiosJSON);


        setTitle(victimName);


        toolbar = (Toolbar) findViewById(R.id.tool_bar);
//        setSupportActionBar(toolbar);


        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new ViewPager3Adapter(getSupportFragmentManager(),Titles,Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom selector for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
