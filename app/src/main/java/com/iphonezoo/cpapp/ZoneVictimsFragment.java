package com.iphonezoo.cpapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class ZoneVictimsFragment extends Fragment {

    List<String> victimsList = new ArrayList<>();
    ListView lvZoneVictims;
    ProgressDialog mProgressDialog;

    public ZoneVictimsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_zone_victims, container, false);

        lvZoneVictims = (ListView)view.findViewById(R.id.listViewZoneVictimsList);


        new GetAllVictims().execute();


        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private class GetAllVictims extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(getActivity());

            mProgressDialog.setTitle("Loading");
            mProgressDialog.setMessage("Loading.....");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();

        }


        @Override
        protected Void doInBackground(Void... params) {

            List<ParseObject> allParseObjects = new ArrayList<>();

            Set<String> vistimsSet = new HashSet<>();

            String instaID = ParseInstallation.getCurrentInstallation().getObjectId();


            ParseQuery imageQuery = ParseQuery.getQuery("EmergencyImage");
         //   imageQuery.whereEqualTo("Installation_id", instaID);
            imageQuery.setLimit(1000);

            try {

                List<ParseObject> imageObjects = imageQuery.find();
                allParseObjects.addAll(imageObjects);

            } catch (ParseException e) {
                e.printStackTrace();
            }


            ParseQuery locationQuery = ParseQuery.getQuery("LocationUpdate");
          //  locationQuery.whereEqualTo("Installation_id",instaID);
            locationQuery.setLimit(1000);
            try {
                List<ParseObject> locationObjects =  locationQuery.find();
                allParseObjects.addAll(locationObjects);

            } catch (ParseException e) {
                e.printStackTrace();
            }


            ParseQuery videoQuery = ParseQuery.getQuery("EmergencyVideo");
           // videoQuery.whereEqualTo("Installation_id",instaID);
            videoQuery.setLimit(1000);

            try {
                List<ParseObject> videoObjects = videoQuery.find();
                allParseObjects.addAll(videoObjects);

            } catch (ParseException e) {
                e.printStackTrace();
            }


            ParseQuery audioQuery = ParseQuery.getQuery("EmergencyAudio");
          //  audioQuery.whereEqualTo("Installation_id", instaID);
            audioQuery.setLimit(1000);
            try {
                List<ParseObject> audioObjects = audioQuery.find();
                allParseObjects.addAll(audioObjects);

            } catch (ParseException e) {
                e.printStackTrace();
            }



            for (ParseObject o : allParseObjects){

                String name = o.getString("name");
                vistimsSet.add(name);

            }

            victimsList.addAll(vistimsSet);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mProgressDialog.dismiss();

            AllVictimsListAdapter adapter = new AllVictimsListAdapter(getActivity(),victimsList);
            lvZoneVictims.setAdapter(adapter);
        }

    }



}
