package com.iphonezoo.cpapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class ServiceListAdapter extends BaseAdapter {

    Context context;
    List<String> services;
    List<ServicesProvided> servicesProvidedList;

    public ServiceListAdapter(Context context, List<String> services) {

        this.context = context;
        this.services = services;

        servicesProvidedList = new ArrayList<>();

        for (int i = 0; i<services.size();i++){

            ServicesProvided s = new ServicesProvided();
            s.setName(services.get(i));
            s.setIsSelected(false);

            servicesProvidedList.add(s);

        }

    }

    @Override
    public int getCount() {
        return services.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView = inflater.inflate(R.layout.service_list_layout,null);

        TextView tvService = (TextView)convertView.findViewById(R.id.textViewService);
        CheckBox checkBox = (CheckBox)convertView.findViewById(R.id.checkBoxServices);

        tvService.setText(services.get(position));
        checkBox.setChecked(false);

        return convertView;
    }
}
