package com.iphonezoo.cpapp;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ZoneListAdapter extends BaseAdapter implements Filterable {

    Context context;
    List<Zone> zoneList;
    List<Zone> zoneListOrigin;


    public ZoneListAdapter(Context context, List<Zone> zoneList) {

        this.context = context;
        this.zoneList = zoneList;
        this.zoneListOrigin = new ArrayList<>();
        this.zoneListOrigin.addAll(zoneList);

        Log.d("ZoneAdapter", "constrocter zoneList size = "+zoneList.size());
        Log.d("ZoneAdapter","Constroctor zoneListSizeOrigin= "+zoneListOrigin.size());

    }

    @Override
    public int getCount() {
        return zoneList.size();
    }

    @Override
    public Object getItem(int position) {
        return zoneList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Zone zone = zoneList.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView = inflater.inflate(R.layout.zone_list_layout,null);

        TextView tvZoneID = (TextView)convertView.findViewById(R.id.textViewZoneID);
        TextView tvZoneName = (TextView)convertView.findViewById(R.id.textViewZoneName);
        TextView tvZoneState = (TextView)convertView.findViewById(R.id.textViewZoneState);
        TextView tvZoneDist = (TextView)convertView.findViewById(R.id.textViewZoneDist);

        tvZoneID.setText(String.valueOf(zone.getZoneID()));
        tvZoneName.setText(zone.getZoneName());
        tvZoneState.setText(zone.getZoneState());
        tvZoneDist.setText(zone.getZoneDist());

        final Button btZoneMap = (Button)convertView.findViewById(R.id.buttonZoneMap);
        btZoneMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,ZoneMapActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("lat", zone.getZoneLat());
                intent.putExtra("lng",zone.getZoneLng());
                intent.putExtra("name",zone.getZoneName());
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,FilterResults results) {

                zoneList = (List<Zone>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                List<Zone> FilteredArrList = new ArrayList<Zone>();



                if (zoneListOrigin == null) {
                    zoneListOrigin = new ArrayList<Zone>(zoneList); // saves the original data in mOriginalValues
                }

                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    Log.d("ZoneAdapter","constraint is null = ");




                    // set the Original result to return
                    results.count = zoneListOrigin.size();
                    results.values = zoneListOrigin;

                    Log.d("ZoneAdapter","zoneListOrigin size = "+zoneListOrigin.size());


                } else {
                    Log.d("ZoneAdapter","constraint is not null = ");

                    constraint = constraint.toString().toLowerCase();

                    Log.d("ZoneAdapter","constraint is = "+constraint);

                    Log.d("ZoneAdapter","zoneListOrigin size  = "+zoneListOrigin.size());

                    for (int i = 0; i < zoneListOrigin.size(); i++) {

                        Log.d("ZoneAdapter", "zoneListOriginSize = " + zoneListOrigin.size());

                        String data = zoneListOrigin.get(i).getZoneName();

                        Log.d("ZoneAdapter","data = "+data);

                        if (data.toLowerCase().startsWith(constraint.toString())) {

                            Log.d("ZoneAdapter","data equals to saved records= ");

                            Log.d("ZoneAdapter","zone name =  = "+zoneListOrigin.get(i).getZoneName());


                            Zone z = new Zone();

                            z.setZoneID(zoneListOrigin.get(i).getZoneID());
                            z.setZoneName(zoneListOrigin.get(i).getZoneName());
                            z.setZoneEmail(zoneListOrigin.get(i).getZoneEmail());
                            z.setZoneTw(zoneListOrigin.get(i).getZoneTw());
                            z.setZoneFb(zoneListOrigin.get(i).getZoneFb());
                            z.setZoneDist(zoneListOrigin.get(i).getZoneDist());
                            z.setZoneLat(zoneListOrigin.get(i).getZoneLat());
                            z.setZoneLng(zoneListOrigin.get(i).getZoneLng());
                            z.setZoneState(zoneListOrigin.get(i).getZoneState());
                            z.setZoneTal(zoneListOrigin.get(i).getZoneTal());

                            FilteredArrList.add(z);
                        }

                        Log.d("ZoneAdapter","FilteredArrList size  = "+FilteredArrList.size());

                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };

        return filter;
    }
}
