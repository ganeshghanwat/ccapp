package com.iphonezoo.cpapp;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.iphonezoo.cpapp.Activity.AddUnsafeZoneActivity;

public class ZoneDetailsActivity extends AppCompatActivity {


    String state;
    String district;
    String zoneName;
    int zoneID;

    Button btChart;
    Button btMap;
    Button btInvestigators;

    TextView tvZoneName;
    TextView tvZoneID;
    TextView tvState;
    TextView tvDist;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone_details);

        state = getIntent().getStringExtra("selectedState");
        district = getIntent().getStringExtra("district");
        zoneID = getIntent().getIntExtra("selectedZoneID", 0);
        zoneName = getIntent().getStringExtra("selectedZoneName");

        setTitle("Zone - "+zoneName);

        Log.d("ZoneData",""+state+" ,"+district+" ,"+zoneName+","+zoneID);

        btChart = (Button)findViewById(R.id.buttonZoneChart);
        btMap = (Button)findViewById(R.id.buttonZoneMap);
        btInvestigators = (Button)findViewById(R.id.buttonZoneInvestigators);

        tvDist = (TextView)findViewById(R.id.textViewZoneDistrict);
        tvState = (TextView)findViewById(R.id.textViewZoneState);
        tvZoneID = (TextView)findViewById(R.id.textViewZoneID);
        tvZoneName = (TextView)findViewById(R.id.textViewZoneName);

        tvZoneName.setText(zoneName);
        tvZoneID.setText(zoneID+"");
        tvState.setText(state);
        tvDist.setText(district);

        btChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable()) {
                    Intent intent = new Intent(ZoneDetailsActivity.this, ZoneChartActivity.class);
                    intent.putExtra("ZoneID", zoneID);
                    startActivity(intent);
                }else{
                    Toast.makeText(ZoneDetailsActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                }

            }
        });

        btMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable()) {
                    Intent intent = new Intent(ZoneDetailsActivity.this, ZoneVictimsMapActivity.class);
                    intent.putExtra("ZoneID", zoneID);
                    startActivity(intent);
                }else {
                    Toast.makeText(ZoneDetailsActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                }

            }
        });

        btInvestigators.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable()) {
                    Intent intent = new Intent(ZoneDetailsActivity.this, ZoneInvestigatorActivity.class);
                    intent.putExtra("ZoneID", zoneID);
                    startActivity(intent);
                }else {
                    Toast.makeText(ZoneDetailsActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_zone_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_addUnsafeZone) {

            if (isNetworkAvailable()) {

                Intent intent = new Intent(ZoneDetailsActivity.this, AddUnsafeZoneActivity.class);
                intent.putExtra("zoneID", zoneID);
                intent.putExtra("zoneSelected", zoneName);
                intent.putExtra("stateSelected", state);
                intent.putExtra("districtSelected", district);
                startActivity(intent);
            }else {
                Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
            }

        }
        else if (id == R.id.action_addNew){

            if (isNetworkAvailable()) {

                Intent intent = new Intent(ZoneDetailsActivity.this, AddNewInvestigatorActivity.class);
                intent.putExtra("zoneID", zoneID);
                intent.putExtra("zoneSelected", zoneName);
                intent.putExtra("stateSelected", state);
                intent.putExtra("districtSelected", district);
                startActivity(intent);

            }else {

                Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
            }

        }

        return super.onOptionsItemSelected(item);
    }

    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }

}
