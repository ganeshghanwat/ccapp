package com.iphonezoo.cpapp;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.iphonezoo.cpapp.Activity.InvestigatorsVictimsActivity;

public class InvestigatorDetailsActivity extends AppCompatActivity {

    TextView tvInvestName;
    TextView tvInvestContact;
    TextView tvInvestAddress;
    TextView tvInvestID;
    TextView tvInvestState;
    TextView tvInvestZone;
    TextView tvInvestType;


    Button btChart;
    Button btMap;
    Button btVictims;

    String contact;
    String nm;
    int zoneID;
    String zoneName;
    String state;
    String address;
    String type;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_investigator_details);

        contact = getIntent().getStringExtra("contact");
        nm = getIntent().getStringExtra("name");
        address = getIntent().getStringExtra("address");
        state = getIntent().getStringExtra("state");
        zoneID = getIntent().getIntExtra("zoneid", 0);
        zoneName = getIntent().getStringExtra("zone");
        type = getIntent().getStringExtra("type");



        tvInvestAddress = (TextView)findViewById(R.id.textViewInvestigatorAddress);
        tvInvestName = (TextView)findViewById(R.id.textViewInvestigatorNameNoti);
        tvInvestContact = (TextView)findViewById(R.id.textViewInvestigatorContactNoti);
        tvInvestID = (TextView)findViewById(R.id.textViewInvestigatorDetailsID);
        tvInvestState = (TextView)findViewById(R.id.textViewInvestigatorStateName);
        tvInvestZone = (TextView)findViewById(R.id.textViewInvestigatorZoneName);
        tvInvestType = (TextView)findViewById(R.id.textViewInvestigatorType);

        btChart = (Button)findViewById(R.id.buttonInvestigatorChart);
        btMap = (Button)findViewById(R.id.buttonInvestigatorMap);
        btVictims = (Button)findViewById(R.id.buttonInvestigatorVictims);

        tvInvestContact.setText(contact);
        tvInvestAddress.setText(address);
        tvInvestID.setText(zoneID+"");
        tvInvestType.setText(type);
        tvInvestZone.setText(zoneName);
        tvInvestState.setText(state);
        tvInvestName.setText(nm);



        btChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable()) {


                    Intent intent = new Intent(InvestigatorDetailsActivity.this, InvestigatorChartActivity.class);
                    intent.putExtra("contact", contact);
                    startActivity(intent);
                }else {
                    Toast.makeText(InvestigatorDetailsActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                }

            }
        });


        btMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable()) {

                    Intent intent = new Intent(InvestigatorDetailsActivity.this, InvestigatorVictimsMapActivity.class);
                    intent.putExtra("contact", contact);
                    startActivity(intent);
                }else {

                    Toast.makeText(InvestigatorDetailsActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                }

            }
        });

        btVictims.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable()) {

                    Intent intent = new Intent(InvestigatorDetailsActivity.this, InvestigatorsVictimsActivity.class);
                    intent.putExtra("invest_contact", contact);
                    startActivity(intent);

                }else {

                    Toast.makeText(InvestigatorDetailsActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_investigator_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }

}
