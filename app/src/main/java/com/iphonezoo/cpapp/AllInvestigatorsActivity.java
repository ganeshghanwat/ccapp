package com.iphonezoo.cpapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AllInvestigatorsActivity extends AppCompatActivity {

    List<String> districtList = new ArrayList<>();
    //  List<Zone> zoneList = new ArrayList<>();
    List<String> zoneListString = new ArrayList<>();

    List<Integer> zoneIDList = new ArrayList<>();

    LinearLayout layoutAdvanceOpt;
    Spinner spState;
    Spinner spDistrict;
    Spinner spZone;

    TextView tvAdvanceOpt;

    String stateSelected;
    String zoneSelected;
    int zoneIDSelected;

    List<String> stateList = new ArrayList<>();

    List<String> staticStateList = new ArrayList<>(Arrays.asList("West Bengal","Uttarakhand", "Uttar Pradesh", "Tripura", "Telangana", "Tamil Nadu", "Sikkim", "Rajasthan", "Punjab", "Pondicherry", "Odisha","Nagaland", "Mizoram", "Maharashtra", "Meghalaya", "Madhya Pradesh", "Karnataka", "Jharkhand", "Jammu & Kashmir", "Himachal Pradesh", "Haryana", "Gujarat","Goa",
            "Daman & Diu", "Chattisgarh", "Bihar", "Assam", "Arunachal Pradesh", "Andhra Pradesh", "Andaman & Nicobar Islands"));

    boolean isAdvanceOpt = false;


    ListView lvAllInvestigators;


    ProgressDialog mProgressDialog;

    List<GeoXman> geoXmanList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_investigators);

      //  selectedZoneName =  getIntent().getStringExtra("selectedZoneName");
      //  selectedZoneID =  getIntent().getIntExtra("selectedZoneID", 0);

        mProgressDialog = new ProgressDialog(this);

        lvAllInvestigators = (ListView)findViewById(R.id.listViewAllInvest);

        layoutAdvanceOpt = (LinearLayout)findViewById(R.id.linearLayoutAdvanceOptAllInvestigators);
        spState = (Spinner)findViewById(R.id.spinnerStateAllInvestigators);
        spDistrict = (Spinner)findViewById(R.id.spinnerDistrictsAllInvestigators);
        spZone = (Spinner)findViewById(R.id.spinnerZonesAllInvestigators);

        tvAdvanceOpt = (TextView)findViewById(R.id.textViewAdvanceOptAllInvestigators);


        layoutAdvanceOpt.setVisibility(View.GONE);

        Collections.sort(staticStateList);

        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(AllInvestigatorsActivity.this, android.R.layout.simple_spinner_item, staticStateList);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // categoryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spState.setAdapter(stateAdapter);


        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //    Log.d("EZones", "State selected = " + stateList.get(position));

                //   stateSelected = stateList.get(position);

                stateSelected = staticStateList.get(position);

                if (isNetworkAvailable()) {

                    if (isAdvanceOpt) {

                        PrepareDistrictSpinner(stateSelected);

                    } else {

                        // load web view with chart

                        new GetStateInvestigatorDataTask(stateSelected).execute();

                    }
                }else {

                    Toast.makeText(AllInvestigatorsActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        tvAdvanceOpt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isAdvanceOpt){

                    layoutAdvanceOpt.setVisibility(View.GONE);
                    isAdvanceOpt = false;
                    tvAdvanceOpt.setText("Show advance options ");

                }else {

                    layoutAdvanceOpt.setVisibility(View.VISIBLE);
                    isAdvanceOpt = true;
                    tvAdvanceOpt.setText("Hide advance options ");

                    if (isNetworkAvailable()) {


                        PrepareDistrictSpinner(stateSelected);

                    }else {

                        Toast.makeText(AllInvestigatorsActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        lvAllInvestigators.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                GeoXman geoXman = geoXmanList.get(position);

                Intent intent = new Intent(AllInvestigatorsActivity.this,InvestigatorDetailsActivity.class);

                intent.putExtra("name",geoXman.getName());
                intent.putExtra("contact",geoXman.getContact());
                intent.putExtra("zone",geoXman.getZoneName());
                intent.putExtra("zoneid",geoXman.getZoneID());
                intent.putExtra("state",geoXman.getState());
                intent.putExtra("address",geoXman.getAddress());
                intent.putExtra("type",geoXman.getType());

                startActivity(intent);

            }
        });

      //  new GetInvestigatorDataTask().execute();

    }

    private void PrepareDistrictSpinner(String state){

        new GetDistrictDataTask(state).execute();

    }

    private void PrepareZoneSpinner(String district){

        new GetZoneDataTask().execute(district);

    }

    //Async task..
    private class GetDistrictDataTask extends AsyncTask<Void, Void, Void> {


        Set<String> districts = new HashSet<>();

        String state;

        public GetDistrictDataTask(String state) {
            this.state = state;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

            districtList.clear();

        }

        @Override
        protected Void doInBackground(Void... params) {


            ParseQuery query = ParseQuery.getQuery("EZones");
            query.whereEqualTo("STATE",state);
            query.setLimit(1000);
            try {

                List<ParseObject> parseObjectList =  query.find();

                for (ParseObject parseObject : parseObjectList){

                    String d = parseObject.getString("DISTRICT");
                    districts.add(d);

                }
                //  districts.add("All");

                districtList.addAll(districts);

            } catch (ParseException e) {

                Log.d("EZones", "failed to find Ezone objects");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mProgressDialog.dismiss();

            ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(AllInvestigatorsActivity.this, android.R.layout.simple_spinner_item, districtList);
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // categoryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            spDistrict.setAdapter(stateAdapter);

            //  new GetZoneDataTask().execute(districtList.get(0));


            spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    String districtSelected = districtList.get(position);
                    Log.d("EZOnes", "Dustrict selected = " + districtSelected);

                    PrepareZoneSpinner(districtSelected);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }

    }

    //Async task..
    private class GetZoneDataTask extends AsyncTask<String, Void, Void> {

        Set<Zone> zonesSet = new HashSet<>();

        Set<String> zoneSetStrings = new HashSet<>();
        Set<Integer> zoneIDSet = new HashSet<>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

            zoneListString.clear();

            zoneIDList.clear();


        }

        @Override
        protected Void doInBackground(String... params) {


            String district = params[0];
            Log.d("EZones","doInBackground... district = "+district);

            ParseQuery query = ParseQuery.getQuery("EZones");
            query.whereEqualTo("STATE",stateSelected);
            query.whereEqualTo("DISTRICT", district);
            query.setLimit(1000);
            try {

                List<ParseObject> parseObjectList =  query.find();

                for (ParseObject parseObject : parseObjectList){

                    Zone zone = new Zone();

                    String state = parseObject.getString("STATE");
                    String dist = parseObject.getString("DISTRICT");
                    String tal = parseObject.getString("TAHSIL");
                    String name = parseObject.getString("TAHSIL");
                    int id = (int)parseObject.getDouble("SECUREZONE");
                    double lat = parseObject.getDouble("LATITUDE");
                    double lng = parseObject.getDouble("LONGITUDE");
                    String email = parseObject.getString("EMAIL");
                    String fb = parseObject.getString("FACEBOOK");
                    String tw = parseObject.getString("TWITTER");


                    zone.setZoneName(name);
                    zone.setZoneID(id);
                    zone.setZoneState(state);
                    zone.setZoneDist(dist);
                    zone.setZoneTal(tal);
                    zone.setZoneLat(lat);
                    zone.setZoneLng(lng);
                    zone.setZoneEmail(email);
                    zone.setZoneFb(fb);
                    zone.setZoneTw(tw);

                    zonesSet.add(zone);

                    zoneSetStrings.add(name);



                   zoneListString.add(name);
                    zoneIDList.add(id);

                }

                //  zoneList.clear();
                //  zoneList.addAll(zonesSet);

                /**

                zoneListString.clear();
                zoneListString.addAll(zoneSetStrings);

                 **/



            } catch (ParseException e) {

                Log.d("EZones", "failed to find Ezone objects");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(AllInvestigatorsActivity.this, android.R.layout.simple_spinner_item, zoneListString);
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // categoryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            spZone.setAdapter(stateAdapter);

            mProgressDialog.dismiss();

            spZone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    String zone = zoneListString.get(position);

                    zoneSelected = zone;
                    zoneIDSelected = zoneIDList.get(position);

                    new GetZoneInvestigatorDataTask(zone).execute();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_all_investigators, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {

            Log.d("AllInvestigators","  zoneSelected = "+zoneSelected);
            Log.d("AllInvestigators","  zoneIDSelected = "+zoneIDSelected);

            if (zoneSelected==null && zoneIDSelected == 0 ){

                Toast.makeText(this, "Please select Zone from Advance Options", Toast.LENGTH_LONG).show();

            }else {

                Intent intent = new Intent(AllInvestigatorsActivity.this, AddNewInvestigatorActivity.class);
                intent.putExtra("zoneID", zoneIDSelected);
                intent.putExtra("zoneSelected", zoneSelected);
                startActivity(intent);

            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Async task..
    private class GetStateInvestigatorDataTask extends AsyncTask<String, Void, Void> {


        String state;

        public GetStateInvestigatorDataTask(String state) {
            this.state = state;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            geoXmanList.clear();


            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {

            ParseQuery query = ParseQuery.getQuery("GeoXman");
            query.whereEqualTo("State",state);
            query.setLimit(1000);

            try {

               List<ParseObject> parseObjectList = query.find();

                for (ParseObject parseObject : parseObjectList){

                    GeoXman geoXman = new GeoXman();

                    String id = parseObject.getString("XManID");
                    String name = parseObject.getString("XManName");
                    String type = parseObject.getString("XManType");
                    String contactNum = parseObject.getString("XManContactNum");
                    ParseGeoPoint location = parseObject.getParseGeoPoint("location");
                    String area = parseObject.getString("AreaCovered");
                    String address = parseObject.getString("XManAddress");
                    String zoneName = parseObject.getString("ZoneName");
                    String state = parseObject.getString("State");
                    int zoneId = parseObject.getInt("ZoneID");


                    String imageURL = "";

                    try {

                        imageURL = parseObject.getParseFile("Pic").getUrl();

                    } catch (NullPointerException e) {

                        imageURL = "";

                    }

                    geoXman.setId(id);
                    geoXman.setName(name);
                    geoXman.setType(type);
                    geoXman.setContact(contactNum);
                    geoXman.setLocation(location);
                    geoXman.setArea(area);
                    geoXman.setState(state);
                    geoXman.setAddress(address);
                    geoXman.setZoneName(zoneName);
                    geoXman.setZoneID(zoneId);
                    geoXman.setImgURL(imageURL);

                    geoXmanList.add(geoXman);

                }

            } catch (ParseException e) {

                Log.d("GeoXman","Failed to find investigators");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            AllInvestigatorListAdapter adapter = new AllInvestigatorListAdapter(AllInvestigatorsActivity.this,geoXmanList);
            lvAllInvestigators.setAdapter(adapter);

            mProgressDialog.dismiss();

            if (geoXmanList.size()<1){

                Toast.makeText(AllInvestigatorsActivity.this, "Investigators Not Founnd", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private class GetZoneInvestigatorDataTask extends AsyncTask<String, Void, Void> {



        String zoneName;

        public GetZoneInvestigatorDataTask(String zoneName) {
            this.zoneName = zoneName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

            geoXmanList.clear();

        }

        @Override
        protected Void doInBackground(String... params) {

            ParseQuery query = ParseQuery.getQuery("GeoXman");
            query.whereEqualTo("ZoneName",zoneName);
            query.setLimit(1000);

            try {

                List<ParseObject> parseObjectList = query.find();

                for (ParseObject parseObject : parseObjectList){

                    GeoXman geoXman = new GeoXman();

                    String id = parseObject.getString("XManID");
                    String name = parseObject.getString("XManName");
                    String type = parseObject.getString("XManType");
                    String contactNum = parseObject.getString("XManContactNum");
                    ParseGeoPoint location = parseObject.getParseGeoPoint("location");
                    String area = parseObject.getString("AreaCovered");
                    String address = parseObject.getString("XManAddress");
                    String zoneName = parseObject.getString("ZoneName");
                    int zoneId = parseObject.getInt("ZoneID");


                    String imageURL = "";

                    try {

                        imageURL = parseObject.getParseFile("Pic").getUrl();

                    } catch (NullPointerException e) {

                        imageURL = "";

                    }



                    geoXman.setId(id);
                    geoXman.setName(name);
                    geoXman.setType(type);
                    geoXman.setContact(contactNum);
                    geoXman.setLocation(location);
                    geoXman.setArea(area);
                    geoXman.setAddress(address);
                    geoXman.setZoneName(zoneName);
                    geoXman.setZoneID(zoneId);
                    geoXman.setImgURL(imageURL);

                    geoXmanList.add(geoXman);

                }

            } catch (ParseException e) {

                Log.d("GeoXman","Failed to find investigators");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            AllInvestigatorListAdapter adapter = new AllInvestigatorListAdapter(AllInvestigatorsActivity.this,geoXmanList);
            lvAllInvestigators.setAdapter(adapter);

            mProgressDialog.dismiss();

            if (geoXmanList.size()<1){

                Toast.makeText(AllInvestigatorsActivity.this, "Investigators Not Founnd", Toast.LENGTH_SHORT).show();
            }

        }

    }

    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }


}
