package com.iphonezoo.cpapp;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;


import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseInstallation;

public class ParseStart extends Application {



    @Override
    public void onCreate() {
        MultiDex.install(this);
        super.onCreate();

        // Enable Local Datastore.
        Parse.enableLocalDatastore(this);
       // Parse.initialize(this, "ferCFDrhFFJQclGl0wnDOmakbV3ZIqVYNRyj3GlW", "Hx3zUUIxy5aE3kcOwzpdgDn69UjMkhPFf5WBao76");

        Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                .applicationId("family11")
                .server("http://103.224.247.55:1001/parse/")   // '/' important after 'parse'
                .enableLocalDataStore()
                .build());

        ParseInstallation.getCurrentInstallation().saveInBackground();

        ParseACL defaultACL = new ParseACL();
        // Optionally enable public read access.
         defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);

        // UNIVERSAL IMAGE LOADER SETUP
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheOnDisc(true).cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache())
                .discCacheSize(100 * 1024 * 1024).build();

        ImageLoader.getInstance().init(config);
        // END - UNIVERSAL IMAGE LOADER SETUP

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
