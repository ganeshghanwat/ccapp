package com.iphonezoo.cpapp;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class InvestigatorVictimsMapActivity extends AppCompatActivity {

    GoogleMap map;

    MapFragment mapFragment;

    String contact;

    ProgressDialog mProgressDialog;

    Button btSuccess;
    Button btFailed;

    List<Case> caseList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_investigator_victims);

        mProgressDialog = new ProgressDialog(this);

        contact = getIntent().getStringExtra("contact");

        btFailed = (Button)findViewById(R.id.buttonInvestigatorVictimStatusFailed);
        btSuccess = (Button)findViewById(R.id.buttonInvestigatorVictimStatusSuccess);

        mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapDialogBoxInvestigatorVictims));

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                map = googleMap;

            }
        });

        new GetVictimsData().execute();


        btFailed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                map.clear();

                for (Case c : caseList){

                    if (c.getStatus().equalsIgnoreCase("Failed")){

                        double lat = c.getLocationPoint().getLatitude();
                        double lng = c.getLocationPoint().getLongitude();

                        String name = c.getvName();

                        if (map!=null) {

                            Marker target = map.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(name)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.victim_red_marker)));
                            // Move the camera instantly to target with a zoom of 15.
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 14));
                        }
                    }

                }

            }
        });


        btSuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            map.clear();

                for (Case c : caseList){

                    if (c.getStatus().equalsIgnoreCase("Successful")){

                        double lat = c.getLocationPoint().getLatitude();
                        double lng = c.getLocationPoint().getLongitude();

                        String name = c.getvName();

                        if (map!=null) {

                            Marker target = map.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(name)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.victim_green_marker)));
                            // Move the camera instantly to target with a zoom of 15.
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 14));
                        }
                    }

                }


            }
        });




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_investigator_victims, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    ///Async Task

    private class GetVictimsData extends AsyncTask<Void,Void,Void> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

        }


        @Override
        protected Void doInBackground(Void... params) {

            ParseQuery investigatorQuery = ParseQuery.getQuery("GeoXman");
            investigatorQuery.whereEqualTo("XManContactNum",contact);
            investigatorQuery.setLimit(1000);


            ParseQuery query = ParseQuery.getQuery("Case");
            query.whereMatchesQuery("GeoXman", investigatorQuery);
            query.setLimit(1000);

            try {


                List<ParseObject> parseObjectList = query.find();

                Log.d("CaseData", "parseObjectList size  = " + parseObjectList.size());

                for (ParseObject parseObject : parseObjectList){

                    Case c = new Case();

                    JSONArray location = parseObject.getJSONArray("Locations");

                    Log.d("CaseData","location jsonArray = "+location);

                    JSONObject jsonObject =  location.getJSONObject(0);

                    double latitude = jsonObject.getDouble("latitude");
                    double longitude = jsonObject.getDouble("longitude");
                    String vName = parseObject.getString("VName");
                    String sImage = parseObject.getString("SImage");
                    String vAddress = parseObject.getString("VAddress");
                    String vContact = parseObject.getString("VContact");
                    String vEmail = parseObject.getString("VEmail");
                    String vImage = parseObject.getString("VImage");
                    String vParents = parseObject.getString("VParents");
                    String vReport = parseObject.getString("VReport");
                    String crimeCategory = parseObject.getString("CrimeCategory");
                    String financialLoss = parseObject.getString("FinancialLoss");
                    String humansAffected = parseObject.getString("HumansAffected");
                    String humansKilled = parseObject.getString("HumansKilled");
                    String noOfSuspects = parseObject.getString("NoOfSuspects");
                    String evidenceAudio = parseObject.getString("evidenceAudio");
                    String evidenceImage = parseObject.getString("evidenceImage");
                    String evidenceVideo = parseObject.getString("evidenceVideo");
                    String invest_contact = parseObject.getString("invest_contact");
                    String invest_name = parseObject.getString("invest_name");
                    String invest_insta_id = parseObject.getString("invest_insta_id");
                    String status = parseObject.getString("status");



                    Log.d("Case","Latitude = "+latitude+" longitude = "+longitude);

                    c.setLocationPoint(new ParseGeoPoint(latitude, longitude));
                    c.setLocations(location);
                    c.setvName(vName);
                    c.setvImage(vImage);
                    c.setsImage(sImage);
                    c.setAudios(evidenceAudio);
                    c.setFinancialLoss(financialLoss);
                    c.setHumansAffected(humansAffected);
                    c.setHumansKilled(humansKilled);
                    c.setInvestContact(invest_contact);
                    c.setInvestInstaID(invest_insta_id);
                    c.setInvestName(invest_name);
                    c.setNoOfSuspect(noOfSuspects);
                    c.setStatus(status);
                    c.setImages(evidenceImage);
                    c.setvAddress(vAddress);
                    c.setvContact(vContact);
                    c.setvParents(vParents);
                    c.setvEmail(vEmail);
                    c.setVideos(evidenceVideo);
                    c.setvReport(vReport);
                    c.setCrimeCategory(crimeCategory);

                    caseList.add(c);

                }

            } catch (ParseException e) {

                e.printStackTrace();

            } catch (JSONException e) {

                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mProgressDialog.dismiss();

            if (caseList.size() >= 1) {

                for (Case c : caseList) {

                    if (c.getStatus().equalsIgnoreCase("Successful")) {

                        double lat = c.getLocationPoint().getLatitude();
                        double lng = c.getLocationPoint().getLongitude();

                        String name = c.getvName();

                        if (map!=null) {

                            Marker target = map.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(name)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.victim_green_marker)));
                            // Move the camera instantly to target with a zoom of 15.
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 14));
                        }
                    }

                }
                // Zoom in, animating the camera.
                if (map!=null) {
                    map.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
                }
            }else {

                Toast.makeText(InvestigatorVictimsMapActivity.this, "Nothing to Display", Toast.LENGTH_SHORT).show();

            }
        }

    }
}
