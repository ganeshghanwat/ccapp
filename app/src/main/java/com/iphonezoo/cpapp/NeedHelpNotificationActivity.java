package com.iphonezoo.cpapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseGeoPoint;

import org.json.JSONException;
import org.json.JSONObject;

public class NeedHelpNotificationActivity extends AppCompatActivity {

    String IName;
    String contact;
    String VName;

    JSONObject jsonObject;

    double latitude;
    double longitude;

    TextView tvIName;
    TextView tvVName;
    TextView tvContact;

    GoogleMap map;

    MapFragment mapFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_need_help_notification);

        try {

            jsonObject = new JSONObject(getIntent().getStringExtra("com.parse.Data"));

            VName = jsonObject.getString("name");

            JSONObject locObject = jsonObject.getJSONObject("location");

            latitude = locObject.getDouble("latitude");
            longitude = locObject.getDouble("longitude");

            System.out.println("location = "+latitude +","+longitude);

           IName = jsonObject.getString("IName");

            VName = jsonObject.getString("name");

            contact = jsonObject.getString("contact");


        } catch (JSONException e) {
            e.printStackTrace();
        }

        tvIName = (TextView)findViewById(R.id.textViewInvestigatorNameNoti);
        tvVName = (TextView)findViewById(R.id.textViewVictimNameNoti);
        tvContact = (TextView)findViewById(R.id.textViewInvestigatorContactNoti);

        mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapDialogBoxNeedHelp));

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                if (googleMap!=null) {

                    map = googleMap;

                    Marker target = map.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)));

                    // Move the camera instantly to target with a zoom of 15.
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 14));

                    // Zoom in, animating the camera.
                    map.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);

                }


            }
        });

        tvContact.setText(contact);
        tvVName.setText(VName);
        tvIName.setText(IName);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_need_help_notification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
