package com.iphonezoo.cpapp;

public class Zone {

    private String zoneName;
    private int zoneID;
    private String zoneState;
    private String zoneDist;
    private String zoneTal;
    private double zoneLat;
    private double zoneLng;
    private String zoneEmail;
    private String zoneFb;
    private String zoneTw;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Zone zone = (Zone) o;

        if (!zoneName.equals(zone.zoneName)) return false;
        return zoneState.equals(zone.zoneState);


    }

    @Override
    public int hashCode() {
        int result = zoneName.hashCode();
        result = 31 * result + zoneState.hashCode();
        return result;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public int getZoneID() {
        return zoneID;
    }

    public void setZoneID(int zoneID) {
        this.zoneID = zoneID;
    }

    public String getZoneState() {
        return zoneState;
    }

    public void setZoneState(String zoneState) {
        this.zoneState = zoneState;
    }

    public String getZoneDist() {
        return zoneDist;
    }

    public void setZoneDist(String zoneDist) {
        this.zoneDist = zoneDist;
    }

    public String getZoneTal() {
        return zoneTal;
    }

    public void setZoneTal(String zoneTal) {
        this.zoneTal = zoneTal;
    }

    public double getZoneLat() {
        return zoneLat;
    }

    public void setZoneLat(double zoneLat) {
        this.zoneLat = zoneLat;
    }

    public double getZoneLng() {
        return zoneLng;
    }

    public void setZoneLng(double zoneLng) {
        this.zoneLng = zoneLng;
    }

    public String getZoneEmail() {
        return zoneEmail;
    }

    public void setZoneEmail(String zoneEmail) {
        this.zoneEmail = zoneEmail;
    }

    public String getZoneFb() {
        return zoneFb;
    }

    public void setZoneFb(String zoneFb) {
        this.zoneFb = zoneFb;
    }

    public String getZoneTw() {
        return zoneTw;
    }

    public void setZoneTw(String zoneTw) {
        this.zoneTw = zoneTw;
    }
}
