package com.iphonezoo.cpapp.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.iphonezoo.cpapp.Adapters.EvidenceAudioListAdapter;
import com.iphonezoo.cpapp.Adapters.EvidenceImageGridViewAdapter;
import com.iphonezoo.cpapp.Adapters.EvidenceVideoListAdapter;
import com.iphonezoo.cpapp.POJO.Audio;
import com.iphonezoo.cpapp.POJO.GeoPoint;
import com.iphonezoo.cpapp.POJO.Image;
import com.iphonezoo.cpapp.POJO.Video;
import com.iphonezoo.cpapp.R;
import com.iphonezoo.cpapp.VictimsMapActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class EvidenceDialogFragment extends DialogFragment {

    View view;

    String name;
    String invest_insta_id;


    // Evidences
    ListView lvVideos;
    ListView lvAudio;
    TextView tvImage;
    TextView tvVideo;
    TextView tvAudio;
    TextView textViewDataNoAva;
    List<Image> imageList = new ArrayList<>();
    List<Video> videoList = new ArrayList<>();
    List<Audio> audioList = new ArrayList<>();
    GridView gvImages;
    TextView tvInvestName;

    GetVictimsEvidenceData getVictimsEvidenceData;



    Button btClose;

    public static EvidenceDialogFragment newInstance(String string,String invest_id) {
        EvidenceDialogFragment f = new EvidenceDialogFragment();
        Bundle args = new Bundle();
        args.putString("name", string);
        args.putString("invest_insta_id", invest_id);
        f.setArguments(args);
        return f;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);

        name = getArguments().getString("name");
        invest_insta_id = getArguments().getString("invest_insta_id");

        Log.d("Tracking","TrackingBottimSheetDialogFragment  onCreate  name = "+name);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        try {

            view = inflater.inflate(R.layout.evidence_dialog_fragment, container, false);

            btClose = (Button)view.findViewById(R.id.buttonCancelEvidenceDialog);

            if (isNetworkAvailable()) {

                setUpEvidenceBottomView(name, invest_insta_id);

            }else {
                Toast.makeText(getActivity(), "You are not connected to internet", Toast.LENGTH_SHORT).show();
            }


            btClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    EvidenceDialogFragment.this.dismiss();

                }
            });


        } catch (InflateException e) {

            e.printStackTrace();
            Log.d("Tracking","inflate exception "+e);
        }


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("Tracking","  onDestroyView ");

        if (getVictimsEvidenceData!=null){

            getVictimsEvidenceData.cancel(true);
        }

    }


    public void setUpEvidenceBottomView(String name,String invest_insta_id){

        gvImages = (GridView)view.findViewById(R.id.gridViewDialogFragment);
        lvAudio = (ListView)view.findViewById(R.id.listViewAudioDialogFragment);
        lvVideos = (ListView)view.findViewById(R.id.listViewVideosDialogFragment);
        tvImage = (TextView)view.findViewById(R.id.textViewImagesDialogFragment);
        tvVideo = (TextView)view.findViewById(R.id.textViewVideossDialogFragment);
        tvAudio = (TextView)view.findViewById(R.id.textViewAudioDialogFragment);
        textViewDataNoAva = (TextView)view.findViewById(R.id.textViewDataNotAvaDialogFragment);
        tvInvestName = (TextView)view.findViewById(R.id.textViewEvidenceInvestigatorNameDialogFragment);

        textViewDataNoAva.setVisibility(View.GONE);



        imageList.clear();
        videoList.clear();
        audioList.clear();

        getVictimsEvidenceData =  new GetVictimsEvidenceData(name,invest_insta_id,getActivity());
        getVictimsEvidenceData.execute();


        gvImages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final Dialog nagDialog = new Dialog(getActivity(),android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                nagDialog.setCancelable(false);
                nagDialog.setContentView(R.layout.preview_image);
                Button btnClose = (Button)nagDialog.findViewById(R.id.btnIvClose);
                ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);

                ImageLoader imageLoader = ImageLoader.getInstance();
                DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                        .cacheOnDisc(true).resetViewBeforeLoading(true)
                        .build();

                imageLoader.displayImage(imageList.get(position).getUrl(), ivPreview, options);

                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {

                        nagDialog.dismiss();
                    }
                });
                nagDialog.show();

            }
        });




    }



    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }

    public String getVideoThumbnail(String videoID){

        String thumb = null;

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String responseJsonStr = null;

        try {
            // Construct the URL for the OpenWeatherMap query
            // Possible parameters are avaiable at OWM's forecast API page, at
            // http://openweathermap.org/API#forecast
            URL url = new URL("https://api.dailymotion.com/video/"+videoID+"?fields=thumbnail_medium_url,thumbnail_small_url,thumbnail_large_url");

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                thumb = null;
                return null;
            }

            responseJsonStr = buffer.toString();

            Log.d("VideoFetch", "response  =  "+responseJsonStr);


            JSONObject jsonObject = new JSONObject(responseJsonStr);

            thumb = jsonObject.getString("thumbnail_medium_url");

            Log.d("VideoFetch", "thumb =  "+thumb);


        } catch (IOException e) {
            Log.d("VideoFetch", "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attemping
            // to parse it.
            thumb = null;
            return null;

        } catch (JSONException e) {

            Log.e("VideoFetch", "exception  ", e);
            e.printStackTrace();
            thumb = null;
            return null;

        } finally{

            if (urlConnection != null) {
                urlConnection.disconnect();
            }

            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }
        }

        return thumb;

    }

    private class GetVictimsEvidenceData extends AsyncTask<Void,Void,Void> {

        ProgressBar progressBar;

        String investigator_name;

        String name;
        Context context;
        String invest_insta_id;

        public GetVictimsEvidenceData(String name,String invest_insta_id, Context context) {
            this.name = name;
            this.context = context;
            this.invest_insta_id = invest_insta_id;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressBar = (ProgressBar)view.findViewById(R.id.progressBarVictimEvidenceDialogFragment);

            progressBar.setVisibility(View.VISIBLE);

            textViewDataNoAva.setVisibility(View.GONE);


/**

 mProgressDialog = new ProgressDialog(VictimsMapActivity.this);
 mProgressDialog.setMessage("Loading.....");
 mProgressDialog.setCancelable(false);
 mProgressDialog.show();

 **/

        }


        @Override
        protected Void doInBackground(Void... params) {


            Calendar calendar = Calendar.getInstance();

            calendar.add(Calendar.HOUR,-24);

            Date d = calendar.getTime();


            ParseQuery imageQuery = ParseQuery.getQuery("EmergencyImage");
            imageQuery.whereEqualTo("name",name);
            //  imageQuery.whereGreaterThan("updatedAt",d);

            imageQuery.addDescendingOrder("createdAt");
            imageQuery.setLimit(1000);

            try {

                List<ParseObject> imageObjects = imageQuery.find();
                for (ParseObject o: imageObjects){

                    Image image = new Image();

                    String url = o.getString("photo_medium_url");
                    String thumbUrl = o.getString("photo_thumb_url");
                    Log.d("Evidence","thumb url = "+thumbUrl);

                    Log.d("Evidence","photo url = "+url);

                    String date = o.getString("date");
                    ParseGeoPoint location = o.getParseGeoPoint("location");

                    image.setUrl(url);
                    image.setThumbUrl(thumbUrl);

                    image.setDate(date);
                    image.setLocation(location);

                    imageList.add(image);

                }

            } catch (ParseException e) {
                e.printStackTrace();
            }


            ParseQuery videoQuery = ParseQuery.getQuery("EmergencyVideo");
            videoQuery.whereEqualTo("name",name);
            //  videoQuery.whereGreaterThan("updatedAt",d);

            videoQuery.addDescendingOrder("createdAt");
            videoQuery.setLimit(1000);

            try {

                List<ParseObject> videoObjects = videoQuery.find();

                for (ParseObject o:videoObjects){

                    Video video = new Video();

                    String id = o.getString("VideoID");
                    String date = o.getString("date");
                    ParseGeoPoint location = o.getParseGeoPoint("location");

                    video.setLocation(location);
                    video.setDate(date);
                    video.setId(id);

                    String thumb = getVideoThumbnail(id);

                    video.setThumb(thumb);


                    videoList.add(video);

                }


            } catch (ParseException e) {
                e.printStackTrace();
            }


            ParseQuery audioQuery = ParseQuery.getQuery("EmergencyAudio");
            //   audioQuery.whereGreaterThan("updatedAt",d);
            audioQuery.whereEqualTo("name",name);

            audioQuery.addDescendingOrder("createdAt");
            audioQuery.setLimit(1000);
            try {

                List<ParseObject> audioObjects = audioQuery.find();
                for (ParseObject o: audioObjects){

                    Audio audio = new Audio();

                    String url = o.getParseFile("AudioFile").getUrl();

                    String date = o.getString("date");

                    audio.setUrl(url);
                    audio.setDate(date);

                    audioList.add(audio);

                }

            } catch (ParseException e) {
                e.printStackTrace();
            }


            ParseQuery investigatorQuery = ParseQuery.getQuery("GeoXman");
            investigatorQuery.whereEqualTo("Installation_id",invest_insta_id);
            Log.d("Evidence","investigator  query  invest_insta_id"+invest_insta_id);


            try {

                ParseObject parseObject = investigatorQuery.getFirst();

                investigator_name = parseObject.getString("XManName");

                Log.d("Evidence","investigator  found name "+investigator_name);



            } catch (ParseException e) {

                e.printStackTrace();

                Log.d("Evidence","investigator not found exception = "+e);

                investigator_name = "NA";

            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {


            progressBar.setVisibility(View.GONE);

            if (investigator_name!=null){

                tvInvestName.setText(investigator_name);

            }else {
                Log.d("Evidence","investigator_name is  = null");

            }

            if (imageList.size()==0 && videoList.size()==0 && audioList.size()==0){

                textViewDataNoAva.setVisibility(View.VISIBLE);

            }

            Log.d("Evidence","imageList.size = "+imageList.size());


            if (imageList.size()>=1){

                tvImage.setVisibility(View.VISIBLE);
                gvImages.setVisibility(View.VISIBLE);

                EvidenceImageGridViewAdapter imageGridViewAdapter = new EvidenceImageGridViewAdapter(context,imageList);
                gvImages.setAdapter(imageGridViewAdapter);

                gvImages.getViewTreeObserver().addOnGlobalLayoutListener( new ViewTreeObserver.OnGlobalLayoutListener()
                {
                    @Override
                    public void onGlobalLayout()
                    {
                        try {

                            gvImages.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            View lastChild = gvImages.getChildAt(gvImages.getChildCount() - 1);
                            gvImages.setLayoutParams(new LinearLayout.LayoutParams(GridLayout.LayoutParams.FILL_PARENT, lastChild.getBottom()));

                        }catch (Exception e){

                            e.printStackTrace();
                            Log.d("VictimsMap","Exception e = "+e);
                        }
                    }
                });



            }else{

                tvImage.setVisibility(View.GONE);
                gvImages.setVisibility(View.GONE);


                //  lvImages.setVisibility(View.GONE);

            }

            Log.d("Evidence","videoList.size = "+videoList.size());

            if (videoList.size()>=1){

                tvVideo.setVisibility(View.VISIBLE);
                lvVideos.setVisibility(View.VISIBLE);
                EvidenceVideoListAdapter adapter = new EvidenceVideoListAdapter(context,videoList);
                lvVideos.setAdapter(adapter);

            }else{
                tvVideo.setVisibility(View.GONE);
                lvVideos.setVisibility(View.GONE);

            }

            Log.d("Evidence","audioList.size = "+audioList.size());

            if (audioList.size()>=1){

                tvAudio.setVisibility(View.VISIBLE);
                lvAudio.setVisibility(View.VISIBLE);

                EvidenceAudioListAdapter audioListAdapter = new EvidenceAudioListAdapter(context,audioList);
                lvAudio.setAdapter(audioListAdapter);

            }
            else{
                tvAudio.setVisibility(View.GONE);
                lvAudio.setVisibility(View.GONE);

            }



            //  ListUtils.setDynamicHeight(lvImages);
            VictimsMapActivity.ListUtils.setDynamicHeight(lvVideos);
            VictimsMapActivity.ListUtils.setDynamicHeight(lvAudio);

        }

    }


    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }


}
