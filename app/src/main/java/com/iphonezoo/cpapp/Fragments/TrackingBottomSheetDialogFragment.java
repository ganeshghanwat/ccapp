package com.iphonezoo.cpapp.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.iphonezoo.cpapp.POJO.GeoPoint;
import com.iphonezoo.cpapp.R;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;


public class TrackingBottomSheetDialogFragment extends DialogFragment {

   String name;

    View view;

    private SupportMapFragment mSupportMapFragment;

    GoogleMap map;

    List<GeoPoint> geoPointList = new ArrayList<>();

    ProgressDialog progressDialog;

    public static TrackingBottomSheetDialogFragment newInstance(String string) {
        TrackingBottomSheetDialogFragment f = new TrackingBottomSheetDialogFragment();
        Bundle args = new Bundle();
        args.putString("name", string);
        f.setArguments(args);
        return f;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        name = getArguments().getString("name");

        Log.d("Tracking","TrackingBottimSheetDialogFragment  onCreate  name = "+name);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        /**

         view = inflater.inflate(R.layout.tracking_bottom_sheet, container, false);
        // Gets the MapView from the XML layout and creates it
        mapView = (MapView) view.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);
        // Gets to GoogleMap from the MapView and does initialization stuff
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                map = googleMap;

                map.getUiSettings().setMyLocationButtonEnabled(false);
                //  map.setMyLocationEnabled(true);

                // Updates the location and zoom of the MapView
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(43.1, -87.9), 10);
                map.animateCamera(cameraUpdate);

            }
        });

**/




        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.tracking_bottom_sheet, container, false);

            mSupportMapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.mapDialogBoxVictimsTrack);

            if (mSupportMapFragment == null) {

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                mSupportMapFragment = SupportMapFragment.newInstance();
                fragmentTransaction.replace(R.id.mapDialogBoxVictimsTrack, mSupportMapFragment).commit();

            }

        } catch (InflateException e) {

            e.printStackTrace();
            Log.d("Tracking","inflate exception "+e);
        }

        if (mSupportMapFragment!=null) {

            mSupportMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    map = googleMap;

                    Log.d("Tracking","TrackingBottimSheetDialogFragment caLLING  GetLocationData().execute();");

                    if (isNetworkAvailable()) {

                        new GetLocationData().execute();

                    }else {

                        Toast.makeText(getActivity(), R.string.no_internet, Toast.LENGTH_SHORT).show();
                    }

                }
            });

        }



        return view;
    }

    @Override
    public void onResume() {
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = LinearLayout.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("Tracking","  onDestroyView ");

        try {
            // Fragment fragment = (getFragmentManager().findFragmentById(R.id.mapDialogBoxAddSafeZone));
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.remove(mSupportMapFragment);
            ft.commit();
        }catch (Exception e){
            e.printStackTrace();
            Log.d("Tracking","  onDestroyView exception  "+e);

        }

    }

    private class GetLocationData extends AsyncTask<Void,Void,Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            geoPointList.clear();

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("loading...");
            progressDialog.show();

        }


        @Override
        protected Void doInBackground(Void... params) {

            ParseQuery<ParseObject> query = ParseQuery.getQuery("LocationUpdate");
          //  query.whereEqualTo("Installation_id", ParseInstallation.getCurrentInstallation().getObjectId());
            query.whereEqualTo("name", name);
            query.addAscendingOrder("updatedAt");
            /**
            if (sDate!=null){
                query.whereGreaterThanOrEqualTo("updatedAt",sDate);
            }
            if (eDate!=null){
                query.whereLessThanOrEqualTo("updatedAt",eDate);
            }
             **/

            query.setLimit(1000);

            List<ParseObject> parseObjectList = null;

            try {

                parseObjectList = query.find();

                System.out.println("ParseObj list :" + parseObjectList);


                for (ParseObject o : parseObjectList) {

                    ParseGeoPoint point = o.getParseGeoPoint("location");

                    Double lat = point.getLatitude();
                    Double lng = point.getLongitude();

                    System.out.println("LatLong" + lat + lng);

                    String time = o.getString("date");

                    GeoPoint geoPoint = new GeoPoint();
                    geoPoint.setLat(lat);
                    geoPoint.setLng(lng);
                    geoPoint.setDate(time);

                    geoPointList.add(geoPoint);

                }

            } catch (ParseException e) {
                e.printStackTrace();

                Throwable th =  e.getCause();

                Log.d("Tracking","TrackingBottimSheetDialogFragment exception = "+e);


                if (th instanceof SocketTimeoutException){

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(getActivity(), "Internal Server Error", Toast.LENGTH_SHORT).show();

                        }
                    });

                }

                if (th instanceof ConnectException) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(getActivity(), "Network Error", Toast.LENGTH_SHORT).show();

                        }
                    });
                }

            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            progressDialog.dismiss();

            try {

                if (geoPointList.size() >= 1) {

                    for (int i = 0; i < geoPointList.size(); i++) {

                        View marker = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);
                        TextView numTxt = (TextView) marker.findViewById(R.id.num_txt);

                        numTxt.setText((i + 1) + "");



                        Marker target = map.addMarker(new MarkerOptions()
                                .position(new LatLng(geoPointList.get(i).getLat(), geoPointList.get(i).getLng()))
                                .title(name)
                                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker)))
                                .snippet(geoPointList.get(i).getDate())
                        );

                        target.showInfoWindow();

                        /**
                         map.addMarker(new MarkerOptions()
                         .position(new LatLng(parseGeoPointList.get(i).getLatitude(), parseGeoPointList.get(i).getLongitude()))
                         .title(victimName))
                         .setSnippet(timeList.get(i));

                         **/

                    }

                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(geoPointList.get(0).getLat(), geoPointList.get(0).getLng())).zoom(17).build();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                    map.moveCamera(cameraUpdate);

                    // Move the camera instantly to target with a zoom of 15.
                    //  map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(parseGeoPointList.get(0).getLatitude(), parseGeoPointList.get(0).getLongitude()), 17));




                    // Zoom in, animating the camera.
                    //  map.animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);

                }
            }
            catch (Exception e){

                e.printStackTrace();
                Log.d("Tracking","TrackingBottimSheetDialogFragment ...xception occured.."+e);
            }
        }


    }

    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.WRAP_CONTENT, AbsListView.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }


}
