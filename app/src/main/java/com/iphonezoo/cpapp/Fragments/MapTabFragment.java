package com.iphonezoo.cpapp.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.iphonezoo.cpapp.Activity.VictimEvidenceTabbedActivity;
import com.iphonezoo.cpapp.POJO.GeoPoint;
import com.iphonezoo.cpapp.POJO.Image;
import com.iphonezoo.cpapp.R;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MapTabFragment extends Fragment {

   ProgressDialog mProgressDialog;

   static List<GeoPoint> geoPointList = new ArrayList<>();

    public static List<GeoPoint> getGeoPointList() {
        return geoPointList;
    }

    String victimName;

    GoogleMap map;
    MapFragment mapFragment;

    String locationsJSON;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        victimName = VictimEvidenceTabbedActivity.getVictimName();
        Log.d("Evidence", "victim name = " + victimName);

        locationsJSON = VictimEvidenceTabbedActivity.getLocationsJSON();

        if (locationsJSON!=null){

            try {

                JSONArray jsonArray = new JSONArray(locationsJSON);

                for (int i =0 ; i<jsonArray.length();i++){

                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    GeoPoint geoPoint = new GeoPoint();
                    geoPoint.setDate(jsonObject.getString("date"));
                    geoPoint.setLat(jsonObject.getDouble("lat"));
                    geoPoint.setLng(jsonObject.getDouble("lng"));

                    geoPointList.add(geoPoint);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_map_tab, container, false);

        mapFragment = ((MapFragment)getActivity().getFragmentManager().findFragmentById(R.id.mapDialogBox));

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                map = googleMap;

                new GetLocationData().execute();

            }
        });





        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private class GetLocationData extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(Void... params) {




            return null;
        }

        @Override
        protected void onPostExecute(Void result) {


            try {

                if (geoPointList.size() >= 1) {

                    for (int i = 0; i < geoPointList.size(); i++) {


                        Marker target = map.addMarker(new MarkerOptions().position(new LatLng(geoPointList.get(i).getLat(), geoPointList.get(i).getLng()))
                                .title(victimName)
                               // .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(getActivity(), marker)))
                                .snippet(geoPointList.get(i).getDate())
                        );

                        target.showInfoWindow();

                        /**
                        map.addMarker(new MarkerOptions()
                                .position(new LatLng(parseGeoPointList.get(i).getLatitude(), parseGeoPointList.get(i).getLongitude()))
                                .title(victimName))
                                .setSnippet(timeList.get(i));

                         **/

                        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(geoPointList.get(i).getLat(), geoPointList.get(i).getLng())).zoom(17).build();
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                        map.moveCamera(cameraUpdate);

                    }

                    // Move the camera instantly to target with a zoom of 15.
                  //  map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(parseGeoPointList.get(0).getLatitude(), parseGeoPointList.get(0).getLongitude()), 17));




                    // Zoom in, animating the camera.
                  //  map.animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);

                }
            }
            catch (Exception e){

                Log.d("Map","MapFragment ...Wxception occured.."+e);
            }
            }


    }


}
