 package com.iphonezoo.cpapp.Fragments;

 import android.app.Activity;
 import android.app.Dialog;
 import android.app.ProgressDialog;
 import android.content.Intent;
 import android.os.AsyncTask;
 import android.os.Bundle;
 import android.os.Handler;
 import android.support.design.widget.FloatingActionButton;
 import android.support.v4.app.Fragment;
 import android.util.Log;
 import android.view.LayoutInflater;
 import android.view.View;
 import android.view.ViewGroup;
 import android.view.ViewTreeObserver;
 import android.view.Window;
 import android.widget.AdapterView;
 import android.widget.Button;
 import android.widget.GridLayout;
 import android.widget.GridView;
 import android.widget.ImageView;
 import android.widget.LinearLayout;
 import android.widget.ListAdapter;
 import android.widget.ListView;
 import android.widget.TextView;
 import android.widget.Toast;


 import com.iphonezoo.cpapp.Activity.VictimEvidenceTabbedActivity;
 import com.iphonezoo.cpapp.Adapters.EvidenceAudioListAdapter;
 import com.iphonezoo.cpapp.Adapters.EvidenceImageGridViewAdapter;
 import com.iphonezoo.cpapp.Adapters.EvidenceVideoListAdapter;
 import com.iphonezoo.cpapp.POJO.Audio;
 import com.iphonezoo.cpapp.POJO.Image;
 import com.iphonezoo.cpapp.POJO.Video;
 import com.iphonezoo.cpapp.R;
 import com.nostra13.universalimageloader.core.DisplayImageOptions;
 import com.nostra13.universalimageloader.core.ImageLoader;
 import com.parse.ParseException;
 import com.parse.ParseGeoPoint;
 import com.parse.ParseInstallation;
 import com.parse.ParseObject;
 import com.parse.ParseQuery;
 import com.parse.SaveCallback;

 import org.json.JSONArray;
 import org.json.JSONException;
 import org.json.JSONObject;

 import java.util.ArrayList;
 import java.util.List;


 public class EvidenceTabFragment extends Fragment {

     String victimName;
     String victimContact;
     long victimFbID;

     ProgressDialog mProgressDialog;

  //   ListView lvImages;
     ListView lvVideos;
     ListView lvAudio;

     TextView tvImage;
     TextView tvVideo;
     TextView tvAudio;


     List<Image> imageList = new ArrayList<>();
     List<Video> videoList = new ArrayList<>();
     List<Audio> audioList = new ArrayList<>();


     private Handler mUiHandler = new Handler();

     GridView gvImages;


     String imagesJSON;
     String videosJSON;
     String audiosJSON;



     @Override
     public void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);

        victimName = VictimEvidenceTabbedActivity.getVictimName();
         imagesJSON = VictimEvidenceTabbedActivity.getImagesJSON();
         videosJSON = VictimEvidenceTabbedActivity.getVideosJSON();
         audiosJSON = VictimEvidenceTabbedActivity.getAudiosJSON();
         Log.d("Evidence","victim name = "+victimName);
         Log.d("Evidence","victim imagesJSON = "+imagesJSON);
         Log.d("Evidence","victim videosJSON = "+videosJSON);
         Log.d("Evidence","victim audiosJSON = "+audiosJSON);

         if (imagesJSON!=null){

             try {

                 JSONArray jsonArray = new JSONArray(imagesJSON);

                 for (int i =0 ; i<jsonArray.length();i++){

                     JSONObject jsonObject = jsonArray.getJSONObject(i);

                     Image image = new Image();

                     image.setDate(jsonObject.getString("date"));
                     image.setUrl(jsonObject.getString("url"));
                     image.setThumbUrl(jsonObject.getString("url"));
                     image.setLat(jsonObject.getDouble("lat"));
                     image.setLng(jsonObject.getDouble("lng"));

                     imageList.add(image);

                 }

             } catch (JSONException e) {
                 e.printStackTrace();
             }
         }


         if (videosJSON!=null){

             try {

                 JSONArray jsonArray = new JSONArray(videosJSON);

                 for (int i =0 ; i<jsonArray.length();i++){

                     JSONObject jsonObject = jsonArray.getJSONObject(i);

                     Video video = new Video();
                     video.setDate(jsonObject.getString("date"));
                     video.setId(jsonObject.getString("videoID"));
                     video.setLat(jsonObject.getDouble("lat"));
                     video.setLng(jsonObject.getDouble("lng"));

                     videoList.add(video);

                 }

             } catch (JSONException e) {
                 e.printStackTrace();
             }
         }

         if (audiosJSON!=null){

             try {

                 JSONArray jsonArray = new JSONArray(audiosJSON);

                 for (int i =0 ; i<jsonArray.length();i++){

                     JSONObject jsonObject = jsonArray.getJSONObject(i);

                     Audio audio = new Audio();

                     audio.setDate(jsonObject.getString("date"));
                     audio.setUrl(jsonObject.getString("url"));
                     audio.setLat(jsonObject.getDouble("lat"));
                     audio.setLng(jsonObject.getDouble("lng"));

                     audioList.add(audio);

                 }

             } catch (JSONException e) {
                 e.printStackTrace();
             }
         }



     }

     @Override
     public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {

         View view = inflater.inflate(R.layout.fragment_evidence_tab, container, false);

         gvImages = (GridView)view.findViewById(R.id.gridView);


         gvImages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
             @Override
             public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                 final Dialog nagDialog = new Dialog(getActivity(),android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                 nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                 nagDialog.setCancelable(false);
                 nagDialog.setContentView(R.layout.preview_image);
                 Button btnClose = (Button)nagDialog.findViewById(R.id.btnIvClose);
                 ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);

                 ImageLoader imageLoader = ImageLoader.getInstance();
                 DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                         .cacheOnDisc(true).resetViewBeforeLoading(true)
                         .build();

                 imageLoader.displayImage(imageList.get(position).getUrl(), ivPreview, options);

                 btnClose.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View arg0) {

                         nagDialog.dismiss();
                     }
                 });
                 nagDialog.show();

             }
         });


       //  lvImages = (ListView)view.findViewById(R.id.listViewImages);

             lvAudio = (ListView)view.findViewById(R.id.listViewAudio);
             lvVideos = (ListView)view.findViewById(R.id.listViewVideos);


         tvImage = (TextView)view.findViewById(R.id.textViewImages);
             tvVideo = (TextView)view.findViewById(R.id.textViewVideoss);
             tvAudio = (TextView)view.findViewById(R.id.textViewAudio);

             new GetAllVictims().execute();

         return view;
     }


     @Override
     public void onAttach(Activity activity) {
         super.onAttach(activity);

     }

     @Override
     public void onDetach() {
         super.onDetach();
     }


     private class GetAllVictims extends AsyncTask<Void,Void,Void> {

         @Override
         protected void onPreExecute() {
             super.onPreExecute();

         }


         @Override
         protected Void doInBackground(Void... params) {



             return null;
         }

         @Override
         protected void onPostExecute(Void result) {


             if (imageList.size()>=1){

                 tvImage.setVisibility(View.VISIBLE);
                 gvImages.setVisibility(View.VISIBLE);
               //  lvImages.setVisibility(View.VISIBLE);
               //  EvidenceImageListAdapter adapter = new EvidenceImageListAdapter(getActivity(),imageList);
               //  lvImages.setAdapter(adapter);


                 EvidenceImageGridViewAdapter imageGridViewAdapter = new EvidenceImageGridViewAdapter(getActivity(),imageList);
                 gvImages.setAdapter(imageGridViewAdapter);

                 gvImages.getViewTreeObserver().addOnGlobalLayoutListener( new ViewTreeObserver.OnGlobalLayoutListener()
                 {
                     @Override
                     public void onGlobalLayout()
                     {
                         gvImages.getViewTreeObserver().removeGlobalOnLayoutListener( this );
                         View lastChild = gvImages.getChildAt( gvImages.getChildCount() - 1 );
                         gvImages.setLayoutParams( new LinearLayout.LayoutParams( GridLayout.LayoutParams.FILL_PARENT, lastChild.getBottom() ) );
                     }
                 });



             }else{

                 tvImage.setVisibility(View.GONE);
                 gvImages.setVisibility(View.GONE);

                 Toast.makeText(getActivity(), "Image Data Not Available", Toast.LENGTH_SHORT).show();


               //  lvImages.setVisibility(View.GONE);

             }

             if (videoList.size()>=1){

                 tvVideo.setVisibility(View.VISIBLE);
                 lvVideos.setVisibility(View.VISIBLE);
                 EvidenceVideoListAdapter adapter = new EvidenceVideoListAdapter(getActivity(),videoList);
                 lvVideos.setAdapter(adapter);

             }else{

                 tvVideo.setVisibility(View.GONE);
                 lvVideos.setVisibility(View.GONE);

                 Toast.makeText(getActivity(), "Video Data Not Available", Toast.LENGTH_SHORT).show();


             }

             if (audioList.size()>=1){

                 tvAudio.setVisibility(View.VISIBLE);
                 lvAudio.setVisibility(View.VISIBLE);

                 EvidenceAudioListAdapter audioListAdapter = new EvidenceAudioListAdapter(getActivity(),audioList);
                 lvAudio.setAdapter(audioListAdapter);

             }
             else{
                 tvAudio.setVisibility(View.GONE);
                 lvAudio.setVisibility(View.GONE);

                 Toast.makeText(getActivity(), "Audio Data Not Available", Toast.LENGTH_SHORT).show();


             }



           //  ListUtils.setDynamicHeight(lvImages);
             ListUtils.setDynamicHeight(lvVideos);
             ListUtils.setDynamicHeight(lvAudio);

         }

     }

     public static class ListUtils {
         public static void setDynamicHeight(ListView mListView) {
             ListAdapter mListAdapter = mListView.getAdapter();
             if (mListAdapter == null) {
                 // when adapter is null
                 return;
             }
             int height = 0;
             int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
             for (int i = 0; i < mListAdapter.getCount(); i++) {
                 View listItem = mListAdapter.getView(i, null, mListView);
                 listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                 height += listItem.getMeasuredHeight();
             }
             ViewGroup.LayoutParams params = mListView.getLayoutParams();
             params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
             mListView.setLayoutParams(params);
             mListView.requestLayout();
         }
     }



 }
