package com.iphonezoo.cpapp;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.iphonezoo.cpapp.Adapters.EvidenceAudioListAdapter;
import com.iphonezoo.cpapp.Adapters.EvidenceImageGridViewAdapter;
import com.iphonezoo.cpapp.Adapters.EvidenceVideoListAdapter;
import com.iphonezoo.cpapp.Fragments.EvidenceDialogFragment;
import com.iphonezoo.cpapp.Fragments.TrackingBottomSheetDialogFragment;
import com.iphonezoo.cpapp.POJO.Audio;
import com.iphonezoo.cpapp.POJO.CCTV;
import com.iphonezoo.cpapp.POJO.Hospitals;
import com.iphonezoo.cpapp.POJO.Image;
import com.iphonezoo.cpapp.POJO.Landmarks;
import com.iphonezoo.cpapp.POJO.PoliceStations;
import com.iphonezoo.cpapp.POJO.SecuredZone;
import com.iphonezoo.cpapp.POJO.UnsafeZones;
import com.iphonezoo.cpapp.POJO.Video;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseLiveQueryClient;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SubscriptionHandling;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;

public class VictimsMapActivity extends AppCompatActivity{


    private final static float CLICK_DRAG_TOLERANCE = 10; // Often, there will be a slight, unintentional, drag when the user taps the FAB, so we need to account for this.

    private float downRawX, downRawY;
    private float dX, dY;



    // cctv

    final static String USERNAME = "admin";
    final static String PASSWORD = "";
    public static String domainName = "xxx.dvrdns.org";
    public static int PORT = 2218;
  //  public static final String URL = "rtsp://192.168.2.6:554/user=" + USERNAME + "&password=" + PASSWORD + "&channel=1&stream=0.sdp?real_stream--rtp-caching=100";
    public static final String URL = "rtsp://172.17.60.240:554/user=" + USERNAME + "&password=" + PASSWORD + "&channel=1&stream=1.sdp?real_stream--rtp-caching=100";
    public static final String URL2 = "rtsp://" + domainName + ":" + PORT + "/user=" + USERNAME + "&password=" + PASSWORD + "&channel=1&stream=1.sdp?real_stream--rtp-caching=100";



    private ClusterManager<SecuredZone> mClusterManager ;
    private ClusterManager<Landmarks> mClusterManagerLandmarks;
    private ClusterManager<Hospitals> mClusterManagerHospitals;
    private ClusterManager<PoliceStations> mClusterManagerPoliceStations;
    private ClusterManager<GeoXman> mClusterManagerInvestigators;
    private ClusterManager<CCTV> mClusterManagerCCTV;

    Point p;

    // Evidences
    ListView lvVideos;
    ListView lvAudio;
    TextView tvImage;
    TextView tvVideo;
    TextView tvAudio;
    TextView textViewDataNoAva;
    List<Image> imageList = new ArrayList<>();
    List<Video> videoList = new ArrayList<>();
    List<Audio> audioList = new ArrayList<>();
    GridView gvImages;
    TextView tvInvestName;

    GetVictimsEvidenceData getVictimsEvidenceData;

    private BottomSheetBehavior mBottomSheetBehavior1;

    String victimNameSelected;


    GoogleMap map;

    MapFragment mapFragment;

    Spinner spinner;

    Button btInvestigators;
    Button btSafezones;
    Button btUnsafezones;
    Button btHospitals;
    Button btLandmarks;
    Button btPoliceStations;
    Button btVictimTracking;

    boolean showInvMarkers =true;
    boolean showSafezoneMarkers =true;
    boolean showUnsafezoneMarkers =true;
    boolean showHospitalMarkers =true;
    boolean showLandmarkMarkers =true;
    boolean showPoliceStationsMarkers =true;

    String stateSelected;

    List<Victim> victimList = new ArrayList<>();
    List<GeoXman> geoXmanList = new ArrayList<>();
    List<UnsafeZones> unsafeZonesList = new ArrayList<>();
    List<SecuredZone> securedZoneList = new ArrayList<>();
    List<Landmarks> landmarksList = new ArrayList<>();
    List<Hospitals> hospitalsList = new ArrayList<>();
    List<PoliceStations> policeStationsList = new ArrayList<>();
    List<CCTV> cctvList = new ArrayList<>();

    List<String> staticStateList = new ArrayList<>(Arrays.asList("West Bengal","Uttarakhand", "Uttar Pradesh", "Tripura", "Telangana", "Tamil Nadu", "Sikkim", "Rajasthan", "Punjab", "Pondicherry", "Odisha","Nagaland", "Mizoram", "Maharashtra", "Meghalaya", "Madhya Pradesh", "Karnataka", "Jharkhand", "Jammu & Kashmir", "Himachal Pradesh", "Haryana", "Gujarat","Goa",
            "Daman & Diu", "Chattisgarh", "Bihar", "Assam", "Arunachal Pradesh", "Andhra Pradesh", "Andaman & Nicobar Islands"));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_victims_map);
       // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
       // setSupportActionBar(toolbar);




        btInvestigators = (Button)findViewById(R.id.buttonInvestigators);
        btSafezones = (Button)findViewById(R.id.buttonSafezones);
        btUnsafezones = (Button)findViewById(R.id.buttonUnsafeZones);
        btHospitals = (Button)findViewById(R.id.buttonHospitals);
        btLandmarks = (Button)findViewById(R.id.buttonLandmarks);
        btPoliceStations = (Button)findViewById(R.id.buttonPoliceStations);
        btVictimTracking = (Button)findViewById(R.id.buttonTrackingVictim);

        View bottomSheet = findViewById(R.id.bottom_sheet1);
        mBottomSheetBehavior1 = BottomSheetBehavior.from(bottomSheet);

        mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapDialogBoxAllVictims));
        spinner = (Spinner)findViewById(R.id.spinnerVictimsMapState);

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                map = googleMap;

                mClusterManager = new ClusterManager<SecuredZone>(VictimsMapActivity.this, map);
                mClusterManagerLandmarks = new ClusterManager<Landmarks>(VictimsMapActivity.this, map);
                mClusterManagerHospitals = new ClusterManager<Hospitals>(VictimsMapActivity.this, map);
                mClusterManagerPoliceStations = new ClusterManager<PoliceStations>(VictimsMapActivity.this, map);
                mClusterManagerInvestigators = new ClusterManager<GeoXman>(VictimsMapActivity.this, map);
                mClusterManagerCCTV = new ClusterManager<CCTV>(VictimsMapActivity.this, map);


                map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {

                        mClusterManager.onCameraIdle();
                        mClusterManagerLandmarks.onCameraIdle();
                        mClusterManagerPoliceStations.onCameraIdle();
                        mClusterManagerHospitals.onCameraIdle();
                        mClusterManagerInvestigators.onCameraIdle();
                        mClusterManagerCCTV.onCameraIdle();

                    }
                });


            }
        });



        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(VictimsMapActivity.this, android.R.layout.simple_spinner_item, staticStateList);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // categoryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(stateAdapter);

        spinner.setSelection(13);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //    Log.d("EZones", "State selected = " + stateList.get(position));

                //   stateSelected = stateList.get(position);

                stateSelected = staticStateList.get(position);

                map.clear();

                geoXmanList.clear();
                unsafeZonesList.clear();
                securedZoneList.clear();
                hospitalsList.clear();
                landmarksList.clear();
                policeStationsList.clear();
                cctvList.clear();


                mClusterManager.clearItems();
                mClusterManager.cluster();
                mClusterManagerLandmarks.clearItems();
                mClusterManagerLandmarks.cluster();
                mClusterManagerPoliceStations.clearItems();
                mClusterManagerPoliceStations.cluster();
                mClusterManagerHospitals.clearItems();
                mClusterManagerHospitals.cluster();
                mClusterManagerInvestigators.clearItems();
                mClusterManagerInvestigators.cluster();
                mClusterManagerCCTV.clearItems();
                mClusterManagerCCTV.cluster();


                new GetVictimsDataTask().execute();

                new GetUnsafeZonesDataTask().execute();

                new GetSecuredZonesDataTask().execute();

                new GetHospitalsDataTask().execute();

                new GetLandMarksDataTask().execute();

                new GetPoliceStationsDataTask().execute();

                new GetCCTVDataTask().execute();


                // move camera to state

                List<Address> addressList = null;

                if (stateSelected != null) {

                    Geocoder geocoder = new Geocoder(getApplicationContext());

                    try {

                        addressList = geocoder.getFromLocationName(stateSelected, 1);

                        Address address = addressList.get(0);

                        String locAddress = address.getAddressLine(0);

                        Log.d("Location", "address =  " + address.getAddressLine(0));

                        LatLng location = new LatLng(address.getLatitude(), address.getLongitude());

                        Log.d("Location", "geocoder address LatLng  " + location.toString());

                        map.animateCamera(CameraUpdateFactory.newLatLng(location));

                        // Move the camera instantly to target with a zoom of 15.
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 7));

                        // Zoom in, animating the camera.
                        map.animateCamera(CameraUpdateFactory.zoomTo(7), 2000, null);


                    } catch (Exception e) {

                        e.printStackTrace();

                        Log.d("Location", "geocoder address not found..." + e);

                    //    Toast.makeText(VictimsMapActivity.this, "Location Not Found...try again", Toast.LENGTH_SHORT).show();
                    }
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btInvestigators.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (showInvMarkers){

                    // hide markers

                    mClusterManagerInvestigators.clearItems();
                    mClusterManagerInvestigators.cluster();

                    HideUnhideInvestigators(true);
                    btInvestigators.setBackgroundResource(R.drawable.investigator_icon_light);
                    showInvMarkers = false;

                }else {

                    // show markers

                    mClusterManagerInvestigators.addItems(geoXmanList);
                    mClusterManagerInvestigators.cluster();

                    HideUnhideInvestigators(false);
                    btInvestigators.setBackgroundResource(R.drawable.investigator_icon);
                    showInvMarkers = true;

                }

            }
        });

        btSafezones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (showSafezoneMarkers){

                    // hide safezones

                    mClusterManager.clearItems();
                    mClusterManager.cluster();

                    HideUnhideSafeZones(true);
                    btSafezones.setBackgroundResource(R.drawable.safezone_icon_light);
                    showSafezoneMarkers = false;

                }else {

                    // show

                    mClusterManager.addItems(securedZoneList);
                    mClusterManager.cluster();

                    HideUnhideSafeZones(false);
                    btSafezones.setBackgroundResource(R.drawable.safezone_icon);
                    showSafezoneMarkers = true;

                }

            }
        });

        btUnsafezones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (showUnsafezoneMarkers){

                    HideUnhideUnsafeZone(true);
                    btUnsafezones.setBackgroundResource(R.drawable.unsafezone_icon_light);
                    showUnsafezoneMarkers = false;

                }else {

                    HideUnhideUnsafeZone(false);
                    btUnsafezones.setBackgroundResource(R.drawable.unsafezone_icon);
                    showUnsafezoneMarkers = true;
                }

            }
        });

        btHospitals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (showHospitalMarkers){

                    // hide

                    mClusterManagerHospitals.clearItems();
                    mClusterManagerHospitals.cluster();

                    HideUnhideHospitals(true);
                    btHospitals.setBackgroundResource(R.drawable.hospital_icon_light);
                    showHospitalMarkers = false;

                }else {

                    // show

                    mClusterManagerHospitals.addItems(hospitalsList);
                    mClusterManagerHospitals.cluster();


                    HideUnhideHospitals(false);
                    btHospitals.setBackgroundResource(R.drawable.hospital_icon);
                    showHospitalMarkers = true;
                }

            }
        });


        btLandmarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (showLandmarkMarkers){

                    // hide
                    mClusterManagerLandmarks.clearItems();
                    mClusterManagerLandmarks.cluster();

                    HideUnhideLandmarks(true);
                    btLandmarks.setBackgroundResource(R.drawable.landmark_icon_light);
                    showLandmarkMarkers = false;

                }else {

                    // show
                    mClusterManagerLandmarks.addItems(landmarksList);
                    mClusterManagerLandmarks.cluster();


                    HideUnhideLandmarks(false);
                    btLandmarks.setBackgroundResource(R.drawable.landmark_icon);
                    showLandmarkMarkers = true;
                }

            }
        });


        btPoliceStations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (showPoliceStationsMarkers){

                    // hide

                    mClusterManagerPoliceStations.clearItems();
                    mClusterManagerPoliceStations.cluster();

                    HideUnhidePoliceStations(true);
                    btPoliceStations.setBackgroundResource(R.drawable.police_station_icon_light);
                    showPoliceStationsMarkers = false;

                }else {

                    // show

                    mClusterManagerPoliceStations.addItems(policeStationsList);
                    mClusterManagerPoliceStations.cluster();


                    HideUnhidePoliceStations(false);
                    btPoliceStations.setBackgroundResource(R.drawable.police_icon);
                    showPoliceStationsMarkers = true;
                }

            }
        });


        btVictimTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (victimNameSelected==null){

                    Toast.makeText(VictimsMapActivity.this, "Please Select Victim ", Toast.LENGTH_SHORT).show();

                }else {

                    //  final BottomSheetDialogFragment myBottomSheet = TrackingBottomSheetDialogFragment.newInstance(victimNameSelected);
                    //   final BottomSheetDialogFragment myBottomSheet = new TrackingBottomSheetDialogFragment();

                    Log.d("Tracking", "VictimsMapActivity  victimNameSelected = " + victimNameSelected);

                    //    myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

                    TrackingBottomSheetDialogFragment dd = new TrackingBottomSheetDialogFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("name",victimNameSelected);
                    dd.setArguments(bundle);

                    dd.show(getSupportFragmentManager(),victimNameSelected);

                }

            }
        });

        // handle bottom sheet swipe event
        mBottomSheetBehavior1.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

                if (newState == BottomSheetBehavior.STATE_EXPANDED) {

                    Log.d("BottomSheet","callback newstate = expanded ");


                }
                else if (newState == BottomSheetBehavior.STATE_COLLAPSED) {

                    Log.d("BottomSheet","callback newstate = collapsed ");

                    if (getVictimsEvidenceData!=null){

                        Log.d("BottomSheet","callback canceling  getVictimsEvidenceData task ");

                        getVictimsEvidenceData.cancel(true);

                    }

                }
                else if (newState == BottomSheetBehavior.STATE_HIDDEN) {

                    Log.d("BottomSheet","callback newstate = hidden ");

                }


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });




        ParseLiveQueryClient parseLiveQueryClient = ParseLiveQueryClient.Factory.getClient();

        final ParseQuery parseQuery = ParseQuery.getQuery("AllVictims");

        SubscriptionHandling<ParseObject> subscriptionHandling = parseLiveQueryClient.subscribe(parseQuery);

        subscriptionHandling.handleEvents(new SubscriptionHandling.HandleEventsCallback<ParseObject>() {
            @Override
            public void onEvents(ParseQuery<ParseObject> query, SubscriptionHandling.Event event, ParseObject object) {
                // HANDLING all events

                Log.d("VictimMap","event received ..... parseObject = "+object);

                String name = object.getString("name");
                String date = object.getString("date");
                String invest_insta_id = object.getString("invest_insta_id");
                final ParseGeoPoint location = object.getParseGeoPoint("location");

                final Victim victim = new Victim();

                victim.setName(name);
                victim.setInvest_insta_id(invest_insta_id);
                victim.setLatLng(new LatLng(location.getLatitude(),location.getLongitude()));
                victim.setDate(date);


                // Create new Victim or Update victims location

                OnVictimCreateOrUpdate(victim);

                /**

                if (victimList.contains(victim)){

                     // update marker position...

                   int indexOfVictim =  victimList.indexOf(victim);

                    Log.d("VictimMap","indexOfVictim = "+indexOfVictim);

                    final Victim v = victimList.get(indexOfVictim);

                    v.setLatLng(new LatLng(location.getLatitude(),location.getLongitude()));

                    final Marker marker = v.getMarker();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            marker.setPosition(new LatLng(location.getLatitude(),location.getLongitude()));

                            marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.victim_marker_large));


                            final long start = SystemClock.uptimeMillis();
                            final long duration = 5000;

                                    final Handler handler = new Handler();
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {

                                            long elapsed = SystemClock.uptimeMillis() - start;

                                            Log.d("VictimsMap","elapsed time = "+elapsed);

                                            if (elapsed>duration){

                                                Log.d("VictimsMap","changing marker elapsed time is greater than 15000");

                                                marker.setVisible(true);

                                                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.victim_red));

                                            }else {

                                               if (marker.isVisible()){

                                                   Log.d("VictimsMap","marker is set to invisible");

                                                   marker.setVisible(false);

                                               }else {

                                                   Log.d("VictimsMap","marker is set to visible");

                                                   marker.setVisible(true);
                                               }

                                                Log.d("VictimsMap","changing marker color to normal");

                                                 //   marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.victim_red));

                                                handler.postDelayed(this,1000);
                                            }

                                            // change marker color to normal....
                                        }
                                    });



                        }
                    });

                }else {
                    // add new marker

                    Log.d("VictimMap","new Victim added");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                    final Marker target = map.addMarker(new MarkerOptions()
                            .position(victim.getLatLng())
                            .title(victim.getName())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.victim_marker_large)));
                    // Move the camera instantly to target with a zoom of 15.
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(victim.getLatLng(), 15));
                            victim.setMarker(target);
                            victimList.add(victim);

                            target.setTag("victim");

                            final long start = SystemClock.uptimeMillis();
                            final long duration = 5000;

                            final Handler handler = new Handler();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {

                                    long elapsed = SystemClock.uptimeMillis() - start;

                                    Log.d("VictimsMap","elapsed time = "+elapsed);

                                    if (elapsed>duration){

                                        Log.d("VictimsMap","changing marker elapsed time is greater than 15000");

                                        target.setVisible(true);

                                        target.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.victim_red));

                                    }else {

                                        if (target.isVisible()){

                                            Log.d("VictimsMap","marker is set to invisible");

                                            target.setVisible(false);

                                        }else {

                                            Log.d("VictimsMap","marker is set to visible");

                                            target.setVisible(true);
                                        }

                                        Log.d("VictimsMap","changing marker color to normal");

                                         //  target.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.victim_red));

                                        handler.postDelayed(this,1000);
                                    }

                                    // change marker color to normal....
                                }
                            });

                        }
                    });

                }

                **/

            }
        });


        // show image popup dialog on new image update

        final ParseQuery parseQueryImage = ParseQuery.getQuery("EmergencyImage");

        SubscriptionHandling<ParseObject> subscriptionHandlingImage = parseLiveQueryClient.subscribe(parseQueryImage);

        subscriptionHandlingImage.handleEvents(new SubscriptionHandling.HandleEventsCallback<ParseObject>() {
            @Override
            public void onEvents(ParseQuery<ParseObject> query, SubscriptionHandling.Event event, ParseObject object) {

                Log.d("VictimsMap","new Image update parse live query");

                final String url = object.getString("photo_medium_url");
                final String name = object.getString("name");
                Log.d("VictimsMap","............. showPopUp()   imageURL =  "+url);
                 ParseGeoPoint location = object.getParseGeoPoint("location");

                String date = object.getString("date");
                String invest_insta_id = object.getString("Installation_id");

                int countIndex = (int) object.getNumber("countIndex");

                final Victim victim = new Victim();

                victim.setName(name);
                victim.setInvest_insta_id(invest_insta_id);
                victim.setLatLng(new LatLng(location.getLatitude(),location.getLongitude()));
                victim.setDate(date);

                int c = countIndex%5;

                if (countIndex==1 || c == 0){

                    OnVictimCreateOrUpdate(victim);

                    final LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Log.d("VictimsMap"," image update ....  runOnUiThread ");

                            try {
                                showPopup(VictimsMapActivity.this, p, url, name, latLng);
                            }catch (Exception e){

                                e.printStackTrace();
                                Log.d("VictimsMap"," Exception "+e);
                            }

                        }
                    });

                }else {

                    Log.d("VictimsMap","............. showPopUp()   countindex is not 1  And c is not 0 = "+c);

                }

            }
        });



    }

    private void OnVictimCreateOrUpdate(final Victim victim){


        if (victimList.contains(victim)){

            // update marker position...

            int indexOfVictim =  victimList.indexOf(victim);

            Log.d("VictimMap","indexOfVictim = "+indexOfVictim);

            final Victim v = victimList.get(indexOfVictim);

            v.setLatLng(victim.getLatLng());

            final Marker marker = v.getMarker();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    marker.setPosition(victim.getLatLng());

                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.victim_marker_large));

                    final long start = SystemClock.uptimeMillis();
                    final long duration = 5000;

                    final Handler handler = new Handler();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            long elapsed = SystemClock.uptimeMillis() - start;

                            Log.d("VictimsMap","elapsed time = "+elapsed);

                            if (elapsed>duration){

                                Log.d("VictimsMap","changing marker elapsed time is greater than 15000");

                                marker.setVisible(true);

                                marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.victim_red));

                            }else {

                                if (marker.isVisible()){

                                    Log.d("VictimsMap","marker is set to invisible");

                                    marker.setVisible(false);

                                }else {

                                    Log.d("VictimsMap","marker is set to visible");

                                    marker.setVisible(true);
                                }

                                Log.d("VictimsMap","changing marker color to normal");

                                //   marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.victim_red));

                                handler.postDelayed(this,1000);
                            }

                            // change marker color to normal....
                        }
                    });



                    /**

                     final Handler handler = new Handler();
                     final long start = SystemClock.uptimeMillis();
                     final long duration = 2500;

                     final Interpolator interpolator = new BounceInterpolator();

                     handler.post(new Runnable() {
                    @Override
                    public void run() {
                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = Math.max(
                    1 - interpolator.getInterpolation((float) elapsed
                    / duration), 0);

                    marker.setAnchor(0.5f, 1.0f + 6 * t);

                    if (t > 0.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                    }
                    }
                    });

                     */


                }
            });

        }else {
            // add new marker

            Log.d("VictimMap","new Victim added");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    final Marker target = map.addMarker(new MarkerOptions()
                            .position(victim.getLatLng())
                            .title(victim.getName())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.victim_marker_large)));
                    // Move the camera instantly to target with a zoom of 15.
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(victim.getLatLng(), 15));
                    victim.setMarker(target);
                    victimList.add(victim);

                    target.setTag("victim");

                    final long start = SystemClock.uptimeMillis();
                    final long duration = 5000;

                    final Handler handler = new Handler();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            long elapsed = SystemClock.uptimeMillis() - start;

                            Log.d("VictimsMap","elapsed time = "+elapsed);

                            if (elapsed>duration){

                                Log.d("VictimsMap","changing marker elapsed time is greater than 15000");

                                target.setVisible(true);

                                target.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.victim_red));

                            }else {

                                if (target.isVisible()){

                                    Log.d("VictimsMap","marker is set to invisible");

                                    target.setVisible(false);

                                }else {

                                    Log.d("VictimsMap","marker is set to visible");

                                    target.setVisible(true);
                                }

                                Log.d("VictimsMap","changing marker color to normal");

                                //  target.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.victim_red));

                                handler.postDelayed(this,1000);
                            }

                            // change marker color to normal....
                        }
                    });

                    /**

                     final Handler handler = new Handler();
                     handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    // change marker color to normal....

                    Log.d("VictimsMap","changing marker color to normal");

                    target.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.victim_red));

                    }
                    }, 30000);

                     **/

                }
            });

        }

    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        int[] location = new int[2];
      //  Button button = (Button) findViewById(R.id.show_popup);

        // Get the x, y location and store it in the location[] array
        // location[0] = x, location[1] = y.
        btHospitals.getLocationOnScreen(location);

        //Initialize the Point with x, and y positions
        p = new Point();
        p.x = location[0];
        p.y = location[1];
    }


    // The method that displays the popup_layout.
    private void showPopup(final Activity context, Point p, String imageURL,String name,LatLng latLng) {


        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

        Log.d("VictimsMap","............. showPopUp().........");

        int popupWidth = 150;
        int popupHeight = 150;

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_layout, viewGroup);



        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        popup.setFocusable(true);
        popup.setAnimationStyle(R.style.Animations_popup);


        // Some offset to align the popup_layout a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = 0;
        int OFFSET_Y = 0;

        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup_layout at the specified location, + offsets.
     //   popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);


        popup.showAtLocation(layout, Gravity.CENTER, 1 + OFFSET_X, 1 + OFFSET_Y);

        ImageView imageView = (ImageView) layout.findViewById(R.id.imageViewPopUp);

        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .build();

        Log.d("VictimsMap","............. showPopUp()   imageURL =  "+imageURL);


        if(!imageURL.equals("")) {

            Log.d("VictimsMap","............. showPopUp()   imageURL =  "+imageURL);

            imageLoader.displayImage(imageURL, imageView, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                    Log.d("VictimsMap","............. imageLoading   onLoadingStarted =  ");


                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                    Log.d("VictimsMap","............. imageLoading   onLoadingFailed =  "+failReason.getType().name());


                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                    Log.d("VictimsMap","............. imageLoading   onLoadingComplete =  ");


                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                    Log.d("VictimsMap","............. imageLoading   onLoadingCancelled =  ");


                }
            });

        }

        TextView textView = (TextView)layout.findViewById(R.id.textViewPopUpName);

        textView.setText(name);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            popup.dismiss();

            }
        },5000);

    }



    //Async task..
    private class GetVictimsDataTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(VictimsMapActivity.this);
            progressDialog.setMessage("Loading");
            progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            Calendar calendar = Calendar.getInstance();

            calendar.add(Calendar.HOUR,-24);

            Date d = calendar.getTime();


            ParseQuery query = ParseQuery.getQuery("AllVictims");
            query.whereGreaterThan("updatedAt",d);
            query.setLimit(1000);

            try {

                List<ParseObject> parseObjectList =  query.find();
                Log.d("AllVictims","No of states objects = "+parseObjectList.size());

                for (ParseObject parseObject : parseObjectList){

                    String name = parseObject.getString("name");
                    String date = parseObject.getString("date");
                    String invest_insta_id = parseObject.getString("invest_insta_id");
                    ParseGeoPoint location = parseObject.getParseGeoPoint("location");

                    Victim victim = new Victim();
                    victim.setName(name);
                    victim.setInvest_insta_id(invest_insta_id);
                    victim.setLatLng(new LatLng(location.getLatitude(),location.getLongitude()));
                    victim.setDate(date);

                    victimList.add(victim);

                }


            } catch (ParseException e) {

                Log.d("AllVictims","failed to find AllVictims objects");
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result) {

            progressDialog.dismiss();

            Log.d("AllVictims", "victimList.size() = "+victimList.size());

            if (victimList.size()>0){

                for (int i = 0; i<victimList.size(); i++) {

                    if (map != null) {

                        Marker target = map.addMarker(new MarkerOptions()
                                .position(victimList.get(i).getLatLng())
                                .title(victimList.get(i).getName())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.victim_red)));
                        // Move the camera instantly to target with a zoom of 15.
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(victimList.get(i).getLatLng(), 15));

                        target.setTag("victim");

                        victimList.get(i).setMarker(target);

                    } else {

                        Toast.makeText(VictimsMapActivity.this, "Unable to load map", Toast.LENGTH_SHORT).show();
                    }
                }




            }else {

                Toast.makeText(VictimsMapActivity.this, "Victims Data Not Available", Toast.LENGTH_SHORT).show();
            }


            map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                    Intent intent = new Intent(VictimsMapActivity.this,AllEvidencesActivity.class);

                    String markerId = marker.getId();

                    for (int i=0; i<victimList.size(); i++) {

                        //   String id = victimList.get(i).getMarkerId();
                        Marker m = victimList.get(i).getMarker();

                        if (m!=null && m.equals(marker)) {

                            String name = victimList.get(i).getName();
                            String invest_insta_id = victimList.get(i).getInvest_insta_id();

                            intent.putExtra("name",name);

                            victimNameSelected = name;

                            //  startActivity(intent);

                            Log.d("BottomSheet","on Infowindow click ");

                            Log.d("Evidence","investigator invest_insta_id"+invest_insta_id);

                            EvidenceDialogFragment dd = new EvidenceDialogFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("name",name);
                            bundle.putString("invest_insta_id",invest_insta_id);
                            dd.setArguments(bundle);

                            dd.show(getSupportFragmentManager(),victimNameSelected);

                            /**

                            setUpEvidenceBottomView(name,invest_insta_id);


                            if(mBottomSheetBehavior1.getState() != BottomSheetBehavior.STATE_EXPANDED) {

                                mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_EXPANDED);

                                Log.d("BottomSheet","state expanded");


                            }
                            else {

                                mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_COLLAPSED);

                                Log.d("BottomSheet","state collapsed");

                            }

                            */

                            break;
                        }
                    }
                }
            });


            map.setOnMarkerClickListener(onMarkerClickListener);

            /**

            map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {

                    marker.showInfoWindow();

                    Log.d("VictimMap", "Selected marker id = " + marker.getId());

                    for (int i = 0; i<victimList.size(); i++){

                        Marker id = victimList.get(i).getMarker();

                        if (id.equals(marker)) {

                            victimNameSelected = victimList.get(i).getName();

                        }

                    }

                    return true;
                }
            });

             **/



            new GetInvestigatorsDataTask().execute();

        }

    }


    //Async task..
    private class GetInvestigatorsDataTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(VictimsMapActivity.this);
            progressDialog.setMessage("Getting investigators data...");
            progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            Log.d("AllVictims","No of investigators objects  stateSelected = "+stateSelected);


            ParseQuery query = ParseQuery.getQuery("GeoXman");
            query.whereEqualTo("State",stateSelected);
            query.setLimit(1000);

            try {

                List<ParseObject> parseObjectList =  query.find();

                Log.d("AllVictims","No of investigators objects = "+parseObjectList.size());

                for (ParseObject parseObject : parseObjectList){

                    String id = parseObject.getString("XManID");
                    String name = parseObject.getString("XManName");
                    String type = parseObject.getString("XManType");
                    String contactNum = parseObject.getString("XManContactNum");
                    ParseGeoPoint location = parseObject.getParseGeoPoint("location");
                    String area = parseObject.getString("AreaCovered");
                    String address = parseObject.getString("XManAddress");
                    String zoneName = parseObject.getString("ZoneName");
                    int zoneId = parseObject.getInt("ZoneID");
                    String state = parseObject.getString("State");
                    String insta_id = parseObject.getString("Installation_id");


                    GeoXman geoXman = new GeoXman();

                    String imageURL = "";

                    try {

                        imageURL = parseObject.getParseFile("Pic").getUrl();

                    } catch (NullPointerException e) {

                        imageURL = "";

                    }

                    geoXman.setId(id);
                    geoXman.setName(name);
                    geoXman.setType(type);
                    geoXman.setContact(contactNum);
                    geoXman.setLocation(location);
                    geoXman.setArea(area);
                    geoXman.setAddress(address);
                    geoXman.setZoneName(zoneName);
                    geoXman.setZoneID(zoneId);
                    geoXman.setState(state);
                    geoXman.setImgURL(imageURL);
                    geoXman.setInsta_id(insta_id);

                    geoXmanList.add(geoXman);

                }

            } catch (ParseException e) {

                Log.d("AllVictims","failed to find Inverstigators objects");
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result) {

            progressDialog.dismiss();

            Log.d("AllVictims", "geoXmanList.size() = "+geoXmanList.size());

            if (geoXmanList.size()>0){

                for (int i = 0; i<geoXmanList.size(); i++) {

                    if (map != null) {

                        /**

                        Marker target = map.addMarker(new MarkerOptions().
                                position(new LatLng(geoXmanList.get(i).getLocation().getLatitude(),geoXmanList.get(i).getLocation().getLongitude()))
                                .title(geoXmanList.get(i).getName())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.investigator_marker)));
                        // Move the camera instantly to target with a zoom of 15.
                      //  map.moveCamera(CameraUpdateFactory.newLatLngZoom(victimList.get(i).getLatLng(), 14));


                        target.setTag("investigator");

                        geoXmanList.get(i).setMarker(target);

                         **/

                    } else {

                        Toast.makeText(VictimsMapActivity.this, "Unable to load map", Toast.LENGTH_SHORT).show();
                    }
                }

                setUpInvestigatorClusterer();

                map.setInfoWindowAdapter(new MyInfoWindowAdapter());


                /**

                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {

                        Log.d("CarData", "Selected marker id = " + marker.getId());

                        for (int i = 0; i<geoXmanList.size(); i++){

                            Marker id = geoXmanList.get(i).getMarker();

                            if (id.equals(marker)) {

                                marker.showInfoWindow();

                            }

                        }

                        return true;
                    }
                });
*/



            }else {

                Toast.makeText(VictimsMapActivity.this, "Investigators Data Not Available", Toast.LENGTH_SHORT).show();
            }

        }

    }

    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private View myContentsView;

        MyInfoWindowAdapter(){

        //    myContentsView = getLayoutInflater().inflate(R.layout.custom_info_window_layout, null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            String markerId = marker.getId();

            String tag = (String) marker.getTag();

            if (tag!=null && tag.equals("investigator")) {

                myContentsView = getLayoutInflater().inflate(R.layout.custom_info_window_layout, null);

                TextView tvName = ((TextView) myContentsView.findViewById(R.id.textViewInfoWindowName));
                TextView tvContact = ((TextView) myContentsView.findViewById(R.id.textViewInfoWindowContact));
                TextView tvZone = ((TextView) myContentsView.findViewById(R.id.textViewInfoWindowZone));


                for (int i = 0; i < geoXmanList.size(); i++) {

                    //  String id = deviceList.get(i).getMarkerId();
                    Marker m = geoXmanList.get(i).getMarker();

                    if (m!=null && m.equals(marker)) {

                        tvName.setText(geoXmanList.get(i).getName());
                        tvContact.setText(geoXmanList.get(i).getContact());
                        tvZone.setText(geoXmanList.get(i).getZoneName());

                        break;
                    }
                }

                return myContentsView;

            }else if (tag!=null && tag.equals("victim")){

                myContentsView = getLayoutInflater().inflate(R.layout.custom_info_window_victim_layout, null);

                TextView tvName = ((TextView) myContentsView.findViewById(R.id.textViewInfoWindowVName));
                TextView tvContact = ((TextView) myContentsView.findViewById(R.id.textViewInfoWindowVLocDate));

                for (int i = 0; i < victimList.size(); i++) {

                    //  String id = deviceList.get(i).getMarkerId();

                    Marker m = victimList.get(i).getMarker();

                    if (m!=null && m.equals(marker)) {

                        tvName.setText(victimList.get(i).getName());
                        tvContact.setText(victimList.get(i).getDate());

                        break;
                    }
                }

                return myContentsView;
            }else
            {

                return  null;
            }


        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }



    private class GetUnsafeZonesDataTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(VictimsMapActivity.this);
            progressDialog.setMessage("Getting unsafe zone data...");
          //  progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            ParseQuery query = ParseQuery.getQuery("UnsafeZones");
            query.whereEqualTo("state",stateSelected);
            query.setLimit(1000);

            try {

                List<ParseObject> parseObjectList =  query.find();

                Log.d("AllVictims","No of UnsafeZone objects = "+parseObjectList.size());

                for (ParseObject parseObject : parseObjectList){

                    String name = parseObject.getString("name");
                    String zoneName = parseObject.getString("zoneName");
                    String address = parseObject.getString("address");
                    ParseGeoPoint location = parseObject.getParseGeoPoint("location");
                    int radious = (int) parseObject.getNumber("radious");
                    int zoneId = parseObject.getInt("zoneID");
                    String state = parseObject.getString("State");

                    UnsafeZones unsafeZones = new UnsafeZones();

                    unsafeZones.setName(name);
                    unsafeZones.setZoneName(zoneName);
                    unsafeZones.setZoneID(zoneId);
                    unsafeZones.setState(state);
                    unsafeZones.setAddress(address);
                    unsafeZones.setLocation(location);
                    unsafeZones.setRadious(radious);

                    unsafeZonesList.add(unsafeZones);

                }

            } catch (ParseException e) {

                Log.d("AllVictims","failed to find UnsafeZones objects");
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result) {

          //  progressDialog.dismiss();

            Log.d("AllVictims", "geoXmanList.size() = "+unsafeZonesList.size());

            if (unsafeZonesList.size()>0){

                for (int i = 0; i<unsafeZonesList.size(); i++) {

                    if (map != null) {

                        Circle circle = map.addCircle(new CircleOptions()
                                .center(new LatLng(unsafeZonesList.get(i).getLocation().getLatitude(), unsafeZonesList.get(i).getLocation().getLongitude()))
                                .radius(unsafeZonesList.get(i).getRadious())
                                .strokeColor(Color.TRANSPARENT).strokeWidth(1)
                                .fillColor(0x40ff0000));

                        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.geofence_marker_layout, null);
                        TextView tvGeofence = (TextView) marker.findViewById(R.id.textViewGeofenceMarkerName);

                        tvGeofence.setText(unsafeZonesList.get(i).getName());

                        Marker target = map.addMarker(new MarkerOptions().position(new LatLng(unsafeZonesList.get(i).getLocation().getLatitude(), unsafeZonesList.get(i).getLocation().getLongitude()))
                                 .title(unsafeZonesList.get(i).getName())
                                // .snippet(unsafeZonesList.get(i).getAddress())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.unsafezone_marker))
                               // .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(VictimsMapActivity.this, marker)))
                        );




                        target.setTag("unsafezone");

                        unsafeZonesList.get(i).setMarker(target);
                        unsafeZonesList.get(i).setCircle(circle);


                    } else {

                        Toast.makeText(VictimsMapActivity.this, "Unable to load map", Toast.LENGTH_SHORT).show();
                    }
                }

                map.setInfoWindowAdapter(new MyInfoWindowAdapter());


            }else {

                Toast.makeText(VictimsMapActivity.this, "Unsafe Zone Data Not Available", Toast.LENGTH_SHORT).show();
            }

        }

    }


    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.WRAP_CONTENT, AbsListView.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    private class GetSecuredZonesDataTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(VictimsMapActivity.this);
            progressDialog.setMessage("Getting secured zone data...");
            //  progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            ParseQuery query = ParseQuery.getQuery("EZones");
            query.whereEqualTo("STATE",stateSelected);
            query.setLimit(1000);

            try {

                List<ParseObject> parseObjectList =  query.find();

                Log.d("AllVictims","No of secured zone objects = "+parseObjectList.size());

                for (ParseObject parseObject : parseObjectList){

                    String zoneName = parseObject.getString("TAHSIL");
                    int zoneId = parseObject.getInt("SECUREZONE");
                    String state = parseObject.getString("STATE");
                    double lat = (double) parseObject.getNumber("LATITUDE");
                    double lng = (double) parseObject.getNumber("LONGITUDE");

                    SecuredZone securedZone = new SecuredZone();

                    securedZone.setName(zoneName);
                    securedZone.setState(state);
                    securedZone.setId(zoneId);
                    securedZone.setLatLng(new LatLng(lat,lng));

                    securedZoneList.add(securedZone);

                }

            } catch (ParseException e) {

                Log.d("AllVictims","failed to find Secured zones objects");
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result) {

            //  progressDialog.dismiss();

            Log.d("AllVictims", "securedZoneList.size() = "+securedZoneList.size());

            if (securedZoneList.size()>0){

                for (int i = 0; i<securedZoneList.size(); i++) {

                    if (map != null) {

                        /**

                        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.geofence_marker_layout, null);
                        TextView tvGeofence = (TextView) marker.findViewById(R.id.textViewGeofenceMarkerName);

                        tvGeofence.setText(securedZoneList.get(i).getName());

                        Marker target = map.addMarker(new MarkerOptions().position(securedZoneList.get(i).getLatLng())
                                 .title(securedZoneList.get(i).getName())
                              //  .snippet(securedZoneList.get(i).getAddress())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.safezone_marker))
                        );

                        target.setTag("securedzone");

                        securedZoneList.get(i).setMarker(target);

                         **/


                    } else {

                        Toast.makeText(VictimsMapActivity.this, "Unable to load map", Toast.LENGTH_SHORT).show();
                    }
                }

                setUpClusterer();

                map.setInfoWindowAdapter(new MyInfoWindowAdapter());

            }else {

                Toast.makeText(VictimsMapActivity.this, "Secured Zone Data Not Available", Toast.LENGTH_SHORT).show();
            }

        }

    }


    public void HideUnhideInvestigators(boolean hide){

        if (hide){

            for (int i = 0; i<geoXmanList.size();i++){

                if (geoXmanList.get(i).getMarker()!=null)
                geoXmanList.get(i).getMarker().setVisible(false);
            }

        }else {

            for (int i = 0; i<geoXmanList.size();i++){

                if (geoXmanList.get(i).getMarker()!=null)
                geoXmanList.get(i).getMarker().setVisible(true);
            }
        }





    }

    public void HideUnhideSafeZones(boolean hide){

        if (hide){

            for (int i = 0; i<securedZoneList.size();i++){

                if (securedZoneList.get(i).getMarker()!=null)
                securedZoneList.get(i).getMarker().setVisible(false);
            }

        }else {

            for (int i = 0; i<securedZoneList.size();i++){

                if (securedZoneList.get(i).getMarker()!=null)
                securedZoneList.get(i).getMarker().setVisible(true);

            }
        }





    }

    public void HideUnhideUnsafeZone(boolean hide){

        if (hide){

            for (int i = 0; i<unsafeZonesList.size();i++){

                if (unsafeZonesList.get(i).getMarker()!=null) {
                    unsafeZonesList.get(i).getMarker().setVisible(false);
                    unsafeZonesList.get(i).getCircle().setVisible(false);
                }
            }

        }else {

            for (int i = 0; i<unsafeZonesList.size();i++){

                if (unsafeZonesList.get(i).getMarker()!=null) {
                    unsafeZonesList.get(i).getMarker().setVisible(true);
                    unsafeZonesList.get(i).getCircle().setVisible(true);
                }
            }
        }
    }

    public void HideUnhideLandmarks(boolean hide){

        if (hide){

            for (int i = 0; i<landmarksList.size();i++){

                if (landmarksList.get(i).getMarker()!=null)
                landmarksList.get(i).getMarker().setVisible(false);
            }

        }else {

            for (int i = 0; i<landmarksList.size();i++){

                if (landmarksList.get(i).getMarker()!=null)
                landmarksList.get(i).getMarker().setVisible(true);
            }
        }





    }

    public void HideUnhideHospitals(boolean hide){

        if (hide){

            for (int i = 0; i<hospitalsList.size();i++){

                if (hospitalsList.get(i).getMarker()!=null)
                hospitalsList.get(i).getMarker().setVisible(false);
            }

        }else {

            for (int i = 0; i<hospitalsList.size();i++){

                if (hospitalsList.get(i).getMarker()!=null)
                hospitalsList.get(i).getMarker().setVisible(true);
            }
        }





    }

    public void HideUnhidePoliceStations(boolean hide){

        if (hide){

            for (int i = 0; i<policeStationsList.size();i++){

                if (policeStationsList.get(i).getMarker()!=null)
                policeStationsList.get(i).getMarker().setVisible(false);
            }

        }else {

            for (int i = 0; i<policeStationsList.size();i++){

                if (policeStationsList.get(i).getMarker()!=null)
                policeStationsList.get(i).getMarker().setVisible(true);
            }
        }



    }


    public void setUpEvidenceBottomView(String name,String invest_insta_id){

        gvImages = (GridView)findViewById(R.id.gridView);
        lvAudio = (ListView)findViewById(R.id.listViewAudio);
        lvVideos = (ListView)findViewById(R.id.listViewVideos);
        tvImage = (TextView)findViewById(R.id.textViewImages);
        tvVideo = (TextView)findViewById(R.id.textViewVideoss);
        tvAudio = (TextView)findViewById(R.id.textViewAudio);
        textViewDataNoAva = (TextView)findViewById(R.id.textViewDataNotAva);
        tvInvestName = (TextView)findViewById(R.id.textViewEvidenceInvestigatorName);

        textViewDataNoAva.setVisibility(View.GONE);



        imageList.clear();
        videoList.clear();
        audioList.clear();

       getVictimsEvidenceData =  new GetVictimsEvidenceData(name,invest_insta_id,VictimsMapActivity.this);
        getVictimsEvidenceData.execute();


        gvImages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final Dialog nagDialog = new Dialog(VictimsMapActivity.this,android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                nagDialog.setCancelable(false);
                nagDialog.setContentView(R.layout.preview_image);
                Button btnClose = (Button)nagDialog.findViewById(R.id.btnIvClose);
                ImageView ivPreview = (ImageView)nagDialog.findViewById(R.id.iv_preview_image);

                ImageLoader imageLoader = ImageLoader.getInstance();
                DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                        .cacheOnDisc(true).resetViewBeforeLoading(true)
                        .build();

                imageLoader.displayImage(imageList.get(position).getUrl(), ivPreview, options);

                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {

                        nagDialog.dismiss();
                    }
                });
                nagDialog.show();

            }
        });




    }



    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }

    public String getVideoThumbnail(String videoID){

        String thumb = null;

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String responseJsonStr = null;

        try {
            // Construct the URL for the OpenWeatherMap query
            // Possible parameters are avaiable at OWM's forecast API page, at
            // http://openweathermap.org/API#forecast
            URL url = new URL("https://api.dailymotion.com/video/"+videoID+"?fields=thumbnail_medium_url,thumbnail_small_url,thumbnail_large_url");

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                thumb = null;
                return null;
            }

            responseJsonStr = buffer.toString();

            Log.d("VideoFetch", "response  =  "+responseJsonStr);


            JSONObject jsonObject = new JSONObject(responseJsonStr);

            thumb = jsonObject.getString("thumbnail_medium_url");

            Log.d("VideoFetch", "thumb =  "+thumb);


        } catch (IOException e) {
            Log.d("VideoFetch", "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attemping
            // to parse it.
            thumb = null;
            return null;

        } catch (JSONException e) {

            Log.e("VideoFetch", "exception  ", e);
            e.printStackTrace();
            thumb = null;
            return null;

        } finally{

            if (urlConnection != null) {
                urlConnection.disconnect();
            }

            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }
        }

        return thumb;

    }

    private class GetVictimsEvidenceData extends AsyncTask<Void,Void,Void> {

        ProgressBar progressBar;

        String investigator_name;

        String name;
        Context context;
        String invest_insta_id;

        public GetVictimsEvidenceData(String name,String invest_insta_id, Context context) {
            this.name = name;
            this.context = context;
            this.invest_insta_id = invest_insta_id;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

             progressBar = (ProgressBar)findViewById(R.id.progressBarVictimEvidence);

            progressBar.setVisibility(View.VISIBLE);

            textViewDataNoAva.setVisibility(View.GONE);


/**

            mProgressDialog = new ProgressDialog(VictimsMapActivity.this);
            mProgressDialog.setMessage("Loading.....");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();

 **/

        }


        @Override
        protected Void doInBackground(Void... params) {


            Calendar calendar = Calendar.getInstance();

            calendar.add(Calendar.HOUR,-24);

            Date d = calendar.getTime();


            ParseQuery imageQuery = ParseQuery.getQuery("EmergencyImage");
            imageQuery.whereEqualTo("name",name);
          //  imageQuery.whereGreaterThan("updatedAt",d);

            imageQuery.addDescendingOrder("createdAt");
            imageQuery.setLimit(1000);

            try {

                List<ParseObject> imageObjects = imageQuery.find();
                for (ParseObject o: imageObjects){

                    Image image = new Image();

                    String url = o.getString("photo_medium_url");
                    String thumbUrl = o.getString("photo_thumb_url");
                    Log.d("Evidence","thumb url = "+thumbUrl);

                    Log.d("Evidence","photo url = "+url);

                    String date = o.getString("date");
                    ParseGeoPoint location = o.getParseGeoPoint("location");

                    image.setUrl(url);
                    image.setThumbUrl(thumbUrl);

                    image.setDate(date);
                    image.setLocation(location);

                    imageList.add(image);

                }

            } catch (ParseException e) {
                e.printStackTrace();
            }


            ParseQuery videoQuery = ParseQuery.getQuery("EmergencyVideo");
            videoQuery.whereEqualTo("name",name);
          //  videoQuery.whereGreaterThan("updatedAt",d);

            videoQuery.addDescendingOrder("createdAt");
            videoQuery.setLimit(1000);

            try {

                List<ParseObject> videoObjects = videoQuery.find();

                for (ParseObject o:videoObjects){

                    Video video = new Video();

                    String id = o.getString("VideoID");
                    String date = o.getString("date");
                    ParseGeoPoint location = o.getParseGeoPoint("location");

                    video.setLocation(location);
                    video.setDate(date);
                    video.setId(id);

                    String thumb = getVideoThumbnail(id);

                    video.setThumb(thumb);


                    videoList.add(video);

                }


            } catch (ParseException e) {
                e.printStackTrace();
            }


            ParseQuery audioQuery = ParseQuery.getQuery("EmergencyAudio");
         //   audioQuery.whereGreaterThan("updatedAt",d);
            audioQuery.whereEqualTo("name",name);

            audioQuery.addDescendingOrder("createdAt");
            audioQuery.setLimit(1000);
            try {

                List<ParseObject> audioObjects = audioQuery.find();
                for (ParseObject o: audioObjects){

                    Audio audio = new Audio();

                    String url = o.getParseFile("AudioFile").getUrl();

                    String date = o.getString("date");

                    audio.setUrl(url);
                    audio.setDate(date);

                    audioList.add(audio);

                }

            } catch (ParseException e) {
                e.printStackTrace();
            }


            ParseQuery investigatorQuery = ParseQuery.getQuery("GeoXman");
            investigatorQuery.whereEqualTo("Installation_id",invest_insta_id);
            Log.d("Evidence","investigator  query  invest_insta_id"+invest_insta_id);


            try {

               ParseObject parseObject = investigatorQuery.getFirst();

                investigator_name = parseObject.getString("XManName");

                Log.d("Evidence","investigator  found name "+investigator_name);



            } catch (ParseException e) {

                e.printStackTrace();

                Log.d("Evidence","investigator not found exception = "+e);

                investigator_name = "NA";

            }


            return null;
        }

        @Override
        protected void onPostExecute(Void result) {


            progressBar.setVisibility(View.GONE);

            if (investigator_name!=null){

                tvInvestName.setText(investigator_name);

            }else {
                Log.d("Evidence","investigator_name is  = null");

            }

            if (imageList.size()==0 && videoList.size()==0 && audioList.size()==0){

                textViewDataNoAva.setVisibility(View.VISIBLE);

            }

            Log.d("Evidence","imageList.size = "+imageList.size());


            if (imageList.size()>=1){

                tvImage.setVisibility(View.VISIBLE);
                gvImages.setVisibility(View.VISIBLE);

                EvidenceImageGridViewAdapter imageGridViewAdapter = new EvidenceImageGridViewAdapter(context,imageList);
                gvImages.setAdapter(imageGridViewAdapter);

                gvImages.getViewTreeObserver().addOnGlobalLayoutListener( new ViewTreeObserver.OnGlobalLayoutListener()
                {
                    @Override
                    public void onGlobalLayout()
                    {
                        try {

                            gvImages.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            View lastChild = gvImages.getChildAt(gvImages.getChildCount() - 1);
                            gvImages.setLayoutParams(new LinearLayout.LayoutParams(GridLayout.LayoutParams.FILL_PARENT, lastChild.getBottom()));

                        }catch (Exception e){

                            e.printStackTrace();
                            Log.d("VictimsMap","Exception e = "+e);
                        }
                    }
                });



            }else{

                tvImage.setVisibility(View.GONE);
                gvImages.setVisibility(View.GONE);


                //  lvImages.setVisibility(View.GONE);

            }

            Log.d("Evidence","videoList.size = "+videoList.size());

            if (videoList.size()>=1){

                tvVideo.setVisibility(View.VISIBLE);
                lvVideos.setVisibility(View.VISIBLE);
                EvidenceVideoListAdapter adapter = new EvidenceVideoListAdapter(context,videoList);
                lvVideos.setAdapter(adapter);

            }else{
                tvVideo.setVisibility(View.GONE);
                lvVideos.setVisibility(View.GONE);

            }

            Log.d("Evidence","audioList.size = "+audioList.size());

            if (audioList.size()>=1){

                tvAudio.setVisibility(View.VISIBLE);
                lvAudio.setVisibility(View.VISIBLE);

                EvidenceAudioListAdapter audioListAdapter = new EvidenceAudioListAdapter(context,audioList);
                lvAudio.setAdapter(audioListAdapter);

            }
            else{
                tvAudio.setVisibility(View.GONE);
                lvAudio.setVisibility(View.GONE);

            }



            //  ListUtils.setDynamicHeight(lvImages);
            ListUtils.setDynamicHeight(lvVideos);
            ListUtils.setDynamicHeight(lvAudio);

        }

    }

    private class GetLandMarksDataTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(VictimsMapActivity.this);
            progressDialog.setMessage("Getting Landmarks data...");
            //  progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            ParseQuery query = ParseQuery.getQuery("Landmarks");
            query.whereEqualTo("state",stateSelected);
            query.setLimit(1000);

            try {

                List<ParseObject> parseObjectList =  query.find();

                Log.d("AllVictims","No of landmarks objects = "+parseObjectList.size());

                for (ParseObject parseObject : parseObjectList){

                    String name = parseObject.getString("name");

                    JSONArray parseGeoPoint = parseObject.getJSONArray("latlong");

                    double lat = parseGeoPoint.getDouble(1);
                    double lng = parseGeoPoint.getDouble(0);

                    Landmarks landmarks = new Landmarks();

                    landmarks.setName(name);
                    landmarks.setLatLng(new LatLng(lat,lng));

                    landmarksList.add(landmarks);

                }

            } catch (ParseException e) {

                Log.d("AllVictims","failed to find Secured zones objects");
                e.printStackTrace();
            } catch (JSONException e) {

                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result) {

            //  progressDialog.dismiss();

            Log.d("AllVictims", "landmarksList.size() = "+landmarksList.size());

            if (landmarksList.size()>0){

                for (int i = 0; i<landmarksList.size(); i++) {

                    if (map != null) {

                        /**

                        Marker target = map.addMarker(new MarkerOptions().position(landmarksList.get(i).getLatLng())
                                .title(landmarksList.get(i).getName())
                                //  .snippet(securedZoneList.get(i).getAddress())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.landmark_marker))
                        );

                        target.setTag("landmark");

                        landmarksList.get(i).setMarker(target);


                         **/

                    } else {

                        Toast.makeText(VictimsMapActivity.this, "Unable to load map", Toast.LENGTH_SHORT).show();
                    }
                }

                setUpLandmarksClusterer();

                map.setInfoWindowAdapter(new MyInfoWindowAdapter());


            }else {

                Toast.makeText(VictimsMapActivity.this, "Landmarks Data Not Available", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private class GetHospitalsDataTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(VictimsMapActivity.this);
            progressDialog.setMessage("Getting Hospitals data...");
            //  progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            ParseQuery query = ParseQuery.getQuery("Hospitals");
            query.whereEqualTo("state",stateSelected);
            query.setLimit(1000);

            try {

                List<ParseObject> parseObjectList =  query.find();

                Log.d("AllVictims","No of Hospitals objects = "+parseObjectList.size());

                for (ParseObject parseObject : parseObjectList){

                    String name = parseObject.getString("Name");

                  //  ParseGeoPoint parseGeoPoint = parseObject.getParseGeoPoint("geospot");

                    JSONArray parseGeoPoint = parseObject.getJSONArray("geospot");

                    double lat = parseGeoPoint.getDouble(1);
                    double lng = parseGeoPoint.getDouble(0);

                    Hospitals hospitals = new Hospitals();

                    hospitals.setName(name);
                    hospitals.setLatLng(new LatLng(lat,lng));

                    hospitalsList.add(hospitals);

                }

            } catch (ParseException e) {

                Log.d("AllVictims","failed to find Hospitals objects");
                e.printStackTrace();

            } catch (JSONException e) {

                Log.d("AllVictims","JSON exception "+e);

                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result) {

            //  progressDialog.dismiss();

            Log.d("AllVictims", "hospitalsList.size() = "+hospitalsList.size());

            if (hospitalsList.size()>0){

                for (int i = 0; i<hospitalsList.size(); i++) {

                    if (map != null) {

                        /**

                        Marker target = map.addMarker(new MarkerOptions().position(hospitalsList.get(i).getLatLng())
                                .title(hospitalsList.get(i).getName())
                                //  .snippet(securedZoneList.get(i).getAddress())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_marker))
                        );

                        target.setTag("hospital");

                        hospitalsList.get(i).setMarker(target);

                         **/


                    } else {

                        Toast.makeText(VictimsMapActivity.this, "Unable to load map", Toast.LENGTH_SHORT).show();
                    }
                }

                setUpHospitalsClusterer();

                map.setInfoWindowAdapter(new MyInfoWindowAdapter());


            }else {

                Toast.makeText(VictimsMapActivity.this, " Hospitals Data Not Available", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private class GetPoliceStationsDataTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(VictimsMapActivity.this);
            progressDialog.setMessage("Getting Police Stations data...");
            //  progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            ParseQuery query = ParseQuery.getQuery("PoliceStations");
            query.whereEqualTo("state",stateSelected);
            query.setLimit(1000);

            try {

                List<ParseObject> parseObjectList =  query.find();

                Log.d("AllVictims","No of PoliceStations objects = "+parseObjectList.size());

                for (ParseObject parseObject : parseObjectList){

                    String name = parseObject.getString("name");
                    String address = parseObject.getString("address");
                    String contact = parseObject.getString("contact");

                      ParseGeoPoint parseGeoPoint = parseObject.getParseGeoPoint("location");

                    double lat = parseGeoPoint.getLatitude();
                    double lng = parseGeoPoint.getLongitude();

                    PoliceStations policeStations = new PoliceStations();

                    policeStations.setName(name);
                    policeStations.setAddress(address);
                    policeStations.setContact(contact);
                    policeStations.setLatLng(new LatLng(lat,lng));

                    policeStationsList.add(policeStations);

                }

            } catch (ParseException e) {

                Log.d("AllVictims", "failed to find PoliceStations objects");
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result) {

            //  progressDialog.dismiss();

            Log.d("AllVictims", "policeStationsList.size() = "+policeStationsList.size());

            if (policeStationsList.size()>0){

                for (int i = 0; i<policeStationsList.size(); i++) {

                    if (map != null) {

                        /**

                        Marker target = map.addMarker(new MarkerOptions().position(policeStationsList.get(i).getLatLng())
                                .title(policeStationsList.get(i).getName())
                                //  .snippet(securedZoneList.get(i).getAddress())
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.police_station_marker))
                        );

                        target.setTag("policestation");

                        policeStationsList.get(i).setMarker(target);

                         **/


                    } else {

                        Toast.makeText(VictimsMapActivity.this, "Unable to load map", Toast.LENGTH_SHORT).show();
                    }
                }

                setUpPoliceStationsClusterer();

                map.setInfoWindowAdapter(new MyInfoWindowAdapter());


            }else {

                Toast.makeText(VictimsMapActivity.this, " Police stations Data Not Available", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private class GetCCTVDataTask extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(VictimsMapActivity.this);
            progressDialog.setMessage("Getting cctv data...");
            //  progressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            ParseQuery query = ParseQuery.getQuery("CCTV");
          //  query.whereEqualTo("state",stateSelected);
            query.setLimit(1000);

            try {

                List<ParseObject> parseObjectList =  query.find();

                Log.d("AllVictims","No of CCTV objects = "+parseObjectList.size());

                for (ParseObject parseObject : parseObjectList){

                    String url = parseObject.getString("url");
                    ParseGeoPoint parseGeoPoint = parseObject.getParseGeoPoint("location");

                    double lat = parseGeoPoint.getLatitude();
                    double lng = parseGeoPoint.getLongitude();

                    CCTV cctv = new CCTV();

                    cctv.setUrl(url);
                    cctv.setLatLng(new LatLng(lat,lng));

                    cctvList.add(cctv);

                }

            } catch (ParseException e) {

                Log.d("AllVictims", "failed to find CCTV objects");
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result) {

            //  progressDialog.dismiss();

            Log.d("AllVictims", "CCTV.size() = "+cctvList.size());

            if (cctvList.size()>0){

                for (int i = 0; i<cctvList.size(); i++) {

                    if (map != null) {

                        /**

                         Marker target = map.addMarker(new MarkerOptions().position(policeStationsList.get(i).getLatLng())
                         .title(policeStationsList.get(i).getName())
                         //  .snippet(securedZoneList.get(i).getAddress())
                         .icon(BitmapDescriptorFactory.fromResource(R.drawable.police_station_marker))
                         );

                         target.setTag("policestation");

                         policeStationsList.get(i).setMarker(target);

                         **/


                    } else {

                        Toast.makeText(VictimsMapActivity.this, "Unable to load map", Toast.LENGTH_SHORT).show();
                    }
                }

                setUpCCTVClusterer();

                map.setInfoWindowAdapter(new MyInfoWindowAdapter());


            }else {

                Toast.makeText(VictimsMapActivity.this, " CCTV Data Not Available", Toast.LENGTH_SHORT).show();
            }

        }

    }


    private void setUpClusterer() {
        // Position the map

     //  map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
      //  map.setOnCameraIdleListener(mClusterManager);
        map.setOnMarkerClickListener(onMarkerClickListener);

        // Add cluster items (markers) to the cluster manager.

        mClusterManager.setRenderer(new SecuredZoneRendered(VictimsMapActivity.this,map,mClusterManager));

       mClusterManager.addItems(securedZoneList);

    }

    private void setUpLandmarksClusterer() {
        // Position the map

        //  map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
       // map.setOnCameraIdleListener(mClusterManagerLandmarks);
        map.setOnMarkerClickListener(onMarkerClickListener);

        // Add cluster items (markers) to the cluster manager.

        mClusterManagerLandmarks.setRenderer(new LandmarksRendered(VictimsMapActivity.this,map,mClusterManagerLandmarks));

        mClusterManagerLandmarks.addItems(landmarksList);

    }

    private void setUpHospitalsClusterer() {
        // Position the map

        //  map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        // map.setOnCameraIdleListener(mClusterManagerLandmarks);
        map.setOnMarkerClickListener(onMarkerClickListener);

        // Add cluster items (markers) to the cluster manager.

        mClusterManagerHospitals.setRenderer(new HospitalsRendered(VictimsMapActivity.this,map,mClusterManagerHospitals));

        mClusterManagerHospitals.addItems(hospitalsList);

    }

    private void setUpPoliceStationsClusterer() {
        // Position the map

        //  map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        // map.setOnCameraIdleListener(mClusterManagerLandmarks);
        map.setOnMarkerClickListener(onMarkerClickListener);

        // Add cluster items (markers) to the cluster manager.

        mClusterManagerPoliceStations.setRenderer(new PoliceStationsRendered(VictimsMapActivity.this,map,mClusterManagerPoliceStations));

        mClusterManagerPoliceStations.addItems(policeStationsList);

    }

    private void setUpCCTVClusterer() {
        // Position the map

        //  map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        // map.setOnCameraIdleListener(mClusterManagerLandmarks);
        map.setOnMarkerClickListener(onMarkerClickListener);

        // Add cluster items (markers) to the cluster manager.

        mClusterManagerCCTV.setRenderer(new CCTVRendered(VictimsMapActivity.this,map,mClusterManagerCCTV));

        mClusterManagerCCTV.addItems(cctvList);

    }


    private void setUpInvestigatorClusterer() {
        // Position the map

        //  map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        // map.setOnCameraIdleListener(mClusterManagerLandmarks);
        map.setOnMarkerClickListener(onMarkerClickListener);

        // Add cluster items (markers) to the cluster manager.

        mClusterManagerInvestigators.setRenderer(new InvestigatorsRendered(VictimsMapActivity.this,map,mClusterManagerInvestigators));

        mClusterManagerInvestigators.addItems(geoXmanList);

    }


    GoogleMap.OnMarkerClickListener onMarkerClickListener = new GoogleMap.OnMarkerClickListener() {
        @Override
        public boolean onMarkerClick(Marker marker) {

            marker.showInfoWindow();

            Log.d("VictimMap", "Selected marker id = " + marker.getId()+" Tag = "+marker.getTag());

            if (marker.getTag()!=null) {

                for (int i = 0; i < victimList.size(); i++) {

                    Marker id = victimList.get(i).getMarker();

                    if (id.equals(marker)) {

                        victimNameSelected = victimList.get(i).getName();

                    }

                }

                if (marker.getTag().equals("cctv")){

                    if (marker.getTag()!=null) {

                        for (int i = 0; i < cctvList.size(); i++) {

                            Marker id = cctvList.get(i).getMarker();

                            if (id.equals(marker)) {

                               // Toast.makeText(VictimsMapActivity.this, "Clicked on CCTV", Toast.LENGTH_SHORT).show();

                                displayCCTV(cctvList.get(i));

                                break;
                            }

                        }
                    }
                }

            }else {


                map.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), (float) Math.floor(map
                                .getCameraPosition().zoom + 1)), 300,
                        null);

            }

            return true;
        }
    };



    private class SecuredZoneRendered extends DefaultClusterRenderer<SecuredZone>{

        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());

        public SecuredZoneRendered(Context context, GoogleMap map, ClusterManager<SecuredZone> clusterManager) {
            super(context, map, clusterManager);

        }

        @Override
        protected void onClusterItemRendered(SecuredZone clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);

            for (int i=0;i<securedZoneList.size();i++){

                if (securedZoneList.get(i).equals(clusterItem)){

                    marker.setTag("securedzone");

                    securedZoneList.get(i).setMarker(marker);

                    Log.d("VictimsMap","SecuredZoneRendered   onClusterItemRendered()  Marker Set on  "+clusterItem.getName()+" - "+clusterItem.getDistrict());

                    break;
                }

            }

        }


        @Override
        protected void onBeforeClusterItemRendered(SecuredZone person, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.

            Log.d("VictimsMap","SecuredZoneRendered   onBeforeClusterItemRendered() ");

          //  mImageView.setImageResource(person.profilePhoto);

          //  Bitmap icon = mIconGenerator.makeIcon();

            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.safezone_marker)).title(person.getName());
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<SecuredZone> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).

            Log.d("VictimsMap","SecuredZoneRendered   onBeforeClusterRendered() ");

            mClusterIconGenerator.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.cluster_icon));
            mClusterIconGenerator.setTextAppearance(R.style.AppTheme_WhiteTextAppearance);
            final Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));

        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }

    }

    private class LandmarksRendered extends DefaultClusterRenderer<Landmarks>{

        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());

        public LandmarksRendered(Context context, GoogleMap map, ClusterManager<Landmarks> clusterManager) {
            super(context, map, clusterManager);

        }

        @Override
        protected void onClusterItemRendered(Landmarks clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);

            for (int i=0;i<landmarksList.size();i++){

                if (landmarksList.get(i).equals(clusterItem)){

                    marker.setTag("landmark");

                    landmarksList.get(i).setMarker(marker);

                    Log.d("VictimsMap","LandmarksRendered   onClusterItemRendered()  Marker Set on  "+clusterItem.getName());

                    break;
                }

            }

        }


        @Override
        protected void onBeforeClusterItemRendered(Landmarks person, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.

            Log.d("VictimsMap","LandmarksRendered   onBeforeClusterItemRendered() ");

            //  mImageView.setImageResource(person.profilePhoto);

            //  Bitmap icon = mIconGenerator.makeIcon();

            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.landmark_marker)).title(person.getName());
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Landmarks> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).

            Log.d("VictimsMap","LandmarksRendered   onBeforeClusterRendered() ");

            mClusterIconGenerator.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.landmark_cluster));
            mClusterIconGenerator.setTextAppearance(R.style.AppTheme_TransparentTextAppearance);
            final Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));

        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }

    }

    private class HospitalsRendered extends DefaultClusterRenderer<Hospitals>{

        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());

        public HospitalsRendered(Context context, GoogleMap map, ClusterManager<Hospitals> clusterManager) {
            super(context, map, clusterManager);

        }

        @Override
        protected void onClusterItemRendered(Hospitals clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);

            for (int i=0;i<hospitalsList.size();i++){

                if (hospitalsList.get(i).equals(clusterItem)){

                    marker.setTag("landmark");

                    hospitalsList.get(i).setMarker(marker);

                    Log.d("VictimsMap","LandmarksRendered   onClusterItemRendered()  Marker Set on  "+clusterItem.getName());

                    break;
                }

            }

        }


        @Override
        protected void onBeforeClusterItemRendered(Hospitals person, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.

            Log.d("VictimsMap","LandmarksRendered   onBeforeClusterItemRendered() ");

            //  mImageView.setImageResource(person.profilePhoto);

            //  Bitmap icon = mIconGenerator.makeIcon();

            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_marker)).title(person.getName());
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Hospitals> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).

            Log.d("VictimsMap","LandmarksRendered   onBeforeClusterRendered() ");

            mClusterIconGenerator.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.hospital_cluster));
            mClusterIconGenerator.setTextAppearance(R.style.AppTheme_TransparentTextAppearance);
            final Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));

        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }

    }

    private class PoliceStationsRendered extends DefaultClusterRenderer<PoliceStations>{

        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());

        public PoliceStationsRendered(Context context, GoogleMap map, ClusterManager<PoliceStations> clusterManager) {
            super(context, map, clusterManager);

        }

        @Override
        protected void onClusterItemRendered(PoliceStations clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);

            for (int i=0;i<policeStationsList.size();i++){

                if (policeStationsList.get(i).equals(clusterItem)){

                    marker.setTag("landmark");

                    policeStationsList.get(i).setMarker(marker);

                    Log.d("VictimsMap","LandmarksRendered   onClusterItemRendered()  Marker Set on  "+clusterItem.getName());

                    break;
                }

            }

        }


        @Override
        protected void onBeforeClusterItemRendered(PoliceStations person, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.

            Log.d("VictimsMap","LandmarksRendered   onBeforeClusterItemRendered() ");

            //  mImageView.setImageResource(person.profilePhoto);

            //  Bitmap icon = mIconGenerator.makeIcon();

            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.police_station_marker)).title(person.getName());

        }

        @Override
        protected void onBeforeClusterRendered(Cluster<PoliceStations> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).

            Log.d("VictimsMap","LandmarksRendered   onBeforeClusterRendered() ");

            mClusterIconGenerator.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.police_station_cluster));
            mClusterIconGenerator.setTextAppearance(R.style.AppTheme_TransparentTextAppearance);
            final Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));

        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }

    }

    private class CCTVRendered extends DefaultClusterRenderer<CCTV>{

        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());

        public CCTVRendered(Context context, GoogleMap map, ClusterManager<CCTV> clusterManager) {
            super(context, map, clusterManager);

        }

        @Override
        protected void onClusterItemRendered(CCTV clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);

            for (int i=0;i<cctvList.size();i++){

                if (cctvList.get(i).equals(clusterItem)){

                    marker.setTag("cctv");

                    cctvList.get(i).setMarker(marker);

                    Log.d("VictimsMap","CCTVRendered   onClusterItemRendered()  Marker Set on  "+clusterItem.getUrl());

                    break;
                }

            }

        }


        @Override
        protected void onBeforeClusterItemRendered(CCTV person, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.

            Log.d("VictimsMap","CCTVRendered   onBeforeClusterItemRendered() ");

            //  mImageView.setImageResource(person.profilePhoto);

            //  Bitmap icon = mIconGenerator.makeIcon();

            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.cctv_marker));

        }

        @Override
        protected void onBeforeClusterRendered(Cluster<CCTV> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).

            Log.d("VictimsMap","CCTVRendered   onBeforeClusterRendered() ");

            mClusterIconGenerator.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.cctv_marker));
            mClusterIconGenerator.setTextAppearance(R.style.AppTheme_TransparentTextAppearance);
            final Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));

        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }

    }


    private class InvestigatorsRendered extends DefaultClusterRenderer<GeoXman>{

        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());

        public InvestigatorsRendered(Context context, GoogleMap map, ClusterManager<GeoXman> clusterManager) {
            super(context, map, clusterManager);

        }

        @Override
        protected void onClusterItemRendered(GeoXman clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);

            for (int i=0;i<geoXmanList.size();i++){

                if (geoXmanList.get(i).equals(clusterItem)){

                    marker.setTag("investigator");

                    geoXmanList.get(i).setMarker(marker);

                    Log.d("VictimsMap","InvestigatorsRendered   onClusterItemRendered()  Marker Set on  "+clusterItem.getName());

                    break;
                }

            }

        }


        @Override
        protected void onBeforeClusterItemRendered(GeoXman person, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.

            Log.d("VictimsMap","InvestigatorsRendered   onBeforeClusterItemRendered() ");

            //  mImageView.setImageResource(person.profilePhoto);

            //  Bitmap icon = mIconGenerator.makeIcon();

            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.investigator_marker)).title(person.getName());

        }

        @Override
        protected void onBeforeClusterRendered(Cluster<GeoXman> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).

            Log.d("VictimsMap","InvestigatorsRendered   onBeforeClusterRendered() ");

            mClusterIconGenerator.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.investigator_clutser));
            mClusterIconGenerator.setTextAppearance(R.style.AppTheme_TransparentTextAppearance);
            final Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));

        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }

    }

    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }

    public void displayCCTV(final CCTV cctv){

        LayoutInflater inflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        final RelativeLayout layout = (RelativeLayout) inflator.inflate(R.layout.cctv_surface_view_layout,null);

        final RelativeLayout mainLayout = (RelativeLayout)findViewById(R.id.content_victims_map_container);

       // RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(170, 170);

      //  params.leftMargin = 100;
      //  params.topMargin = 100;

        mainLayout.addView(layout);

        final SurfaceView surfaceView = (SurfaceView) layout.findViewById(R.id.surfaceView);
        final Button btClose = (Button)layout.findViewById(R.id.buttonSurfaceViewClose);
        Button btFullScreen = (Button)layout.findViewById(R.id.buttonSurfaceViewFullScreen);

       final MediaPlayer mPlayer1 = new MediaPlayer();

       SurfaceHolder.Callback mCallback1 = new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {

                try {

                    mPlayer1.setDataSource(VictimsMapActivity.this, Uri.parse(cctv.getUrl()));
                    mPlayer1.setDisplay(surfaceHolder);
                    mPlayer1.prepareAsync();
                    mPlayer1.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mediaPlayer) {

                            Log.d("VictimsMap","onPrepared fired media player ");

                            mPlayer1.start();
                        }
                    });


                    mPlayer1.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                        @Override
                        public boolean onError(MediaPlayer mp, int what, int extra) {

                            Log.d("VictimsMap","onError fired media player ");

                            if (what == 100)
                            {
                              //  mPlayer1.stopPlayback();
                                mPlayer1.reset();

                                Toast.makeText(VictimsMapActivity.this, "unable to play video stream", Toast.LENGTH_SHORT).show();
                            }else{
                                mPlayer1.reset();
                                Toast.makeText(VictimsMapActivity.this, "can't play this video", Toast.LENGTH_SHORT).show();

                            }
                            return true;

                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d("VictimsMap","Exception e =  "+e);

                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {

                Log.d("VictimsMap","surfaceChanged  ");

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

                Log.d("VictimsMap","surfaceDestroyed  ");

                mPlayer1.release();
            }
        };


        // Configure the Surface View.
        surfaceView.setKeepScreenOn(true);
        // Configure the Surface Holder and register the callback.
        SurfaceHolder holder1 = surfaceView.getHolder();
        holder1.addCallback(mCallback1);
        holder1.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        btClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // destroy surface view

                mainLayout.removeView(layout);

               // surfaceView.setVisibility(View.GONE);

            }
        });

        btFullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewGroup.LayoutParams layoutParams = surfaceView.getLayoutParams();



               String tag = (String) v.getTag();
                Log.d("VictimsMap","tag = "+tag);

                if (tag.equals("0")){
                    // fulscreen
                    v.setTag("1");

                    layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
                    layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;

                    surfaceView.setLayoutParams(layoutParams);


                }else{

                    // small screen
                    v.setTag("0");
                    layoutParams.height = 300;
                    layoutParams.width = 300;
                    surfaceView.setLayoutParams(layoutParams);

                }

            }
        });

        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {


                int action = motionEvent.getAction();
                if (action == MotionEvent.ACTION_DOWN) {

                    downRawX = motionEvent.getRawX();
                    downRawY = motionEvent.getRawY();
                    dX = view.getX() - downRawX;
                    dY = view.getY() - downRawY;

                    return true; // Consumed

                }
                else if (action == MotionEvent.ACTION_MOVE) {

                    int viewWidth = view.getWidth();
                    int viewHeight = view.getHeight();

                    View viewParent = (View)view.getParent();
                    int parentWidth = viewParent.getWidth();
                    int parentHeight = viewParent.getHeight();

                    float newX = motionEvent.getRawX() + dX;
                    newX = Math.max(0, newX); // Don't allow the FAB past the left hand side of the parent
                    newX = Math.min(parentWidth - viewWidth, newX); // Don't allow the FAB past the right hand side of the parent

                    float newY = motionEvent.getRawY() + dY;
                    newY = Math.max(0, newY); // Don't allow the FAB past the top of the parent
                    newY = Math.min(parentHeight - viewHeight, newY); // Don't allow the FAB past the bottom of the parent

                    view.animate()
                            .x(newX)
                            .y(newY)
                            .setDuration(0)
                            .start();

                    return true; // Consumed

                }
                else if (action == MotionEvent.ACTION_UP) {

                    float upRawX = motionEvent.getRawX();
                    float upRawY = motionEvent.getRawY();

                    float upDX = upRawX - downRawX;
                    float upDY = upRawY - downRawY;

                    if (Math.abs(upDX) < CLICK_DRAG_TOLERANCE && Math.abs(upDY) < CLICK_DRAG_TOLERANCE) { // A click
                        //  return performClick();
                        return true;
                    }
                    else { // A drag
                        return true; // Consumed
                    }

                }
                else {
                    return true;
                }
            }
        });


    }

}
