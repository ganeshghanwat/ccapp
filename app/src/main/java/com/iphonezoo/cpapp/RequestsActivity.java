package com.iphonezoo.cpapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class RequestsActivity extends AppCompatActivity {

    ListView lvRequests;

    List<IRequests> iRequestsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests);

        lvRequests = (ListView)findViewById(R.id.listViewRequests);

        ParseQuery parseQuery = ParseQuery.getQuery("IRequests");
        parseQuery.fromLocalDatastore();
        try {

           List<ParseObject> parseObjectList = parseQuery.find();

            for (ParseObject object : parseObjectList){

                IRequests i = new IRequests();

                String contact = object.getString("Contact");
                String insta_id = object.getString("Installation_id");
                boolean isVerified = object.getBoolean("isVerified");

                i.setContact(contact);
                i.setIsVerified(isVerified);
                i.setInsta_id(insta_id);

                iRequestsList.add(i);

            }

        } catch (ParseException e) {

            e.printStackTrace();
        }

        RequestListAdapter adapter = new RequestListAdapter(this,iRequestsList);
        lvRequests.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_requests, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
