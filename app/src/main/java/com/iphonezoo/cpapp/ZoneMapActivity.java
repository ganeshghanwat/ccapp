package com.iphonezoo.cpapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class ZoneMapActivity extends AppCompatActivity {

    GoogleMap map;
    MapFragment mapFragment;

    double lat;
    double lng;
    String name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone_map);

       lat =  getIntent().getDoubleExtra("lat",0.0);
       lng = getIntent().getDoubleExtra("lng",0.0);
       name = getIntent().getStringExtra("name");

        mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapDialogBox));

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                map = googleMap;

                Marker target = map.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(name));

                // Move the camera instantly to target with a zoom of 15.
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 14));

                // Zoom in, animating the camera.
                map.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_zone_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
