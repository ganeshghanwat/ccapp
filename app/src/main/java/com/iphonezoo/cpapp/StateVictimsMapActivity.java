package com.iphonezoo.cpapp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.iphonezoo.cpapp.Adapters.EvidenceAudioListAdapter;
import com.iphonezoo.cpapp.Adapters.EvidenceImageGridViewAdapter;
import com.iphonezoo.cpapp.Adapters.EvidenceVideoListAdapter;
import com.iphonezoo.cpapp.POJO.Audio;
import com.iphonezoo.cpapp.POJO.Image;
import com.iphonezoo.cpapp.POJO.Video;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class StateVictimsMapActivity extends AppCompatActivity {

    private ClusterManager<Case> mClusterManager;
    private ClusterManager<Case> mClusterManagerFailed;

    // Evidences
    ListView lvVideos;
    ListView lvAudio;
    TextView tvImage;
    TextView tvVideo;
    TextView tvAudio;
    TextView textViewDataNoAva;
    List<Image> imageList = new ArrayList<>();
    List<Video> videoList = new ArrayList<>();
    List<Audio> audioList = new ArrayList<>();
    GridView gvImages;
    TextView tvInvestName;

    GetVictimsEvidenceData getVictimsEvidenceData;

    String state;
    GoogleMap map;
    MapFragment mapFragment;

    ProgressDialog mProgressDialog;

    Button btSuccess;
    Button btFailed;

    List<Case> caseList = new ArrayList<>();
    List<Case> caseListSuccess = new ArrayList<>();
    List<Case> caseListFailed = new ArrayList<>();

    boolean showSuccessCases = true;
    boolean showFailedCases = true;

    private BottomSheetBehavior mBottomSheetBehavior1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_victims_map);

        mProgressDialog = new ProgressDialog(this);

        btSuccess = (Button)findViewById(R.id.buttonStateVictimsSuccess);
        btFailed = (Button)findViewById(R.id.buttonStateVictimsFailed);

        View bottomSheet = findViewById(R.id.bottomViewStateVictims);
        mBottomSheetBehavior1 = BottomSheetBehavior.from(bottomSheet);

        state = getIntent().getStringExtra("state");

        Log.d("CaseData", "state = " + state);

        mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapDialogBoxStateVictims));

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                map = googleMap;

                mClusterManager = new ClusterManager<Case>(StateVictimsMapActivity.this, map);
                mClusterManagerFailed = new ClusterManager<Case>(StateVictimsMapActivity.this, map);

                map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {

                        mClusterManager.onCameraIdle();
                        mClusterManagerFailed.onCameraIdle();

                    }
                });


            }
        });

        new GetVictimsData().execute();

        btFailed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (map!=null) {

                    if (showFailedCases){

                        mClusterManager.clearItems();
                        mClusterManager.cluster();

                      //  HideUnhideFailedCases(true);
                        btFailed.setBackgroundResource(R.drawable.victim_red_icon_light);
                        showFailedCases = false;

                    }else {

                        mClusterManager.addItems(caseListFailed);
                        mClusterManager.cluster();


                        //   HideUnhideFailedCases(false);
                        btFailed.setBackgroundResource(R.drawable.victim_red_icon);
                        showFailedCases = true;
                    }
                }

            }
        });


        btSuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (map!=null) {

                  //  map.clear();

                    if (showSuccessCases){

                        HideUnhideSuccessCases(true);
                        btSuccess.setBackgroundResource(R.drawable.victim_green_icon_light);
                        showSuccessCases = false;

                    }else {

                        HideUnhideSuccessCases(false);
                        btSuccess.setBackgroundResource(R.drawable.victim_green_icon);
                        showSuccessCases = true;
                    }
                }

            }
        });


        // handle bottom sheet swipe event
        mBottomSheetBehavior1.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

                if (newState == BottomSheetBehavior.STATE_EXPANDED) {

                    Log.d("BottomSheet","callback newstate = expanded ");


                }
                else if (newState == BottomSheetBehavior.STATE_COLLAPSED) {

                    Log.d("BottomSheet","callback newstate = collapsed ");

                    if (getVictimsEvidenceData!=null){

                        Log.d("BottomSheet","callback canceling  getVictimsEvidenceData task ");

                        getVictimsEvidenceData.cancel(true);

                    }

                }
                else if (newState == BottomSheetBehavior.STATE_HIDDEN) {

                    Log.d("BottomSheet","callback newstate = hidden ");

                }


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

    }

    ///Async Task

    private class GetVictimsData extends AsyncTask<Void,Void,Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

        }


        @Override
        protected Void doInBackground(Void... params) {

            ParseQuery investigatorQuery = ParseQuery.getQuery("GeoXman");
            investigatorQuery.whereEqualTo("State", state);
            investigatorQuery.setLimit(1000);


            ParseQuery query = ParseQuery.getQuery("Case");
            query.whereMatchesQuery("GeoXman", investigatorQuery);
            query.setLimit(1000);

            try {


                List<ParseObject> parseObjectList = query.find();

                Log.d("CaseData", "parseObjectList size  = " + parseObjectList.size());

                for (ParseObject parseObject : parseObjectList) {

                    Case c = new Case();

                    JSONArray location = parseObject.getJSONArray("Locations");

                    Log.d("CaseData", "location jsonArray = " + location);

                    JSONObject jsonObject = location.getJSONObject(0);

                    double latitude = jsonObject.getDouble("latitude");
                    double longitude = jsonObject.getDouble("longitude");
                    String vName = parseObject.getString("VName");
                    String sImage = parseObject.getString("SImage");
                    String vAddress = parseObject.getString("VAddress");
                    String vContact = parseObject.getString("VContact");
                    String vEmail = parseObject.getString("VEmail");
                    String vImage = parseObject.getString("VImage");
                    String vParents = parseObject.getString("VParents");
                    String vReport = parseObject.getString("VReport");
                    String crimeCategory = parseObject.getString("CrimeCategory");
                    String financialLoss = parseObject.getString("FinancialLoss");
                    String humansAffected = parseObject.getString("HumansAffected");
                    String humansKilled = parseObject.getString("HumansKilled");
                    String noOfSuspects = parseObject.getString("NoOfSuspects");
                    String evidenceAudio = parseObject.getString("evidenceAudio");
                    String evidenceImage = parseObject.getString("evidenceImage");
                    String evidenceVideo = parseObject.getString("evidenceVideo");
                    String invest_contact = parseObject.getString("invest_contact");
                    String invest_name = parseObject.getString("invest_name");
                    String invest_insta_id = parseObject.getString("invest_insta_id");
                    String status = parseObject.getString("status");

                    String imagesJSON = parseObject.getString("imagesJSON");
                    String videossJSON = parseObject.getString("videosJSON");
                    String audiosJSON = parseObject.getString("audiosJSON");

                   // JSONArray locationsJSON = parseObject.getJSONArray("locationsJSON");


                    Log.d("Case", "Latitude = " + latitude + " longitude = " + longitude);

                    c.setLocationPoint(new ParseGeoPoint(latitude, longitude));
                    c.setLocations(location);
                    c.setvName(vName);
                    c.setvImage(vImage);
                    c.setsImage(sImage);
                    c.setAudios(evidenceAudio);
                    c.setFinancialLoss(financialLoss);
                    c.setHumansAffected(humansAffected);
                    c.setHumansKilled(humansKilled);
                    c.setInvestContact(invest_contact);
                    c.setInvestInstaID(invest_insta_id);
                    c.setInvestName(invest_name);
                    c.setNoOfSuspect(noOfSuspects);
                    c.setStatus(status);
                    c.setImages(evidenceImage);
                    c.setvAddress(vAddress);
                    c.setvContact(vContact);
                    c.setvParents(vParents);
                    c.setvEmail(vEmail);
                    c.setVideos(evidenceVideo);
                    c.setvReport(vReport);
                    c.setCrimeCategory(crimeCategory);

                    c.setImagesJSON(imagesJSON);
                    c.setVideosJSON(videossJSON);
                    c.setAudiosJSON(audiosJSON);

                    c.setLocationsJSON(location.toString());

                    caseList.add(c);

                    if (c.getStatus().equalsIgnoreCase("Successful")){

                        caseListSuccess.add(c);

                    }else if (c.getStatus().equalsIgnoreCase("Failed")){

                        caseListFailed.add(c);

                    }

                }

            } catch (ParseException e) {

                e.printStackTrace();

            } catch (JSONException e) {

                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mProgressDialog.dismiss();

            if (caseList.size() >= 1) {

                /**

                for (Case c : caseList) {

                    if (c.getStatus().equalsIgnoreCase("Successful")) {

                        double lat = c.getLocationPoint().getLatitude();
                        double lng = c.getLocationPoint().getLongitude();

                        String name = c.getvName();

                        if (map != null) {

                            Marker target = map.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(name)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.victim_green_marker)));
                            // Move the camera instantly to target with a zoom of 15.

                            target.setTag("success");

                            c.setMarker(target);

                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 14));
                        }
                    } else if (c.getStatus().equalsIgnoreCase("Failed")) {

                        double lat = c.getLocationPoint().getLatitude();
                        double lng = c.getLocationPoint().getLongitude();

                        String name = c.getvName();

                        if (map != null) {

                            Marker target = map.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title(name)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.victim_red_marker)));
                            // Move the camera instantly to target with a zoom of 15.

                            target.setTag("failed");

                            c.setMarker(target);

                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 14));
                        }
                    }
**/

                setUpSuccessClusterer();
                setUpFailedClusterer();

                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {

                        marker.showInfoWindow();

                        if (marker.getTag()==null) {

                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), (float) Math.floor(map
                                            .getCameraPosition().zoom + 1)), 300,
                                    null);

                        }

                        return true;
                    }
                });



                map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {

                        if (marker.getTag() != null) {

                            String status = (String) marker.getTag();

                            if (status.equals("success")) {

                                for (int i = 0; i < caseListSuccess.size(); i++) {

                                    Marker m = caseListSuccess.get(i).getMarker();

                                    if (m!=null && m.equals(marker)) {

                                        String name = caseListSuccess.get(i).getvName();
                                        String invest_insta_id = caseListSuccess.get(i).getInvestInstaID();


                                        //  startActivity(intent);

                                        Log.d("BottomSheet", "on Infowindow click ");

                                        Log.d("Evidence", "investigator invest_insta_id" + invest_insta_id);


                                        setUpEvidenceBottomView(caseListSuccess.get(i));

                                        if (mBottomSheetBehavior1.getState() != BottomSheetBehavior.STATE_EXPANDED) {

                                            mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_EXPANDED);

                                            Log.d("BottomSheet", "state expanded");


                                        } else {

                                            mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_COLLAPSED);

                                            Log.d("BottomSheet", "state collapsed");

                                        }

                                        break;
                                    }
                                }


                            } else if (status.equals("failed")) {


                                for (int i = 0; i < caseListFailed.size(); i++) {

                                    Marker m = caseListFailed.get(i).getMarker();

                                    if (m!=null && m.equals(marker)) {

                                        String name = caseListFailed.get(i).getvName();
                                        String invest_insta_id = caseListFailed.get(i).getInvestInstaID();


                                        //  startActivity(intent);

                                        Log.d("BottomSheet", "on Infowindow click ");

                                        Log.d("Evidence", "investigator invest_insta_id" + invest_insta_id);


                                        setUpEvidenceBottomView(caseListFailed.get(i));

                                        if (mBottomSheetBehavior1.getState() != BottomSheetBehavior.STATE_EXPANDED) {

                                            mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_EXPANDED);

                                            Log.d("BottomSheet", "state expanded");


                                        } else {

                                            mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_COLLAPSED);

                                            Log.d("BottomSheet", "state collapsed");

                                        }

                                        break;
                                    }
                                }
                            }
                        }
                    }
                });

                } else {

                Toast.makeText(StateVictimsMapActivity.this, "Nothing to Display", Toast.LENGTH_SHORT).show();

            }
        }
    }

    public void HideUnhideSuccessCases(boolean hide){

        if (hide){

            for (int i = 0; i<caseListSuccess.size();i++){

                if (caseListSuccess.get(i).getStatus().equalsIgnoreCase("Successful")) {

                    if (caseListSuccess.get(i).getMarker()!=null)
                        caseListSuccess.get(i).getMarker().setVisible(false);

                }
            }

        }else {

            for (int i = 0; i<caseListSuccess.size();i++){

                if (caseListSuccess.get(i).getStatus().equalsIgnoreCase("Successful")) {

                    if (caseListSuccess.get(i).getMarker()!=null)
                        caseListSuccess.get(i).getMarker().setVisible(true);

                }
            }
        }

    }

    public void HideUnhideFailedCases(boolean hide){

        if (hide){

            for (int i = 0; i<caseListFailed.size();i++){

                if (caseListFailed.get(i).getStatus().equalsIgnoreCase("Failed")) {

                    if (caseListFailed.get(i).getMarker()!=null)
                        caseListFailed.get(i).getMarker().setVisible(false);

                }

            }

        }else {

            for (int i = 0; i<caseListFailed.size();i++){

                if (caseListFailed.get(i).getStatus().equalsIgnoreCase("Failed")) {

                    if (caseListFailed.get(i).getMarker()!=null)
                        caseListFailed.get(i).getMarker().setVisible(true);

                }

            }
        }





    }


    public void setUpEvidenceBottomView(Case cs) {

        gvImages = (GridView) findViewById(R.id.gridViewStateVictims);
        lvAudio = (ListView) findViewById(R.id.listViewAudioStateVictims);
        lvVideos = (ListView) findViewById(R.id.listViewVideosStateVictims);
        tvImage = (TextView) findViewById(R.id.textViewImagesStateVictims);
        tvVideo = (TextView) findViewById(R.id.textViewVideossStateVictims);
        tvAudio = (TextView) findViewById(R.id.textViewAudioStateVictims);
        textViewDataNoAva = (TextView) findViewById(R.id.textViewDataNotAvaStateVictims);
        tvInvestName = (TextView) findViewById(R.id.textViewEvidenceInvestigatorNameStateVictims);

        textViewDataNoAva.setVisibility(View.GONE);


        imageList.clear();
        videoList.clear();
        audioList.clear();

        getVictimsEvidenceData = new GetVictimsEvidenceData(cs, StateVictimsMapActivity.this);
        getVictimsEvidenceData.execute();


        gvImages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final Dialog nagDialog = new Dialog(StateVictimsMapActivity.this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                nagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                nagDialog.setCancelable(false);
                nagDialog.setContentView(R.layout.preview_image);
                Button btnClose = (Button) nagDialog.findViewById(R.id.btnIvClose);
                ImageView ivPreview = (ImageView) nagDialog.findViewById(R.id.iv_preview_image);

                ImageLoader imageLoader = ImageLoader.getInstance();
                DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                        .cacheOnDisc(true).resetViewBeforeLoading(true)
                        .build();

                imageLoader.displayImage(imageList.get(position).getUrl(), ivPreview, options);

                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {

                        nagDialog.dismiss();
                    }
                });
                nagDialog.show();

            }
        });

    }


    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }

    public String getVideoThumbnail(String videoID){

        String thumb = null;

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String responseJsonStr = null;

        try {
            // Construct the URL for the OpenWeatherMap query
            // Possible parameters are avaiable at OWM's forecast API page, at
            // http://openweathermap.org/API#forecast
            URL url = new URL("https://api.dailymotion.com/video/"+videoID+"?fields=thumbnail_medium_url,thumbnail_small_url,thumbnail_large_url");

            // Create the request to OpenWeatherMap, and open the connection
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                thumb = null;
                return null;
            }

            responseJsonStr = buffer.toString();

            Log.d("VideoFetch", "response  =  "+responseJsonStr);


            JSONObject jsonObject = new JSONObject(responseJsonStr);

            thumb = jsonObject.getString("thumbnail_medium_url");

            Log.d("VideoFetch", "thumb =  "+thumb);


        } catch (IOException e) {
            Log.d("VideoFetch", "Error ", e);
            // If the code didn't successfully get the weather data, there's no point in attemping
            // to parse it.
            thumb = null;
            return null;

        } catch (JSONException e) {

            Log.e("VideoFetch", "exception  ", e);
            e.printStackTrace();
            thumb = null;
            return null;

        } finally{

            if (urlConnection != null) {
                urlConnection.disconnect();
            }

            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("PlaceholderFragment", "Error closing stream", e);
                }
            }
        }

        return thumb;

    }

    private class GetVictimsEvidenceData extends AsyncTask<Void,Void,Void> {

        ProgressBar progressBar;

        String investigator_name;
        String investigator_contact;

        String name;
        Context context;
        String invest_insta_id;

        Case cs;

        public GetVictimsEvidenceData(Case cs, Context context) {
            this.context = context;
            this.cs = cs;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            progressBar = (ProgressBar)findViewById(R.id.progressBarVictimEvidenceStateVictims);

            progressBar.setVisibility(View.VISIBLE);

            textViewDataNoAva.setVisibility(View.GONE);

           name = cs.getvName();
           invest_insta_id = cs.getInvestInstaID();
           investigator_name = cs.getInvestName();
            investigator_contact = cs.getInvestContact();

        }


        @Override
        protected Void doInBackground(Void... params) {

            try {

                String images = cs.getImagesJSON();

                if (images!=null) {

                    JSONArray jsonArray = new JSONArray(images);

                    for (int i = 0; i < jsonArray.length(); i++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        Image image = new Image();

                        image.setDate(jsonObject.getString("date"));
                        image.setUrl(jsonObject.getString("url"));
                        image.setThumbUrl(jsonObject.getString("url"));
                        image.setLat(jsonObject.getDouble("lat"));
                        image.setLng(jsonObject.getDouble("lng"));

                        imageList.add(image);
                    }
                }
            }catch (JSONException e) {
                e.printStackTrace();


            }


            if (cs.getVideosJSON()!=null){

                try {

                    JSONArray jsonArray = new JSONArray(cs.getVideosJSON());

                    for (int i =0 ; i<jsonArray.length();i++){

                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        Video video = new Video();
                        video.setDate(jsonObject.getString("date"));
                        video.setId(jsonObject.getString("videoID"));
                        video.setLat(jsonObject.getDouble("lat"));
                        video.setLng(jsonObject.getDouble("lng"));

                        String thumb = getVideoThumbnail(jsonObject.getString("videoID"));

                        video.setThumb(thumb);

                        videoList.add(video);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (cs.getAudiosJSON()!=null){

                try {

                    JSONArray jsonArray = new JSONArray(cs.getAudiosJSON());

                    for (int i =0 ; i<jsonArray.length();i++){

                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        Audio audio = new Audio();

                        audio.setDate(jsonObject.getString("date"));
                        audio.setUrl(jsonObject.getString("url"));
                        audio.setLat(jsonObject.getDouble("lat"));
                        audio.setLng(jsonObject.getDouble("lng"));

                        audioList.add(audio);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {


            progressBar.setVisibility(View.GONE);

            if (investigator_name!=null){

                tvInvestName.setText(investigator_name);

            }else {
                Log.d("Evidence","investigator_name is  = null");

            }

            if (imageList.size()==0 && videoList.size()==0 && audioList.size()==0){

                textViewDataNoAva.setVisibility(View.VISIBLE);

            }

            Log.d("Evidence","imageList.size = "+imageList.size());


            if (imageList.size()>=1){

                tvImage.setVisibility(View.VISIBLE);
                gvImages.setVisibility(View.VISIBLE);

                EvidenceImageGridViewAdapter imageGridViewAdapter = new EvidenceImageGridViewAdapter(context,imageList);
                gvImages.setAdapter(imageGridViewAdapter);

                gvImages.getViewTreeObserver().addOnGlobalLayoutListener( new ViewTreeObserver.OnGlobalLayoutListener()
                {
                    @Override
                    public void onGlobalLayout()
                    {
                        try {

                            gvImages.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            View lastChild = gvImages.getChildAt(gvImages.getChildCount() - 1);
                            gvImages.setLayoutParams(new LinearLayout.LayoutParams(GridLayout.LayoutParams.FILL_PARENT, lastChild.getBottom()));

                        }catch (Exception e){

                            e.printStackTrace();
                            Log.d("VictimsMap","Exception e = "+e);
                        }
                    }
                });



            }else{

                tvImage.setVisibility(View.GONE);
                gvImages.setVisibility(View.GONE);


                //  lvImages.setVisibility(View.GONE);

            }

            Log.d("Evidence","videoList.size = "+videoList.size());

            if (videoList.size()>=1){

                tvVideo.setVisibility(View.VISIBLE);
                lvVideos.setVisibility(View.VISIBLE);
                EvidenceVideoListAdapter adapter = new EvidenceVideoListAdapter(context,videoList);
                lvVideos.setAdapter(adapter);

            }else{
                tvVideo.setVisibility(View.GONE);
                lvVideos.setVisibility(View.GONE);

            }

            Log.d("Evidence","audioList.size = "+audioList.size());

            if (audioList.size()>=1){

                tvAudio.setVisibility(View.VISIBLE);
                lvAudio.setVisibility(View.VISIBLE);

                EvidenceAudioListAdapter audioListAdapter = new EvidenceAudioListAdapter(context,audioList);
                lvAudio.setAdapter(audioListAdapter);

            }
            else{
                tvAudio.setVisibility(View.GONE);
                lvAudio.setVisibility(View.GONE);

            }



            //  ListUtils.setDynamicHeight(lvImages);
            StateVictimsMapActivity.ListUtils.setDynamicHeight(lvVideos);
            StateVictimsMapActivity.ListUtils.setDynamicHeight(lvAudio);

        }

    }


    private void setUpSuccessClusterer() {
        // Position the map

        //  map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        //  map.setOnCameraIdleListener(mClusterManager);
        //  map.setOnMarkerClickListener(onMarkerClickListener);

        // Add cluster items (markers) to the cluster manager.

        mClusterManager.setRenderer(new SuccessRendered(StateVictimsMapActivity.this,map,mClusterManager));

        mClusterManager.addItems(caseListSuccess);

    }

    private class SuccessRendered extends DefaultClusterRenderer<Case> {

        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());

        public SuccessRendered(Context context, GoogleMap map, ClusterManager<Case> clusterManager) {
            super(context, map, clusterManager);

        }

        @Override
        protected void onClusterItemRendered(Case clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);

            for (int i=0;i<caseListSuccess.size();i++){

                Case cs = caseListSuccess.get(i);

                if (cs.equals(clusterItem)) {

                    marker.setTag("success");

                    caseListSuccess.get(i).setMarker(marker);

                    Log.d("Case", "   onClusterItemRendered()  Marker Set on  ");

                    break;
                }
            }

        }


        @Override
        protected void onBeforeClusterItemRendered(Case person, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.

            Log.d("VictimsMap","SecuredZoneRendered   onBeforeClusterItemRendered() ");

            //  mImageView.setImageResource(person.profilePhoto);

            //  Bitmap icon = mIconGenerator.makeIcon();

            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.victim_green_marker)).title(person.getvName());

        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Case> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).

            Log.d("VictimsMap","SecuredZoneRendered   onBeforeClusterRendered() ");

            mClusterIconGenerator.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.victim_green_icon_clutser));
            mClusterIconGenerator.setTextAppearance(R.style.AppTheme_TransparentTextAppearance);
            final Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));

        }
        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }

    }

    private void setUpFailedClusterer() {
        // Position the map

        //  map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        //  map.setOnCameraIdleListener(mClusterManager);
        //  map.setOnMarkerClickListener(onMarkerClickListener);

        // Add cluster items (markers) to the cluster manager.

        mClusterManagerFailed.setRenderer(new FailedRendered(StateVictimsMapActivity.this,map,mClusterManagerFailed));

        mClusterManagerFailed.addItems(caseListFailed);

    }

    private class FailedRendered extends DefaultClusterRenderer<Case> {

        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());

        public FailedRendered(Context context, GoogleMap map, ClusterManager<Case> clusterManager) {
            super(context, map, clusterManager);

        }

        @Override
        protected void onClusterItemRendered(Case clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);

            for (int i = 0; i < caseListFailed.size(); i++) {

                Case cs = caseListFailed.get(i);

                if (cs.equals(clusterItem)) {

                    marker.setTag("failed");

                    caseListFailed.get(i).setMarker(marker);

                    Log.d("Case", "   onClusterItemRendered()  Marker Set on  ");

                    break;
                }
            }
        }


        @Override
        protected void onBeforeClusterItemRendered(Case person, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.

            Log.d("VictimsMap","SecuredZoneRendered   onBeforeClusterItemRendered() ");

            //  mImageView.setImageResource(person.profilePhoto);

            //  Bitmap icon = mIconGenerator.makeIcon();

            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.victim_red_marker)).title(person.getvName());
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Case> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).

            Log.d("VictimsMap","SecuredZoneRendered   onBeforeClusterRendered() ");

            mClusterIconGenerator.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.victim_red_icon_cluster));
            mClusterIconGenerator.setTextAppearance(R.style.AppTheme_TransparentTextAppearance);
            final Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));

        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }

    }



}
