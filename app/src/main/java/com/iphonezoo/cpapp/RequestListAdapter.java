package com.iphonezoo.cpapp;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestListAdapter extends BaseAdapter{

    Context context;
    List<IRequests> iRequestsList;


    public RequestListAdapter(Context context, List<IRequests> iRequestsList) {
        this.context = context;
        this.iRequestsList = iRequestsList;
    }

    @Override
    public int getCount() {
        return iRequestsList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final IRequests iRequests = iRequestsList.get(position);

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView = inflater.inflate(R.layout.requests_layout,null);

        TextView tvContact = (TextView)convertView.findViewById(R.id.textViewRequestContact);
        Button btVerify = (Button)convertView.findViewById(R.id.buttonRequestVerify);

        tvContact.setText(iRequests.getContact());

        if (iRequests.isVerified()){

            btVerify.setBackgroundResource(R.drawable.verified_button);
            btVerify.setEnabled(false);

        }
        else{

            btVerify.setBackgroundResource(R.drawable.verify_button);

        }


        btVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //call parse CloudCode

                Map<String, String> params = new HashMap<String, String>();
                params.put("installation",iRequests.getInsta_id());
                params.put("contact",iRequests.getContact());

                ParseCloud.callFunctionInBackground("verifyInvestigator", params, new FunctionCallback<Object>() {
                    @Override
                    public void done(Object o, ParseException e) {

                        if (e==null){

                            Log.d("Verification","Verified......");

                        }
                        else {

                            Log.d("Verification","Verification failed......");
                            e.printStackTrace();

                        }
                    }
                });

            }
        });


        return convertView;
    }
}
