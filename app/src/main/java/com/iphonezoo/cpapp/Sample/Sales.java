package com.iphonezoo.cpapp.Sample;


public class Sales {

    String year;
    int sales;
    int expences;

    public Sales(String year, int sales, int expences) {
        this.year = year;
        this.sales = sales;
        this.expences = expences;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public int getExpences() {
        return expences;
    }

    public void setExpences(int expences) {
        this.expences = expences;
    }
}
