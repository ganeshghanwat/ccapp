package com.iphonezoo.cpapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class ZonesTabFragment extends Fragment {


    String stateSelected;
    Spinner spDistrict;
    ListView lvZones;

    List<String> districtList = new ArrayList<>();
    List<Zone> zoneList = new ArrayList<>();
    ProgressDialog mProgressDialog;

    ZoneListAdapter zoneListAdapter;

    EditText etSearchZone;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        stateSelected = ZonesTabbedActivity.getStateSelected();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_zones_tab, container, false);

        mProgressDialog = new ProgressDialog(getActivity());

        spDistrict = (Spinner)view.findViewById(R.id.spinnerDistricts);
        lvZones = (ListView)view.findViewById(R.id.listViewZones);
        etSearchZone = (EditText)view.findViewById(R.id.editTextSearchZone);


        new GetDistrictDataTask().execute();

        // zoneListAdapter = new ZoneListAdapter(this,zoneList);
        //  lvZones.setAdapter(zoneListAdapter);

        spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String districtSelected = districtList.get(position);
                Log.d("EZOnes", "Dustrict selected = " + districtSelected);

                new GetZoneDataTask().execute(districtSelected);

                // zoneListAdapter.notifyDataSetChanged();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {


            }
        });


        lvZones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Zone selectedZone = (Zone) parent.getItemAtPosition(position);


                //  Zone selectedZone = zoneList.get(position);
                Log.d("EZones", "Zone selected = " + selectedZone.getZoneName());

                //  Intent intent = new Intent(ZonesActivity.this, AllInvestigatorsActivity.class);
              //  Intent intent = new Intent(getActivity(), AllInvestigatorsTabbedActivity.class);
                Intent intent = new Intent(getActivity(), ZoneDetailsActivity.class);
                intent.putExtra("selectedZoneName", selectedZone.getZoneName());
                intent.putExtra("selectedState", stateSelected);
                intent.putExtra("selectedZoneID", selectedZone.getZoneID());
                intent.putExtra("district",selectedZone.getZoneDist());
                startActivity(intent);


            }
        });


        etSearchZone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                zoneListAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }



    //Async task..
    private class GetDistrictDataTask extends AsyncTask<Void, Void, Void> {


        Set<String> districts = new HashSet<>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {


            ParseQuery query = ParseQuery.getQuery("EZones");
            query.whereEqualTo("STATE",stateSelected);
            query.setLimit(1000);
            try {

                List<ParseObject> parseObjectList =  query.find();

                for (ParseObject parseObject : parseObjectList){

                    String d = parseObject.getString("DISTRICT");
                    districts.add(d);

                }
                //  districts.add("All");

                districtList.addAll(districts);

            } catch (ParseException e) {

                Log.d("EZones", "failed to find Ezone objects");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mProgressDialog.dismiss();

            ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, districtList);
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // categoryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            spDistrict.setAdapter(stateAdapter);

            new GetZoneDataTask().execute(districtList.get(0));

        }

    }

    //Async task..
    private class GetZoneDataTask extends AsyncTask<String, Void, Void> {


        Set<Zone> zonesSet = new HashSet<>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {


            String district = params[0];
            Log.d("EZones","doInBackground... district = "+district);

            ParseQuery query = ParseQuery.getQuery("EZones");
            query.whereEqualTo("STATE",stateSelected);
            query.whereEqualTo("DISTRICT", district);
            query.setLimit(1000);
            try {

                List<ParseObject> parseObjectList =  query.find();

                for (ParseObject parseObject : parseObjectList){

                    Zone zone = new Zone();

                    String state = parseObject.getString("STATE");
                    String dist = parseObject.getString("DISTRICT");
                    String tal = parseObject.getString("TAHSIL");
                    String name = parseObject.getString("TAHSIL");
                    int id = (int)parseObject.getDouble("SECUREZONE");
                    double lat = parseObject.getDouble("LATITUDE");
                    double lng = parseObject.getDouble("LONGITUDE");
                    String email = parseObject.getString("EMAIL");
                    String fb = parseObject.getString("FACEBOOK");
                    String tw = parseObject.getString("TWITTER");


                    zone.setZoneName(name);
                    zone.setZoneID(id);
                    zone.setZoneState(state);
                    zone.setZoneDist(dist);
                    zone.setZoneTal(tal);
                    zone.setZoneLat(lat);
                    zone.setZoneLng(lng);
                    zone.setZoneEmail(email);
                    zone.setZoneFb(fb);
                    zone.setZoneTw(tw);

                    zonesSet.add(zone);

                }

                zoneList.clear();
                zoneList.addAll(zonesSet);

            } catch (ParseException e) {

                Log.d("EZones", "failed to find Ezone objects");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            zoneListAdapter = new ZoneListAdapter(getActivity(),zoneList);
            lvZones.setAdapter(zoneListAdapter);

            zoneListAdapter.notifyDataSetChanged();

            mProgressDialog.dismiss();



        }

    }


}
