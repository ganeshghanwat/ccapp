package com.iphonezoo.cpapp;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

public class ZonesTabbedActivity extends AppCompatActivity {



    Toolbar toolbar;
    ViewPager pager;
    ViewPagerStateAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[]={"Zones","Investigators"};
    int Numboftabs =2;

    static String stateSelected;


    public static String getStateSelected() {
        return stateSelected;
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zones_tabbed);


        stateSelected = getIntent().getStringExtra("stateSelected");

        setTitle(stateSelected);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
//        setSupportActionBar(toolbar);


        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new ViewPagerStateAdapter(getSupportFragmentManager(),Titles,Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom selector for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_zones_tabbed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == R.id.action_state_map){

            Intent intent = new Intent(ZonesTabbedActivity.this,StateVictimsMapActivity.class);
            intent.putExtra("state",stateSelected);
            startActivity(intent);

        }
        else if (id == R.id.action_state_chart) {

            Intent intent = new Intent(ZonesTabbedActivity.this,StateChartActivity.class);
            intent.putExtra("state",stateSelected);
            startActivity(intent);


        }

        return super.onOptionsItemSelected(item);
    }
}
