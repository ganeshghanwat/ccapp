package com.iphonezoo.cpapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;


public class AllInvestigatorsFragment extends Fragment {

    ListView lvAllInvestigators;

    EditText etSearch;

    String selectedZoneName;
    int selectedZoneID;

    ProgressDialog mProgressDialog;

    AllInvestigatorListAdapter adapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        selectedZoneName = AllInvestigatorsTabbedActivity.getSelectedZoneName();
        selectedZoneID = AllInvestigatorsTabbedActivity.getSelectedZoneID();
        mProgressDialog = new ProgressDialog(getActivity());



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_all_investigators, container, false);

        lvAllInvestigators = (ListView)view.findViewById(R.id.listViewAllInvestTab);

        etSearch = (EditText)view.findViewById(R.id.editTextSearch);

        if (isNetworkAvailable()) {

            new GetInvestigatorDataTask().execute();
        }else {

            Toast.makeText(getActivity(), R.string.no_internet, Toast.LENGTH_SHORT).show();
        }


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                adapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }



    //Async task..
    private class GetInvestigatorDataTask extends AsyncTask<String, Void, Void> {


        List<GeoXman> geoXmanList = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {

            ParseQuery query = ParseQuery.getQuery("GeoXman");
            query.whereEqualTo("ZoneID",selectedZoneID);
            query.whereEqualTo("ZoneName",selectedZoneName);
            query.setLimit(1000);

            try {

                List<ParseObject> parseObjectList = query.find();

                for (ParseObject parseObject : parseObjectList){

                    GeoXman geoXman = new GeoXman();

                    String id = parseObject.getString("XManID");
                    String name = parseObject.getString("XManName");
                    String type = parseObject.getString("XManType");
                    String contactNum = parseObject.getString("XManContactNum");
                    ParseGeoPoint location = parseObject.getParseGeoPoint("location");
                    String area = parseObject.getString("AreaCovered");
                    String address = parseObject.getString("XManAddress");
                    String zoneName = parseObject.getString("ZoneName");
                    int zoneId = parseObject.getInt("ZoneID");

                    String imageURL = "";

                    try {

                        imageURL = parseObject.getParseFile("Pic").getUrl();

                    } catch (NullPointerException e) {

                        imageURL = "";

                    }

                    geoXman.setId(id);
                    geoXman.setName(name);
                    geoXman.setType(type);
                    geoXman.setContact(contactNum);
                    geoXman.setLocation(location);
                    geoXman.setArea(area);
                    geoXman.setAddress(address);
                    geoXman.setZoneName(zoneName);
                    geoXman.setZoneID(zoneId);
                    geoXman.setImgURL(imageURL);

                    geoXmanList.add(geoXman);

                }

            } catch (ParseException e) {

                Log.d("GeoXman", "Failed to find investigators");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

             adapter = new AllInvestigatorListAdapter(getActivity(),geoXmanList);
             lvAllInvestigators.setAdapter(adapter);

            mProgressDialog.dismiss();

        }

    }

    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }


}
