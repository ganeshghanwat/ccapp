package com.iphonezoo.cpapp;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public class Victim {

    String name;
    LatLng latLng;
    String date;
    String invest_insta_id;

    Marker marker;




    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Victim victim = (Victim) o;

        return name.equals(victim.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInvest_insta_id() {
        return invest_insta_id;
    }

    public void setInvest_insta_id(String invest_insta_id) {
        this.invest_insta_id = invest_insta_id;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }
}
