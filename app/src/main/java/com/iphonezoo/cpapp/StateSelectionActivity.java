package com.iphonezoo.cpapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.iphonezoo.cpapp.Activity.AllChartsActivity;
import com.iphonezoo.cpapp.Activity.AllMapsActivity;
import com.parse.FunctionCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class StateSelectionActivity extends AppCompatActivity {

    Spinner spSelectState;
    Button btGo;
    Button btCharts;

    Button btVictimMap;

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    String stateSelected;

    List<String> stateList = new ArrayList<>();

    List<String> staticStateList = new ArrayList<>(Arrays.asList("West Bengal","Uttarakhand", "Uttar Pradesh", "Tripura", "Telangana", "Tamil Nadu", "Sikkim", "Rajasthan", "Punjab", "Pondicherry", "Odisha","Nagaland", "Mizoram", "Maharashtra", "Meghalaya", "Madhya Pradesh", "Karnataka", "Jharkhand", "Jammu & Kashmir", "Himachal Pradesh", "Haryana", "Gujarat","Goa",
            "Daman & Diu", "Chattisgarh", "Bihar", "Assam", "Arunachal Pradesh", "Andhra Pradesh", "Andaman & Nicobar Islands"));

    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_selection);

        mProgressDialog = new ProgressDialog(this);

        Collections.sort(staticStateList);

        ParseInstallation.getCurrentInstallation().saveInBackground();

        spSelectState = (Spinner) findViewById(R.id.spinnerSelectState);
        btGo = (Button)findViewById(R.id.buttonGo);
        btVictimMap = (Button)findViewById(R.id.buttonVictimsMap);
        btCharts = (Button)findViewById(R.id.buttonCharts);

        if (Build.VERSION.SDK_INT >= 23) {

            askPermissions();

            // Marshmallow+
        } else {
            // Pre-Marshmallow
        }


        btVictimMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(StateSelectionActivity.this,VictimsMapActivity.class);
                startActivity(intent);

            }
        });

     //   new GetStateDataTask().execute();

        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(StateSelectionActivity.this, android.R.layout.simple_spinner_item, staticStateList);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // categoryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spSelectState.setAdapter(stateAdapter);


        spSelectState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //    Log.d("EZones", "State selected = " + stateList.get(position));

                //   stateSelected = stateList.get(position);

                stateSelected = staticStateList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            /*    Map<String, String> params = new HashMap<String, String>();
                params.put("installation","PKhN1OUaNS");
                params.put("contact","+919975755271");

                ParseCloud.callFunctionInBackground("verifyInvestigator", params, new FunctionCallback<Object>() {
                    @Override
                    public void done(Object o, ParseException e) {

                        if (e == null) {

                            Log.d("Verification", "Verified......");

                        } else {

                            Log.d("Verification", "Verification failed......");
                            e.printStackTrace();

                        }
                    }
                });

*/
               // Intent intent = new Intent(StateSelectionActivity.this, ZonesActivity.class);
                Intent intent = new Intent(StateSelectionActivity.this, ZonesTabbedActivity.class);
                intent.putExtra("stateSelected", stateSelected);
                startActivity(intent);

            }
        });

     /*   btTesting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Map<String, String> params = new HashMap<String, String>();
                params.put("installation","PKhN1OUaNS");
                params.put("contact","+919975755271");

                ParseCloud.callFunctionInBackground("verifyInvestigator", params, new FunctionCallback<Object>() {
                    @Override
                    public void done(Object o, ParseException e) {

                        if (e == null) {

                            Log.d("Verification", "Verified......");

                        } else {

                            Log.d("Verification", "Verification failed......");
                            e.printStackTrace();

                        }
                    }
                });

            }
        });

*/


        btCharts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Intent intent = new Intent(StateSelectionActivity.this, AllChartsActivity.class);
                Intent intent = new Intent(StateSelectionActivity.this, AllMapsActivity.class);
                startActivity(intent);

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_state_selection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void askPermissions(){

        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();

        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("GPS");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("Location");
        if (!addPermission(permissionsList, Manifest.permission.SEND_SMS))
            permissionsNeeded.add("Send SMS");
        if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("Read Phone State");
        if (!addPermission(permissionsList, Manifest.permission.CALL_PHONE))
            permissionsNeeded.add("Call Phone");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write External Storage");
        if (!addPermission(permissionsList, Manifest.permission.C2D_MESSAGE))
            permissionsNeeded.add("C2D Message");
        if (!addPermission(permissionsList, Manifest.permission.READ_CONTACTS))
            permissionsNeeded.add("Read Contacts");


        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                }

                /**

                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });

                 **/

                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(StateSelectionActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                // Check for Rationale Option
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS:
            {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.SEND_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CALL_PHONE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.C2D_MESSAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.C2D_MESSAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
                        )
                {
                    // All Permissions Granted
                    //  insertDummyContact();
                } else {
                    // Permission Denied
                  //  Toast.makeText(StateSelectionActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT).show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }





    //Async task..
    private class GetStateDataTask extends AsyncTask<Void, Void, Void> {


        Set<String> states = new HashSet<>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

                mProgressDialog.setMessage("Loading");
                mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {


            ParseQuery query = ParseQuery.getQuery("EZones");
            query.setLimit(1000);

            try {

              List<ParseObject> parseObjectList =  query.find();
                Log.d("EZones","No of states objects = "+parseObjectList.size());

                for (ParseObject parseObject : parseObjectList){

                      String state = parseObject.getString("STATE");
                        states.add(state);
                    Log.d("EZones", "state = "+state);

                }

                stateList.addAll(states);

            } catch (ParseException e) {

                Log.d("EZones","failed to find Ezone objects");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mProgressDialog.dismiss();

            Log.d("EZones", "stateList.size() = "+stateList.size());

            ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(StateSelectionActivity.this, android.R.layout.simple_spinner_item, stateList);
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // categoryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            spSelectState.setAdapter(stateAdapter);

        }

    }

}
