package com.iphonezoo.cpapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.HashMap;
import java.util.Map;

public class SMSListener extends BroadcastReceiver {

    String installation_id;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.

        Log.d("SMS", "SMSListener : onReceive() fired....");


        // Get the object of SmsManager
        final SmsManager sms = SmsManager.getDefault();

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();


        SmsMessage smsMessage;

        if(Build.VERSION.SDK_INT>=19) { //KITKAT

            SmsMessage[] msgs = Telephony.Sms.Intents.getMessagesFromIntent(intent);

            smsMessage = msgs[0];

            //Toast.makeText(context, "kitkat version : "+smsMessage.toString(), Toast.LENGTH_LONG).show();
            System.out.println("kitkat version "+ smsMessage.toString());
            Log.d("SMS","SMSListener : kitkat version :"+ smsMessage.toString());

        }

        else {

            Object pdus[] = (Object[]) bundle.get("pdus");

            smsMessage = SmsMessage.createFromPdu((byte[]) pdus[0]);

            System.out.println("other version " + smsMessage.toString());
            Log.d("SMS","SMSListener : other version :"+smsMessage.toString());


        }

        Log.d("SMS", smsMessage.toString());

        String msg = smsMessage.toString();

        System.out.println("sms body =  " + smsMessage.toString());


        String smsDisplayText = smsMessage.getDisplayMessageBody();

        System.out.println("SMS....smsDisplayText =  "+ smsDisplayText);


        String messageBody = smsMessage.getDisplayMessageBody();

        System.out.println("SMS....messageBody =  "+ messageBody);


        if(smsDisplayText.contains("Investigator")){

            Log.d("SMS", " sms Found");
            System.out.println("SMS....SMS found  ");


            String senderPhoneNum = smsMessage.getDisplayOriginatingAddress();

            Log.d("SMS", "senderNumber = " + senderPhoneNum);

            System.out.println("SMS....numbar =   "+senderPhoneNum);

            Log.d("SMS", "mesaage Body : " + messageBody);

            String messageArray[] = messageBody.split(" ");

            Log.d("SMS","Listener..  messageArray[] = "+messageArray);
            System.out.println("SMS....messageArray =   " + messageArray);


            installation_id = messageArray[2];

            Log.d("SMS", "Listener..  installation_id = " + installation_id);
            System.out.println("SMS....insta is =   "+installation_id);


            Map<String, String> params = new HashMap<String, String>();
            params.put("installation", installation_id);
            params.put("contact",senderPhoneNum);

            ParseCloud.callFunctionInBackground("verifyInvestigator", params, new FunctionCallback<Object>() {
                @Override
                public void done(Object o, ParseException e) {

                    if (e == null) {

                        Log.d("Verification", "Verified......");
                        System.out.println("SMS....Verified  ");


                    } else {

                        Log.d("Verification", "Verification failed......");
                        System.out.println("SMS....verification failed  ");
                        e.printStackTrace();

                    }
                }
            });


              /*  ParseObject object =  new ParseObject("IRequests");

                object.put("Contact",senderPhoneNum);

                object.put("isVerified",false);

                object.put("Installation_id",installation_id);

                object.put("MessageBody",messageBody);

            try {

                object.pin();

                Log.d("SMS", "Saved to localDataStore....pinned ");

            } catch (ParseException e) {

                Log.d("SMS", ".........Failed to save to local datastore.............");
                e.printStackTrace();
            }

            */

        }
        else{
            Log.d("SMS", ".............Wrong SMS.........");
            System.out.println("SMS....Wrong sms  ");

        }
    }
}
