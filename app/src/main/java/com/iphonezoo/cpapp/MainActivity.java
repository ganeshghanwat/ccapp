package com.iphonezoo.cpapp;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.iphonezoo.cpapp.Activity.AllChartsActivity;
import com.iphonezoo.cpapp.Activity.AllMapsActivity;
import com.iphonezoo.cpapp.Adapters.GridViewMainActivityAdapter;
import com.iphonezoo.cpapp.POJO.CCTV;


import java.io.IOException;
import java.util.ArrayList;

import cn.jzvd.JZVideoPlayerStandard;

public class MainActivity extends AppCompatActivity {

    private final static float CLICK_DRAG_TOLERANCE = 10; // Often, there will be a slight, unintentional, drag when the user taps the FAB, so we need to account for this.

    private float downRawX, downRawY;
    private float dX, dY;


    String name[] = {

            "Live Dashboard",
            "Investigators",
            "Charts",
            "Victims Map",
            "Zones"
    };
    int[] image = {
            R.drawable.dashboard_dashboard,
            R.drawable.dashboard_investigators,
            R.drawable.dashboard_charts,
            R.drawable.dashboard_maps,
            R.drawable.dashboard_zones
    };


    GridView gridView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        gridView = (GridView)findViewById(R.id.gridViewMainActivity);

        GridViewMainActivityAdapter adapter = new GridViewMainActivityAdapter(name,this,image);
        gridView.setAdapter(adapter);

        findViewById(R.id.btTesting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cctv = "rtsp://192.168.2.6:554/user=admin&password=&channel=1&stream=1.sdp?real_stream--rtp-caching=100";

                displayVideoViewCCTV(cctv);

              //  displayCCTV(cctv);
            }
        });


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position==0){

                    if (isNetworkAvailable()) {

                        Intent intent = new Intent(MainActivity.this, VictimsMapActivity.class);
                        startActivity(intent);
                    }else {
                        Toast.makeText(MainActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                    }


                }else if (position==1){

                    if (isNetworkAvailable()) {
                        // Intent intent = new Intent(MainActivity.this,StateSelectionActivity.class);
                        Intent intent = new Intent(MainActivity.this, AllInvestigatorsActivity.class);
                        startActivity(intent);
                    }else {
                        Toast.makeText(MainActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                    }

                }else if (position==2){

                    if (isNetworkAvailable()) {
                        Intent intent = new Intent(MainActivity.this, AllChartsActivity.class);
                        startActivity(intent);
                    }else {
                        Toast.makeText(MainActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                    }


                }else if (position==3){

                    if (isNetworkAvailable()) {
                        Intent intent = new Intent(MainActivity.this, AllMapsActivity.class);
                        startActivity(intent);
                    }else {
                        Toast.makeText(MainActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                    }

                }else if (position==4){

                    if (isNetworkAvailable()) {
                        Intent intent = new Intent(MainActivity.this, ZonesActivity.class);
                        startActivity(intent);
                    }else {
                        Toast.makeText(MainActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                    }

            }


        }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }

    public void displayVideoViewCCTV(String videoUrl){

        LayoutInflater inflator = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        final RelativeLayout layout = (RelativeLayout) inflator.inflate(R.layout.cctv_video_view_layout,null);

        final RelativeLayout mainLayout = (RelativeLayout)findViewById(R.id.main_activity_container);

        View view = (View)layout.findViewById(R.id.viewNew);

        // RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(170, 170);

        //  params.leftMargin = 100;
        //  params.topMargin = 100;

        mainLayout.addView(layout);

       // final VideoView vvVideoPlay = (VideoView) layout.findViewById(R.id.videoView);
        final Button btClose = (Button)layout.findViewById(R.id.buttonSurfaceViewClose);
        Button btFullScreen = (Button)layout.findViewById(R.id.buttonSurfaceViewFullScreen);



        final JZVideoPlayerStandard jcVideoPlayerStandard = (JZVideoPlayerStandard) layout.findViewById(R.id.videoPlayer);
        jcVideoPlayerStandard.setUp( videoUrl, JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL , " XYZ " );
       // jcVideoPlayerStandard . thumbImageView . setImage( " http://p.qpic.cn/videoyun/0/2449_43b6f696980311e59ed467f22794e792_1/640 " );


        jcVideoPlayerStandard.startVideo();

        btClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // destroy surface view

                Log.d("VictimsMap","on close button click  ");

                mainLayout.removeView(layout);

                // surfaceView.setVisibility(View.GONE);

            }
        });

        btFullScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewGroup.LayoutParams layoutParams = jcVideoPlayerStandard.getLayoutParams();

                String tag = (String) v.getTag();
                Log.d("VictimsMap","tag = "+tag);

                if (tag.equals("0")){
                    // fulscreen
                    v.setTag("1");

                    layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
                    layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;

                    jcVideoPlayerStandard.setLayoutParams(layoutParams);


                }else{

                    // small screen
                    v.setTag("0");
                    layoutParams.height = 300;
                    layoutParams.width = 300;
                    jcVideoPlayerStandard.setLayoutParams(layoutParams);

                }

            }
        });

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                Log.d("MainActivity","onTOuch Fired ");

                int action = motionEvent.getAction();
                if (action == MotionEvent.ACTION_DOWN) {

                    downRawX = motionEvent.getRawX();
                    downRawY = motionEvent.getRawY();
                    dX = layout.getX() - downRawX;
                    dY = layout.getY() - downRawY;

                    return true; // Consumed

                }
                else if (action == MotionEvent.ACTION_MOVE) {

                    int viewWidth = layout.getWidth();
                    int viewHeight = layout.getHeight();

                    View viewParent = (View)layout.getParent();
                    int parentWidth = viewParent.getWidth();
                    int parentHeight = viewParent.getHeight();

                    float newX = motionEvent.getRawX() + dX;
                    newX = Math.max(0, newX); // Don't allow the FAB past the left hand side of the parent
                    newX = Math.min(parentWidth - viewWidth, newX); // Don't allow the FAB past the right hand side of the parent

                    float newY = motionEvent.getRawY() + dY;
                    newY = Math.max(0, newY); // Don't allow the FAB past the top of the parent
                    newY = Math.min(parentHeight - viewHeight, newY); // Don't allow the FAB past the bottom of the parent

                    layout.animate()
                            .x(newX)
                            .y(newY)
                            .setDuration(0)
                            .start();

                    return true; // Consumed

                }
                else if (action == MotionEvent.ACTION_UP) {

                    float upRawX = motionEvent.getRawX();
                    float upRawY = motionEvent.getRawY();

                    float upDX = upRawX - downRawX;
                    float upDY = upRawY - downRawY;

                    if (Math.abs(upDX) < CLICK_DRAG_TOLERANCE && Math.abs(upDY) < CLICK_DRAG_TOLERANCE) { // A click
                        //  return performClick();
                        return true;
                    }
                    else { // A drag
                        return true; // Consumed
                    }

                }
                else {
                    return true;
                }
            }
        });


    }


}
