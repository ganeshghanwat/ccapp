package com.iphonezoo.cpapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ZonesActivity extends AppCompatActivity {


    Spinner spDistrict;
    ListView lvZones;

    List<String> districtList = new ArrayList<>();
    List<Zone> zoneList = new ArrayList<>();

    String stateSelected;

    ProgressDialog mProgressDialog;

    ZoneListAdapter zoneListAdapter;

    EditText etSearchZone;

    Spinner spState;

    List<String> staticStateList = new ArrayList<>(Arrays.asList("West Bengal","Uttarakhand", "Uttar Pradesh", "Tripura", "Telangana", "Tamil Nadu", "Sikkim", "Rajasthan", "Punjab", "Pondicherry", "Odisha","Nagaland", "Mizoram", "Maharashtra", "Meghalaya", "Madhya Pradesh", "Karnataka", "Jharkhand", "Jammu & Kashmir", "Himachal Pradesh", "Haryana", "Gujarat","Goa",
            "Daman & Diu", "Chattisgarh", "Bihar", "Assam", "Arunachal Pradesh", "Andhra Pradesh", "Andaman & Nicobar Islands"));


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zones);

        mProgressDialog = new ProgressDialog(ZonesActivity.this);

        spDistrict = (Spinner)findViewById(R.id.spinnerDistricts);
        lvZones = (ListView)findViewById(R.id.listViewZones);
        etSearchZone = (EditText)findViewById(R.id.editTextSearchZone);
        spState = (Spinner)findViewById(R.id.spinnerStateAllZones);



        Collections.sort(staticStateList);

        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(ZonesActivity.this, android.R.layout.simple_spinner_item, staticStateList);
        stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // categoryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spState.setAdapter(stateAdapter);


        spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //    Log.d("EZones", "State selected = " + stateList.get(position));

                //   stateSelected = stateList.get(position);

                stateSelected = staticStateList.get(position);

                    if (isNetworkAvailable()){

                new GetDistrictDataTask().execute();

            }else {
                    Toast.makeText(ZonesActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        lvZones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Zone selectedZone = (Zone) parent.getItemAtPosition(position);


              //  Zone selectedZone = zoneList.get(position);
                Log.d("EZones", "Zone selected = " + selectedZone.getZoneName());

                //  Intent intent = new Intent(ZonesActivity.this, AllInvestigatorsActivity.class);
                Intent intent = new Intent(ZonesActivity.this, AllInvestigatorsTabbedActivity.class);
                intent.putExtra("selectedZoneName", selectedZone.getZoneName());
                intent.putExtra("selectedState",stateSelected);
                intent.putExtra("selectedZoneID", selectedZone.getZoneID());
                startActivity(intent);


            }
        });


        etSearchZone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                zoneListAdapter.getFilter().filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_zones, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //Async task..
    private class GetDistrictDataTask extends AsyncTask<Void, Void, Void> {


        Set<String> districts = new HashSet<>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

            districtList.clear();

        }

        @Override
        protected Void doInBackground(Void... params) {


            ParseQuery query = ParseQuery.getQuery("EZones");
            query.whereEqualTo("STATE",stateSelected);
            query.setLimit(1000);
            try {

                List<ParseObject> parseObjectList =  query.find();

                for (ParseObject parseObject : parseObjectList){

                    String d = parseObject.getString("DISTRICT");
                    districts.add(d);

                }
              //  districts.add("All");

                districtList.addAll(districts);

            } catch (ParseException e) {

                Log.d("EZones", "failed to find Ezone objects");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mProgressDialog.dismiss();

            ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(ZonesActivity.this, android.R.layout.simple_spinner_item, districtList);
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // categoryAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            spDistrict.setAdapter(stateAdapter);


            spDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    String districtSelected = districtList.get(position);
                    Log.d("EZOnes", "Dustrict selected = " + districtSelected);

                    new GetZoneDataTask().execute(districtSelected);

                    // zoneListAdapter.notifyDataSetChanged();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {


                }
            });

          //  new GetZoneDataTask().execute(districtList.get(0));

        }

    }

    //Async task..
    private class GetZoneDataTask extends AsyncTask<String, Void, Void> {


        Set<Zone> zonesSet = new HashSet<>();


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {


            String district = params[0];
            Log.d("EZones","doInBackground... district = "+district);

            ParseQuery query = ParseQuery.getQuery("EZones");
            query.whereEqualTo("STATE",stateSelected);
            query.whereEqualTo("DISTRICT", district);
            query.setLimit(1000);
            try {

                List<ParseObject> parseObjectList =  query.find();

                for (ParseObject parseObject : parseObjectList){

                    Zone zone = new Zone();

                    String state = parseObject.getString("STATE");
                    String dist = parseObject.getString("DISTRICT");
                    String tal = parseObject.getString("TAHSIL");
                    String name = parseObject.getString("TAHSIL");
                    int id = (int)parseObject.getDouble("SECUREZONE");
                    double lat = parseObject.getDouble("LATITUDE");
                    double lng = parseObject.getDouble("LONGITUDE");
                    String email = parseObject.getString("EMAIL");
                    String fb = parseObject.getString("FACEBOOK");
                    String tw = parseObject.getString("TWITTER");


                    zone.setZoneName(name);
                    zone.setZoneID(id);
                    zone.setZoneState(state);
                    zone.setZoneDist(dist);
                    zone.setZoneTal(tal);
                    zone.setZoneLat(lat);
                    zone.setZoneLng(lng);
                    zone.setZoneEmail(email);
                    zone.setZoneFb(fb);
                    zone.setZoneTw(tw);

                    zonesSet.add(zone);

                }

                zoneList.clear();
                zoneList.addAll(zonesSet);

            } catch (ParseException e) {

                Log.d("EZones", "failed to find Ezone objects");
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            zoneListAdapter = new ZoneListAdapter(ZonesActivity.this,zoneList);
            lvZones.setAdapter(zoneListAdapter);

            zoneListAdapter.notifyDataSetChanged();

            mProgressDialog.dismiss();


        }

    }

    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }

}
