package com.iphonezoo.cpapp;


import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterItem;
import com.parse.ParseGeoPoint;

import org.json.JSONArray;

import java.util.Date;
import java.util.List;

public class Case implements ClusterItem {

    ParseGeoPoint locationPoint;


    Date createdAt;
    JSONArray locations;
    String vName;
    String vContact;
    String vAddress;
    String sImage;
    String vEmail;
    String vParents;
    String vReport;
    String vImage;
    String financialLoss;
    String humansAffected;
    String humansKilled;
    String noOfSuspect;
    String audios;
    String images;
    String videos;
    String status;
    String investContact;
    String investInstaID;
    String investName;
    String crimeCategory;

    long fbID;
    String imagesJSON;
    String videosJSON;
    String audiosJSON;
    String locationsJSON;

    Marker marker;



    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public ParseGeoPoint getLocationPoint() {
         return locationPoint;
    }

    public void setLocationPoint(ParseGeoPoint locationPoint) {
        this.locationPoint = locationPoint;
    }

    public JSONArray getLocations() {
        return locations;
    }

    public void setLocations(JSONArray locations) {
        this.locations = locations;
    }

    public String getvName() {
        return vName;
    }

    public void setvName(String vName) {
        this.vName = vName;
    }

    public String getvContact() {
        return vContact;
    }

    public void setvContact(String vContact) {
        this.vContact = vContact;
    }

    public String getvAddress() {
        return vAddress;
    }

    public void setvAddress(String vAddress) {
        this.vAddress = vAddress;
    }

    public String getsImage() {
        return sImage;
    }

    public void setsImage(String sImage) {
        this.sImage = sImage;
    }

    public String getvEmail() {
        return vEmail;
    }

    public void setvEmail(String vEmail) {
        this.vEmail = vEmail;
    }

    public String getvParents() {
        return vParents;
    }

    public void setvParents(String vParents) {
        this.vParents = vParents;
    }

    public String getvReport() {
        return vReport;
    }

    public void setvReport(String vReport) {
        this.vReport = vReport;
    }

    public String getvImage() {
        return vImage;
    }

    public void setvImage(String vImage) {
        this.vImage = vImage;
    }

    public String getFinancialLoss() {
        return financialLoss;
    }

    public void setFinancialLoss(String financialLoss) {
        this.financialLoss = financialLoss;
    }

    public String getHumansAffected() {
        return humansAffected;
    }

    public void setHumansAffected(String humansAffected) {
        this.humansAffected = humansAffected;
    }

    public String getHumansKilled() {
        return humansKilled;
    }

    public void setHumansKilled(String humansKilled) {
        this.humansKilled = humansKilled;
    }

    public String getNoOfSuspect() {
        return noOfSuspect;
    }

    public void setNoOfSuspect(String noOfSuspect) {
        this.noOfSuspect = noOfSuspect;
    }

    public String getAudios() {
        return audios;
    }

    public void setAudios(String audios) {
        this.audios = audios;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getVideos() {
        return videos;
    }

    public void setVideos(String videos) {
        this.videos = videos;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInvestContact() {
        return investContact;
    }

    public void setInvestContact(String investContact) {
        this.investContact = investContact;
    }

    public String getInvestInstaID() {
        return investInstaID;
    }

    public void setInvestInstaID(String investInstaID) {
        this.investInstaID = investInstaID;
    }

    public String getInvestName() {
        return investName;
    }

    public void setInvestName(String investName) {
        this.investName = investName;
    }

    public String getCrimeCategory() {
        return crimeCategory;
    }

    public void setCrimeCategory(String crimeCategory) {
        this.crimeCategory = crimeCategory;
    }

    public long getFbID() {
        return fbID;
    }

    public void setFbID(long fbID) {
        this.fbID = fbID;
    }

    public String getImagesJSON() {
        return imagesJSON;
    }

    public void setImagesJSON(String imagesJSON) {
        this.imagesJSON = imagesJSON;
    }

    public String getVideosJSON() {
        return videosJSON;
    }

    public void setVideosJSON(String videosJSON) {
        this.videosJSON = videosJSON;
    }

    public String getAudiosJSON() {
        return audiosJSON;
    }

    public void setAudiosJSON(String audiosJSON) {
        this.audiosJSON = audiosJSON;
    }

    public String getLocationsJSON() {
        return locationsJSON;
    }

    public void setLocationsJSON(String locationsJSON) {
        this.locationsJSON = locationsJSON;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    @Override
    public LatLng getPosition() {
        return new LatLng(locationPoint.getLatitude(),locationPoint.getLongitude());
    }

    @Override
    public String getTitle() {
        return vName;
    }

    @Override
    public String getSnippet() {
        return null;
    }
}
