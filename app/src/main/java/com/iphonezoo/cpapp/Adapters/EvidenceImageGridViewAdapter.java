package com.iphonezoo.cpapp.Adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iphonezoo.cpapp.POJO.Image;
import com.iphonezoo.cpapp.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class EvidenceImageGridViewAdapter extends BaseAdapter{


    Context context;
    List<Image> imageList;

    public EvidenceImageGridViewAdapter(Context context, List<Image> imageList) {
        this.context = context;
        this.imageList = imageList;
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.image_grid_view_layout,null);

        ImageView imageView = (ImageView)view.findViewById(R.id.imageViewGrid);
        TextView tvDate = (TextView)view.findViewById(R.id.textViewImageGridDate);

        tvDate.setText(imageList.get(position).getDate());

        ImageLoader imageLoader = ImageLoader.getInstance();

        Log.d("Evidence","thumb url adapter= "+imageList.get(position).getThumbUrl());

        imageLoader.displayImage(imageList.get(position).getThumbUrl(), imageView);

        return view;
    }
}
