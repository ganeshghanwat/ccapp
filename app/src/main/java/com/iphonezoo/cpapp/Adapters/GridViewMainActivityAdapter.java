package com.iphonezoo.cpapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iphonezoo.cpapp.R;


public class GridViewMainActivityAdapter extends BaseAdapter {

    String[] name;
    private Context mContext;
    int[] Imageid;


    public GridViewMainActivityAdapter(String[] name, Context mContext, int[] imageid) {
        this.name = name;
        this.mContext = mContext;
        Imageid = imageid;
    }

    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View list = inflater.inflate(R.layout.gridview_main_activity,null);


        ImageView imageView = (ImageView)list.findViewById(R.id.imageViewGridView);
        TextView descTextView = (TextView)list.findViewById(R.id.textViewGridView);

        descTextView.setText(name[position]);
        imageView.setImageResource(Imageid[position]);

        return list;
    }
}
