package com.iphonezoo.cpapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.iphonezoo.cpapp.Case;
import com.iphonezoo.cpapp.R;
import com.iphonezoo.cpapp.Victim;

import java.util.List;

public class InvestigatorVictimsAdapter extends BaseAdapter {

    List<Case> victimList;
    Context context;

    public InvestigatorVictimsAdapter(List<Case> victimList, Context context) {
        this.victimList = victimList;
        this.context = context;
    }

    @Override
    public int getCount() {

        return victimList.size();

    }

    @Override
    public Object getItem(int position) {
        return victimList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.investigator_victims_layout,null);

        TextView tvName = (TextView)view.findViewById(R.id.textViewInvVName);
        TextView tvDate = (TextView)view.findViewById(R.id.textViewInvVCreatedAt);

        tvName.setText(victimList.get(position).getvName());
        tvDate.setText(victimList.get(position).getCreatedAt().toString());

        return view;
    }
}
