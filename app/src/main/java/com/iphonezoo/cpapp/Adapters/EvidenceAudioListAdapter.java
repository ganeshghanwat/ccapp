package com.iphonezoo.cpapp.Adapters;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.iphonezoo.cpapp.POJO.Audio;
import com.iphonezoo.cpapp.R;

import java.io.IOException;
import java.util.List;


public class EvidenceAudioListAdapter extends BaseAdapter {

    MediaPlayer mediaPlayer;

    Context context;
    List<Audio> audioList;

    public EvidenceAudioListAdapter(Context context, List<Audio> audioList) {
        this.context = context;
        this.audioList = audioList;
    }


    @Override
    public int getCount() {
        return audioList.size();
    }

    @Override
    public Object getItem(int position) {
        return audioList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

       final int i = position;

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.evidence_audio_list_layout,null);

        TextView tvDate = (TextView)view.findViewById(R.id.textViewEvidenceAudioDate);

        tvDate.setText(audioList.get(position).getDate());

        Button btPlay = (Button)view.findViewById(R.id.buttonEviAudioPlay);
        Button btStop = (Button)view.findViewById(R.id.buttonEviAudioStop);

        btPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkAvailable()) {

                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

                    try {

                        Log.d("Evidence", " audio url = " + audioList.get(i).getUrl());

                        mediaPlayer.setDataSource(audioList.get(i).getUrl());
                        mediaPlayer.prepare();
                        mediaPlayer.start();


                    } catch (IOException e) {

                        e.printStackTrace();
                        Log.d("Evidence", " audio play exception  = " + e);

                    }
                }else {
                    Toast.makeText(context, R.string.no_internet, Toast.LENGTH_SHORT).show();
                }

            }
        });

        btStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    try{

                    mediaPlayer.stop();

                }catch (Exception e){

                        e.printStackTrace();

                    }


            }
        });


        return view;
    }


    // check internet connection
    private boolean isNetworkAvailable() {

        boolean status = false;

        try {

            ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            status = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            // return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch (Exception e){

            Log.d("Internet","isNetworkAvailable()    exception occured");
            e.printStackTrace();

            status = false;

        }

        return status;
    }

}
